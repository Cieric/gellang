// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once

#include <vector>
#include <string>
#include <sstream>
#include "token.h"

class Tokenizer
{
	std::string buffer;
	std::stringstream source;
	size_t line = 1;
	size_t column = 1;
public:
	std::string filename;
	std::vector<std::string> errors;
	Tokenizer(std::stringstream&& source);

	Token Next();
	Tokens GetAll();

private:
	Token BuildToken(Token::Type type);
	Token BuildToken(Token::Type type, std::string text);


	char Advance();
	void NewLine();

	void setError(std::string error);
	
	bool Consume(char c, std::string err);
	bool Check(char c);
	bool CheckAny(std::vector<char> s);
	bool MatchAny(std::vector<char> s);
	char Peek();
	char Prev();

	Token ParseWord(char c);
	Token ParseNumber(char c);
	void SkipComment(bool mutiline = false);
};

