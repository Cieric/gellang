// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "stmt.h"
#include "../CompilerUtils.h"
#include "../hash.h"

DataType* typeFromString(std::string name)
{
    switch (hash(name.c_str()))
    {
    case const_hash("void"):   return  new DataTypeVoid();
    case const_hash("bool"):   return  new DataTypeBool();
    case const_hash("int"):    return  new DataTypeInt(32, true );
    case const_hash("uint"):   return  new DataTypeInt(32, false );
    case const_hash("string"): return  new DataTypeString();

    case const_hash("uvec2"):  return  new DataTypeVector(typeFromString("uint"), 2);
    case const_hash("uvec3"):  return  new DataTypeVector(typeFromString("uint"), 3);
    case const_hash("uvec4"):  return  new DataTypeVector(typeFromString("uint"), 4);

    case const_hash("uint8"):  return  new DataTypeInt(8, false );
    case const_hash("float"):  return  new DataTypeFloat(32);

    case const_hash("vec2"):   return  new DataTypeVector(typeFromString("float"), 2);
    case const_hash("mat2x2"):
    case const_hash("mat2"):   return  new DataTypeMatrix(typeFromString("vec2"), 2);

    case const_hash("vec3"):   return  new DataTypeVector(typeFromString("float"), 3);
    case const_hash("mat3x3"):
    case const_hash("mat3"):   return  new DataTypeMatrix(typeFromString("vec3"), 3);

    case const_hash("vec4"):   return  new DataTypeVector(typeFromString("float"), 4);
    case const_hash("mat4x4"):
    case const_hash("mat4"):   return  new DataTypeMatrix(typeFromString("vec4"), 4);
    }

    CompilerError("Error could not determine type from string!");
}