// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include "../token.h"
#include "../SymbolMap.h"
#include "../DataType.h"
#include <cstring>

struct Expr
{
    enum Type {
        BINARY,
        LITERAL,
        UNARY,
        GROUPING,
        VARIABLE,
        ASSIGN,
        CALL,
        SWIZZLE,
        ARRAY,
        MEMBER,
        POSTSCRIPT,
        PREFIX,
        SELECTION,
        SEQUENCE,
        FMA,
    } type;
};

struct ExprBinary : Expr
{
    ExprBinary(Expr* left, Token op, Expr* right) :
        Expr{BINARY},
        left(left),
        op(op),
        right(right)
    {}

    Expr* left;
    Token op;
    Expr* right;
};

struct ExprFMA : Expr
{
    ExprFMA(Expr* left, Expr* middle, Expr* right) :
        Expr{ FMA },
        left(left),
        middle(middle),
        right(right)
    {}

    Expr* left;
    Expr* middle;
    Expr* right;
};

struct ExprLiteral : Expr
{
    ExprLiteral(Token lit) :
        Expr{ LITERAL },
        lit(lit)
    {
        memset(rawdata, 0, sizeof(rawdata));
    }

    ExprLiteral(bool lit) :
        Expr{ LITERAL },
        lit(Token{})
    {
        memset(rawdata, 0, sizeof(rawdata));
    }

    Token lit;
    char rawdata[32];
};

struct ExprUnary : Expr
{
    ExprUnary(Token lit, Expr* right) :
        Expr{UNARY},
        lit(lit),
        right(right)
    {}

    Token lit;
    Expr* right;
};

struct ExprGrouping : Expr
{
    ExprGrouping(Expr* expr) :
        Expr{ GROUPING },
        expr(expr)
    {}

    Expr* expr;
};

struct ExprVariable : Expr
{
    ExprVariable(Token name, Symbol* symbolTableId) :
        Expr{ VARIABLE },
        name(name),
        symbolTableId(symbolTableId)
    {}

    Token name;
    Symbol* symbolTableId;
};

struct ExprAssign : Expr
{
    ExprAssign(Expr* name, Expr* value) :
        Expr{ ASSIGN },
        name(name),
        value(value)
    {}

    Expr* name;
    Expr* value;
};

struct ExprCall : Expr
{
    ExprCall(Token callee, std::vector<Expr*> arguments) :
        Expr{ CALL },
        callee(callee),
        arguments(arguments)
    {}

    Token callee;
    std::vector<Expr*> arguments;
};

struct ExprSwizzle : Expr
{
    ExprSwizzle(Expr* variable, Token swizzle) :
        Expr{ SWIZZLE },
        variable(variable),
        swizzle(swizzle)
    {}

    Expr* variable;
    Token swizzle;
};

struct ExprArray : Expr
{
    ExprArray(Expr* variable, Expr* index) :
        Expr{ ARRAY },
        variable(variable),
        index(index)
    {}

    Expr* variable;
    Expr* index;
};

struct ExprMember : Expr
{
    ExprMember(Expr* variable, Token member) :
        Expr{ MEMBER },
        variable(variable),
        member(member)
    {}

    Expr* variable;
    Token member;
};

struct ExprPostscript : Expr
{
    ExprPostscript(Expr* variable, Token op) :
        Expr{ POSTSCRIPT },
        variable(variable),
        op(op)
    {}

    Expr* variable;
    Token op;
};

struct ExprPrefix : Expr
{
    ExprPrefix(Expr* variable, Token op) :
        Expr{ PREFIX },
        variable(variable),
        op(op)
    {}

    Expr* variable;
    Token op;
};

struct ExprSelection : Expr
{
    ExprSelection(Expr* test, Expr* then, Expr* elseThen) :
        Expr{ SELECTION },
        test(test),
        then(then),
        elseThen(elseThen)
    {}

    Expr* test;
    Expr* then;
    Expr* elseThen;
};

struct ExprSequence : Expr
{
    ExprSequence(std::vector<Expr*> sequence) :
        Expr{ SEQUENCE },
        sequence(sequence)
    {}

    std::vector<Expr*> sequence;
};