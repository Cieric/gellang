// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include "expr.h"
#include "../Assembler/ScopeMap.h"
#include "../Assembler/SpirV/SpirvAstOps.h"
#include "../Params.h"

DataType* typeFromString(std::string token);

enum StmtType {
    PRINT,
    EXPR,
    DECL,
    BLOCK,
    IF,
    WHILE,
    DO_WHILE,
    STRUCT,
    FUNC,
    RETURN,
    BREAK,
    ASM,
};

struct Stmt
{
    StmtType type;
};

struct StmtPrint : Stmt
{
    StmtPrint(Expr* expr) :
        Stmt{ PRINT },
        expr(expr)
    {}

    Expr* expr;
};

struct StmtExpr : Stmt
{
    StmtExpr(Expr* expr) :
        Stmt{ EXPR },
        expr(expr)
    {}

    Expr* expr;
};

struct StmtDecl : Stmt
{
    StmtDecl(DataType* type, std::vector<std::pair<Symbol*, Expr*>> varDecls) :
        Stmt{ DECL },
        type(type),
        varDecls(varDecls)
    {}

    DataType* type;
    std::vector<std::pair<Symbol*, Expr*>> varDecls;
};

struct StmtBlock : Stmt
{
    StmtBlock(std::vector<Stmt*> stmts) :
        Stmt{ BLOCK },
        stmts(stmts)
    {}

    ScopeMap<Ast*>* scope;
    std::vector<Stmt*> stmts;
    uint32_t frameSize;
};

struct StmtIf : Stmt
{
    StmtIf(Expr* condition, Stmt* thenBranch, Stmt* elseBranch) :
        Stmt{ IF },
        condition(condition),
        thenBranch(thenBranch),
        elseBranch(elseBranch)
    {}

    Expr* condition;
    Stmt* thenBranch;
    Stmt* elseBranch;
};

struct StmtWhile : Stmt
{
    StmtWhile(Expr* condition, Stmt* body) :
        Stmt{ WHILE },
        condition(condition),
        body(body)
    {}

    Expr* condition;
    Stmt* body;
};

struct StmtDoWhile : Stmt
{
    StmtDoWhile(Stmt* body, Expr* condition) :
        Stmt{ DO_WHILE },
        condition(condition),
        body(body)
    {}

    Expr* condition;
    Stmt* body;
};

struct StmtStruct : Stmt
{
    StmtStruct(Token name, std::vector<Symbol*> members) :
        Stmt{ STRUCT },
        name(name),
        members(members)
    {}
    
    Token name;
    std::vector<Symbol*> members;
};

struct StmtFunc : Stmt
{
    StmtFunc(Token returnType, Token name, std::vector<Parameter> parameters, StmtBlock* body) :
        Stmt{ FUNC },
        returnType(typeFromString(returnType.text)),
        name(name),
        parameters(parameters),
        body(body)
    {}

    ScopeMap<Ast*>* scope;
    DataType* returnType;
    Token name;
    std::vector<Parameter> parameters;
    bool varargs = false;
    StmtBlock* body;
};

struct StmtReturn : Stmt
{
    StmtReturn(Token keyword, Expr* value) :
        Stmt{ RETURN },
        keyword(keyword),
        value(value)
    {}

    Token keyword;
    Expr* value;
};

struct StmtBreak : Stmt
{
    StmtBreak(Token keyword, Expr* value) :
        Stmt{ BREAK },
        keyword(keyword),
        value(value)
    {}

    Token keyword;
    Expr* value;
};

struct StmtAsm : Stmt
{
    StmtAsm(Token result, Token opcode, std::vector<Token> parameters) :
        Stmt{ ASM },
        result(result),
        opcode(opcode),
        parameters(parameters)
    {}

    Token result;
    Token opcode;
    std::vector<Token> parameters;
};