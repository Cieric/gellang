// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once

enum class Precision
{
	lowp,
	mediump,
	highp
};