// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once

#include <cstddef>
#include <sstream>
#include <list>
#include <set>
#include <unordered_map>
#include <stack>
#include "token.h"

struct Define
{
    std::string key;
    std::set<std::string> parameters;
    Tokens value;
};

using Defines = std::unordered_map<std::string, Define>;

class Preprocessor
{
    Defines defines;
    std::deque<Token> tokens;
    Token token();

    int outputCode = 0;
public:
    std::vector<std::string> errors;
    Preprocessor(Tokens tokens);
    Tokens process();
private:
    void setError(std::string errorStr);

    void Advance();
    bool Check(Token::Type type);
    Token Consume(Token::Type type, std::string error);

    void addDefines(Defines& defines);
    Tokens parseDefine();
    Tokens processDefine(Define& define);

    Tokens parseInclude();
    Tokens parseIfDefine();
    Tokens parseIfNDefine();
    Tokens parseIf();
    //Tokens resolveDefines(TokensIter& iter, Defines defines);

    //void parsePreprocessorCommand(TokensIter& input);
    //void removeComments(TokensIter& input);
    //void parseDefine(TokensIter& input);
};