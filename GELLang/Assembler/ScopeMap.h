// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <string>
#include <unordered_map>

template<typename T>
struct ScopeMap
{
    ScopeMap<T>* parent;
    std::unordered_map<std::string, T*> symbols;

    T* get(std::string symbolName)
    {
        auto found = symbols.find(symbolName);
        if (found != symbols.end())
            return found->second;
        return parent->get(symbolName);
    }

    T* def(std::string symbolName)
    {
        auto found = symbols.find(symbolName);
        if (found != symbols.end())
            return nullptr;
        auto symbol = new T();
        symbols.insert_or_assign(symbolName, symbol);
        return symbol;
    }

    ScopeMap<T>* push()
    {
        auto symbolMap = new ScopeMap<T>();
        symbolMap->parent = this;
        return symbolMap;
    }

    ScopeMap<T>* pop()
    {
        return this->parent;
    }
};
