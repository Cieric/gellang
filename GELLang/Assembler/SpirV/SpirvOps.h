// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <cstring>
#include <stdint.h>
#include <string>
#include "SpirvOpParamTypes.h"

enum class OpCode : uint16_t
{
	OpNop			= 0x00,
	OpSource        = 0x03,
	OpName          = 0x05,
	OpString        = 0x07,
	OpLine          = 0x08,
	OpExtInstImport = 0x0B,
	OpExtInst		= 0x0C,
	OpMemoryModel   = 0x0E,
	OpEntryPoint    = 0x0F,
	OpExecutionMode = 0x10,
	OpCapability    = 0x11,
	OpTypeVoid		= 0x13,
	OpTypeBool		= 0x14,
	OpTypeInt		= 0x15,
	OpTypeFloat		= 0x16,
	OpTypeVector	= 0x17,
	OpTypeMatrix	= 0x18,
	OpTypeStruct	= 0x1E,
	OpTypePointer	= 0x20,
	OpTypeFunction	= 0x21,
	OpConstant		= 0x2B,
	OpFunction		= 0x36,
	OpFunctionParameter = 0x37,
	OpFunctionEnd	= 0x38,
	OpFunctionCall	= 0x39,
	OpVariable		= 0x3B,
	OpLoad			= 0x3D,
	OpStore			= 0x3E,
	OpAccessChain	= 0x41,
	OpVectorShuffle = 0x4F,
	OpCompositeConstruct = 0x50,
	OpCompositeExtract = 0x51,
	OpConvertFToS	= 0x6E,
	OpConvertSToF	= 0x6F,
	OpSNegate		= 0x7E,
	OpFNegate		= 0x7F,
	OpIAdd			= 0x80,
	OpFAdd			= 0x81,
	OpFSub			= 0x83,
	OpIMul			= 0x84,
	OpFMul			= 0x85,
	OpSDiv			= 0x87,
	OpFDiv			= 0x88,
	OpFMod			= 0x8D,
	OpMatrixTimesScaler = 0x8F,
	OpVectorTimesMatrix = 0x90,
	OpMatrixTimesVector = 0x91,
	OpMatrixTimesMatrix = 0x92,
	OpDot			= 0x94,
	OpLogicalOr		= 0xA6,
	OpLogicalAnd	= 0xA7,
	OpLogicalNot	= 0xA8,
	OpSGreaterThan	= 0xAD,
	OpFOrdLessThan	= 0xB8,
	OpFOrdLessThanEqual	= 0xBC,
	OpFOrdGreaterThan = 0xBA,
	OpFOrdGreaterThanEqual = 0xBE,
	OpSLessThan		= 0xB1,
	OpSLessThanEqual = 0xB3,
	OpFOrdEqual		= 0xB4,
	OpLoopMerge		= 0xF6,
	OpSelectionMerge = 0xF7,
	OpLabel			= 0xF8,
	OpBranch		= 0xF9,
	OpBranchConditional = 0xFA,
	OpReturn		= 0xFD,
	OpReturnValue	= 0xFE,
};

inline uint32_t encodeOpCode(OpCode opcode, uint16_t words)
{
	uint32_t code = words;
	code <<= 16;
	return code | (uint16_t)opcode;
}

inline void decodeOpCode(uint32_t code, OpCode& opcode, uint16_t& words)
{
	opcode = (OpCode)code;
	code >>= 16;
	words = code;
}

class Op
{
public:
	uint16_t words;
	OpCode opCode;
	std::vector<uint32_t> data;

	Op(uint16_t words, OpCode opCode) :
		words(words),
		opCode(opCode)
	{};

	inline std::vector<uint32_t>::iterator begin()
	{
		return data.begin();
	};

	inline std::vector<uint32_t>::iterator end()
	{
		return data.end();
	};
};

class OpNop : public Op
{
public:
	OpNop() :
		Op(1, OpCode::OpNop)
	{
	};
};

class OpSource : public Op
{
public:
	OpSource(SourceLanguage sourceLanguage, uint32_t version) :
		Op(3, OpCode::OpSource)
	{
		data.resize(2);
		data[0] = (uint32_t)sourceLanguage;
		data[1] = version;
	};
};

class OpName : public Op
{
public:
	OpName(uint32_t target, std::string name) :
		Op(3 + uint16_t((name.length() + 3) >> 2), OpCode::OpName)
	{
		size_t size = 2 + ((name.length() + 3) >> 2);
		data.resize(size);
		data[0] = target;
		data[size - 1] = 0;
		memcpy(data.data() + 1, name.data(), name.length());
	};
};

class OpString : public Op
{
public:
	OpString(uint32_t target, std::string name) :
		Op(3 + uint16_t((name.length() + 3) >> 2), OpCode::OpString)
	{
		size_t size = 2 + ((name.length() + 3) >> 2);
		data.resize(size);
		data[0] = target;
		memcpy(data.data() + 1, name.data(), name.length());
		data[size - 1] = 0;
	};
};

class OpLine : public Op
{
public:
	OpLine(uint32_t file, uint32_t line, uint32_t column) :
		Op(4, OpCode::OpLine)
	{
		data.resize(3);
		data[0] = file;
		data[1] = line;
		data[2] = column;
	};
};

class OpExtInstImport : public Op
{
public:
	OpExtInstImport(uint32_t target, std::string name) :
		Op(3 + uint16_t((name.length() + 3) >> 2), OpCode::OpExtInstImport)
	{
		size_t size = 2 + ((name.length() + 3) >> 2);
		data.resize(size);
		data[0] = target;
		data[size - 1] = 0;
		memcpy(data.data() + 1, name.data(), name.length());
	};
};

class OpExtInst : public Op
{
public:
	OpExtInst(uint32_t resultType, uint32_t result, uint32_t set, uint32_t instruction, std::vector<uint32_t> operands) :
		Op(5 + (uint16_t)operands.size(), OpCode::OpExtInst)
	{
		data.resize(4);
		data.reserve(words - 1);
		data[0] = resultType;
		data[1] = result;
		data[2] = set;
		data[3] = instruction;
		data.insert(data.end(), operands.begin(), operands.end());
	};
};

class OpCapability : public Op
{
public:
	OpCapability(Capability capability) :
		Op(2, OpCode::OpCapability)
	{
		data.resize(1);
		data[0] = (uint32_t)capability;
	};
};

class OpMemoryModel : public Op
{
public:
	OpMemoryModel(AddressingModel addressingModel, MemoryModel memoryModel) :
		Op(3, OpCode::OpMemoryModel)
	{
		data.resize(2);
		data[0] = (uint32_t)addressingModel;
		data[1] = (uint32_t)memoryModel;
	};
};

class OpEntryPoint : public Op
{
public:
	OpEntryPoint(ExecutionModel executionModel, uint32_t entryPoint, std::string name) :
		Op(4 + uint16_t((name.length() + 3) >> 2), OpCode::OpEntryPoint)
	{
		size_t size = 3 + ((name.length() + 3) >> 2);
		data.resize(size);
		data[0] = (uint32_t)executionModel;
		data[1] = entryPoint;
		data[size - 1] = 0;
		memcpy(data.data() + 2, name.data(), name.length());
	};
};

class OpExecutionMode : public Op
{
public:
	OpExecutionMode(uint32_t entryPoint, ExecutionMode executionMode) :
		Op(3, OpCode::OpExecutionMode)
	{
		data.resize(2);
		data[0] = entryPoint;
		data[1] = (uint32_t)executionMode;
	};
};

class OpTypeVoid : public Op
{
public:
	OpTypeVoid(uint32_t result) :
		Op(2, OpCode::OpTypeVoid)
	{
		data.resize(1);
		data[0] = result;
	};
};

class OpTypeBool : public Op
{
public:
	OpTypeBool(uint32_t result) :
		Op(2, OpCode::OpTypeBool)
	{
		data.resize(1);
		data[0] = result;
	};
};

class OpTypeInt : public Op
{
public:
	OpTypeInt(uint32_t result, uint32_t width, uint32_t _signed) :
		Op(4, OpCode::OpTypeInt)
	{
		data.resize(3);
		data[0] = result;
		data[1] = width;
		data[2] = _signed;
	};
};

class OpTypeFloat : public Op
{
public:
	OpTypeFloat(uint32_t result, uint32_t width) :
		Op(3, OpCode::OpTypeFloat)
	{
		data.resize(2);
		data[0] = result;
		data[1] = width;
	};
};

class OpTypeVector : public Op
{
public:
	OpTypeVector(uint32_t result, uint32_t componentType, uint32_t componentCount) :
		Op(4, OpCode::OpTypeVector)
	{
		data.resize(3);
		data[0] = result;
		data[1] = componentType;
		data[2] = componentCount;
	};
};

class OpTypeMatrix : public Op
{
public:
	OpTypeMatrix(uint32_t result, uint32_t componentType, uint32_t componentCount) :
		Op(4, OpCode::OpTypeMatrix)
	{
		data.resize(3);
		data[0] = result;
		data[1] = componentType;
		data[2] = componentCount;
	};
};

class OpTypeStruct : public Op
{
public:
	OpTypeStruct(uint32_t result, std::vector<uint32_t> memberTypes = {}) :
		Op(2 + uint16_t(memberTypes.size()), OpCode::OpTypeStruct)
	{
		data.resize(1);
		data[0] = result;
		data.insert(data.end(), memberTypes.begin(), memberTypes.end());
	};
};

class OpTypePointer : public Op
{
public:
	OpTypePointer(uint32_t result, StorageClass storage, uint32_t type) :
		Op(4, OpCode::OpTypePointer)
	{
		data.resize(3);
		data[0] = result;
		data[1] = (uint32_t)storage;
		data[2] = type;
	};
};

class OpTypeFunction : public Op
{
public:
	OpTypeFunction(uint32_t result, uint32_t returnType, std::vector<uint32_t> parameters = {}) :
		Op(3 + (uint16_t)parameters.size(), OpCode::OpTypeFunction)
	{
		data.reserve(2 + parameters.size());
		data.resize(2);
		data[0] = result;
		data[1] = returnType;
		data.insert(data.end(), parameters.begin(), parameters.end());
	};
};

class OpConstant : public Op
{
public:
	OpConstant(uint32_t resultType, uint32_t result, uint32_t value) :
		Op(4, OpCode::OpConstant)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = value;
	};
};

class OpFunction : public Op
{
public:
	OpFunction(uint32_t resultType, uint32_t result, FunctionControl functionControl, uint32_t functionType) :
		Op(5, OpCode::OpFunction)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = (uint32_t)functionControl;
		data[3] = functionType;
	};
};

class OpFunctionParameter : public Op
{
public:
	OpFunctionParameter(uint32_t resultType, uint32_t result) :
		Op(3, OpCode::OpFunctionParameter)
	{
		data.resize(2);
		data[0] = resultType;
		data[1] = result;
	};
};

class OpFunctionEnd : public Op
{
public:
	OpFunctionEnd() :
		Op(1, OpCode::OpFunctionEnd)
	{
		data.resize(0);
	};
};

class OpFunctionCall : public Op
{
public:
	OpFunctionCall(uint32_t resultType, uint32_t result, uint32_t function, std::vector<uint32_t> operands) :
		Op(4 + (uint16_t)operands.size(), OpCode::OpFunctionCall)
	{
		data.resize(3);
		data.reserve(words - 1);
		data[0] = resultType;
		data[1] = result;
		data[2] = function;
		data.insert(data.end(), operands.begin(), operands.end());
	};
};

class OpVariable : public Op
{
public:
	OpVariable(uint32_t resultType, uint32_t result, StorageClass storageClass) :
		Op(4, OpCode::OpVariable)
	{
		//TODO: Add support for initializer
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = (uint32_t)storageClass;
	};
};

class OpLoad : public Op
{
public:
	//TODO: Add support for memory operands	
	OpLoad(uint32_t resultType, uint32_t result, uint32_t pointer) :
		Op(4, OpCode::OpLoad)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = pointer;
	};
};

class OpStore : public Op
{
public:
	OpStore(uint32_t pointer, uint32_t object) :
		Op(3, OpCode::OpStore)
	{
		data.resize(2);
		data[0] = pointer;
		data[1] = object;
	};
};

class OpAccessChain : public Op
{
public:
	OpAccessChain(uint32_t resultType, uint32_t result, uint32_t base, std::vector<uint32_t> indexes) :
		Op(4 + (uint16_t)indexes.size(), OpCode::OpAccessChain)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = base;
		data.insert(data.end(), indexes.begin(), indexes.end());
	};
};

class OpVectorShuffle : public Op
{
public:
	OpVectorShuffle(uint32_t resultType, uint32_t result, uint32_t vector1, uint32_t vector2, std::vector<uint32_t> components) :
		Op(5 + (uint16_t)components.size(), OpCode::OpVectorShuffle)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = vector1;
		data[3] = vector2;
		data.insert(data.end(), components.begin(), components.end());
	};
};

class OpCompositeConstruct : public Op
{
public:
	OpCompositeConstruct(uint32_t resultType, uint32_t result, std::vector<uint32_t> constituents) :
		Op(3 + (uint16_t)constituents.size(), OpCode::OpCompositeConstruct)
	{
		data.resize(2);
		data[0] = resultType;
		data[1] = result;
		data.insert(data.end(), constituents.begin(), constituents.end());
	};
};

class OpCompositeExtract : public Op
{
public:
	OpCompositeExtract(uint32_t resultType, uint32_t result, uint32_t composite, std::vector<uint32_t> indexes) :
		Op(4 + (uint16_t)indexes.size(), OpCode::OpCompositeExtract)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = composite;
		data.insert(data.end(), indexes.begin(), indexes.end());
	};
};

class OpConvertFToS : public Op
{
public:
	OpConvertFToS(uint32_t resultType, uint32_t result, uint32_t signedValue) :
		Op(4, OpCode::OpConvertFToS)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = signedValue;
	};
};

class OpConvertSToF : public Op
{
public:
	OpConvertSToF(uint32_t resultType, uint32_t result, uint32_t signedValue) :
		Op(4, OpCode::OpConvertSToF)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = signedValue;
	};
};

class OpSNegate : public Op
{
public:
	OpSNegate(uint32_t resultType, uint32_t result, uint32_t operand) :
		Op(4, OpCode::OpSNegate)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand;
	};
};

class OpFNegate : public Op
{
public:
	OpFNegate(uint32_t resultType, uint32_t result, uint32_t operand) :
		Op(4, OpCode::OpFNegate)
	{
		data.resize(3);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand;
	};
};

class OpIAdd : public Op
{
public:
	OpIAdd(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpIAdd)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFAdd : public Op
{
public:
	OpFAdd(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFAdd)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFSub : public Op
{
public:
	OpFSub(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFSub)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpIMul : public Op
{
public:
	OpIMul(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpIMul)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFMul : public Op
{
public:
	OpFMul(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFMul)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpSDiv : public Op
{
public:
	OpSDiv(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpSDiv)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFDiv : public Op
{
public:
	OpFDiv(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFDiv)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFMod : public Op
{
public:
	OpFMod(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFMod)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpMatrixTimesScaler : public Op
{
public:
	OpMatrixTimesScaler(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpMatrixTimesScaler)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpMatrixTimesVector : public Op
{
public:
	OpMatrixTimesVector(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpMatrixTimesVector)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpVectorTimesMatrix : public Op
{
public:
	OpVectorTimesMatrix(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpVectorTimesMatrix)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpMatrixTimesMatrix : public Op
{
public:
	OpMatrixTimesMatrix(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpMatrixTimesMatrix)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpDot : public Op
{
public:
	OpDot(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpDot)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpLogicalOr : public Op
{
public:
	OpLogicalOr(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpLogicalOr)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpLogicalAnd : public Op
{
public:
	OpLogicalAnd(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpLogicalAnd)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpLogicalNot : public Op
{
public:
	OpLogicalNot(uint32_t resultType, uint32_t result, uint32_t operand1) :
		Op(5, OpCode::OpLogicalNot)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
	};
};


class OpSGreaterThan : public Op
{
public:
	OpSGreaterThan(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpSGreaterThan)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFOrdLessThan : public Op
{
public:
	OpFOrdLessThan(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFOrdLessThan)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFOrdLessThanEqual : public Op
{
public:
	OpFOrdLessThanEqual(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFOrdLessThanEqual)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFOrdGreaterThan : public Op
{
public:
	OpFOrdGreaterThan(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFOrdGreaterThan)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFOrdGreaterThanEqual : public Op
{
public:
	OpFOrdGreaterThanEqual(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFOrdGreaterThanEqual)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpSLessThan : public Op
{
public:
	OpSLessThan(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpSLessThan)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpSLessThanEqual : public Op
{
public:
	OpSLessThanEqual(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpSLessThanEqual)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpFOrdEqual : public Op
{
public:
	OpFOrdEqual(uint32_t resultType, uint32_t result, uint32_t operand1, uint32_t operand2) :
		Op(5, OpCode::OpFOrdEqual)
	{
		data.resize(4);
		data[0] = resultType;
		data[1] = result;
		data[2] = operand1;
		data[3] = operand2;
	};
};

class OpLoopMerge : public Op
{
public:
	OpLoopMerge(uint32_t mergeBlock, uint32_t continueBlock, LoopControl loopControl) :
		Op(4, OpCode::OpLoopMerge)
	{
		data.resize(3);
		data[0] = mergeBlock;
		data[1] = continueBlock;
		data[2] = (uint32_t)loopControl;
	};
};

class OpSelectionMerge : public Op
{
public:
	OpSelectionMerge(uint32_t mergeBlock, SelectionControl selectionControl) :
		Op(3, OpCode::OpSelectionMerge)
	{
		data.resize(2);
		data[0] = mergeBlock;
		data[1] = (uint32_t)selectionControl;
	};
};

class OpLabel : public Op
{
public:
	OpLabel(uint32_t result) :
		Op(2, OpCode::OpLabel)
	{
		data.resize(1);
		data[0] = result;
	};
};

class OpBranch : public Op
{
public:
	OpBranch(uint32_t targetLabel) :
		Op(2, OpCode::OpBranch)
	{
		data.resize(1);
		data[0] = targetLabel;
	};
};

class OpBranchConditional : public Op
{
public:
	OpBranchConditional(uint32_t condition, uint32_t trueLabel, uint32_t falseLabel) :
		Op(4, OpCode::OpBranchConditional)
	{
		data.resize(3);
		data[0] = condition;
		data[1] = trueLabel;
		data[2] = falseLabel;
	};
};

class OpReturn : public Op
{
public:
	OpReturn() :
		Op(1, OpCode::OpReturn)
	{
		data.resize(0);
	};
};

class OpReturnValue : public Op
{
public:
	OpReturnValue(uint32_t id) :
		Op(2, OpCode::OpReturnValue)
	{
		data.resize(1);
		data[0] = id;
	};
};
