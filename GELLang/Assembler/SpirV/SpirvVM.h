// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <vector>
#include <string>
#include <stdint.h>
#include <unordered_map>
#include "SpirvOps.h"
#include <variant>
#include <typeinfo>
#include "../../DataType.h"
#include <filesystem>
#include <concepts>

namespace fs = std::filesystem;

#define STACK_MAX 256

enum class InterpretResult {
    OK,
    COMPILE_ERROR,
    RUNTIME_ERROR
};

struct Chunk {
    std::vector<uint32_t> code;
    void write(uint32_t word);
    
    Chunk();
    Chunk(fs::path loadPath);

    template<typename T>
    void write(T op)
    {
        static_assert(std::is_base_of<Op, T>::value, "T must derive from Op");
        code.push_back(encodeOpCode(op.opCode, op.words));
        code.insert(code.end(), op.begin(), op.end());
        //code.push_back(word);
    }

    template<std::same_as<Chunk> T>
    void write(T c)
    {
        code.insert(code.end(), c.code.begin(), c.code.end());
    }
};

inline void printChunk(const Chunk chunk)
{
    for (size_t i = 0; i < chunk.code.size() * 4; ++i)
    {
        printf("%02x ", ((uint8_t*)chunk.code.data())[i]);
    }
}

struct Lit
{
    DataType* type;
    uint8_t data[32];
};

class SpirvVM
{
    //Chunk* chunk;
    uint32_t* ip;
    std::unordered_map<std::string, uint32_t> literalName2ID;
    std::unordered_map<uint32_t, std::string> literalID2Name;

    size_t lastLine = 0;

    uint32_t ReadWord();
    void registerName(uint32_t, std::string);
public:
    //std::unordered_map<uint32_t, std::variant<DataType, Lit>> results;

    std::string err;

    void disassembleChunk(Chunk* chunk, const char* name);
    size_t disassembleInstruction(Chunk* chunk, size_t offset);

    //void resetStack();
    //void push(Lit lit);
    //Lit pop();
    void Print(const char* name, uint16_t opLength, size_t offset);
    void PrintOpSource(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpName(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpString(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpCapability(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpExtInstImport(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpExtInst(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpMemoryModel(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpEntryPoint(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpExecutionMode(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpLine(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeVoid(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeBool(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeInt(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeFloat(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeVector(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeMatrix(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeStruct(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypePointer(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpTypeFunction(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpUnary(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpConstant(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpFunctionParameter(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpFunction(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpFunctionEnd(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpFunctionCall(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpVariable(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpLoad(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpStore(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpAccessChain(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpVectorShuffle(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpCompositeConstruct(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpCompositeExtract(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpConvertSToF(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpMath(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpLoopMerge(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpSelectionMerge(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpLabel(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpBranch(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpBranchConditional(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpReturn(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    void PrintOpReturnValue(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset);
    InterpretResult run(Chunk* chunk);

    //InterpretResult getLiteral(uint32_t idx, Lit& result);
    //InterpretResult getType(uint32_t idx, Type& result);

    template<typename T>
    InterpretResult get(uint32_t idx, T& result);
    template<typename T>
    InterpretResult get(uint32_t idx, T*& result);

    Lit getLiteral(std::string name);
    std::string getLiteralName(uint32_t idx);
};

template<typename T>
inline InterpretResult SpirvVM::get(uint32_t idx, T& result)
{
    //if (auto value = std::get_if<T>(&results[idx])) {
    //    result = *value;
    //    return InterpretResult::OK;
    //}
    err = std::string("Tried to read value that's not a ") + typeid(T).name() + " as a " + typeid(T).name() + "!";
    return InterpretResult::RUNTIME_ERROR;
}
