// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <stdint.h>
#include <string>
#include <vector>
#include <exception>
#include <stdexcept>
#include "SpirvOpParamTypes.h"
#include "../../Params.h"

enum class AstCode : uint16_t
{
	AstNop			= 0x00,
	AstSource		= 0x03,
	AstName			= 0x05,
	AstString		= 0x07,
	AstLine			= 0x08,
	AstExtInstImport= 0x0B,
	AstExtInst		= 0x0C,
	AstMemoryModel	= 0x0E,
	AstEntryPoint	= 0x0F,
	AstExecutionMode= 0x10,
	AstCapability	= 0x11,
	AstTypeVoid		= 0x13,
	AstTypeBool		= 0x14,
	AstTypeInt		= 0x15,
	AstTypeFloat	= 0x16,
	AstTypeVector	= 0x17,
	AstTypeMatrix	= 0x18,
	AstTypeStruct	= 0x1B,
	AstTypePointer	= 0x20,
	AstTypeFunction	= 0x21,
	AstConstant		= 0x2B,
	AstFunction		= 0x36,
	AstFunctionParameter = 0x37,
	AstFunctionEnd	= 0x38,
	AstFunctionCall	= 0x39,
	AstVariable		= 0x3B,
	AstLoad			= 0x3D,
	AstStore		= 0x3E,
	AstAccessChain  = 0x41,
	AstVectorShuffle = 0x4F,
	AstCompositeConstruct = 0x50,
	AstCompositeExtract = 0x51,
	AstConvertFToS	= 0x6E,
	AstConvertSToF	= 0x6F,
	AstSNegate		= 0x7E,
	AstFNegate		= 0x7F,
	AstIAdd			= 0x80,
	AstFAdd			= 0x81,
	AstFSub			= 0x83,
	AstIMul			= 0x84,
	AstFMul			= 0x85,
	AstSDiv			= 0x87,
	AstFDiv			= 0x88,
	AstFMod			= 0x8D,
	AstMatrixTimesScaler = 0x8F,
	AstVectorTimesMatrix = 0x90,
	AstMatrixTimesVector = 0x91,
	AstMatrixTimesMatrix = 0x92,
	AstDot			= 0x94,
	AstLogicalOr	= 0xA6,
	AstLogicalAnd	= 0xA7,
	AstLogicalNot   = 0xA8,
	AstIEqual		= 0xAA,
	AstSGreaterThan = 0xAD,
	AstSLessThan	= 0xB1,
	AstSLessThanEqual = 0xB3,
	AstFOrdEqual	= 0xB4,
	AstFOrdLessThan = 0xB8,
	AstFOrdLessThanEqual = 0xBC,
	AstFOrdGreaterThan = 0xBA,
	AstFOrdGreaterThanEqual = 0xBE,
	AstLoopMerge	= 0xF6,
	AstSelectionMerge = 0xF7,
	AstLabel		= 0xF8,
	AstBranch		= 0xF9,
	AstBranchConditional = 0xFA,
	AstReturn		= 0xFD,
	AstReturnValue	= 0xFE,
};

class Ref;

class Ast
{
public:
	AstCode opCode;
	uint32_t result;
	Ref* resultType;

	Ast(AstCode opCode) :
		opCode(opCode),
		result(-1),
		resultType(nullptr)
	{};

	Ast(AstCode opCode, Ref* resultType) :
		opCode(opCode),
		result(-1),
		resultType(resultType)
	{};
};

class Ref
{
	Ast* target;
public:
	Ref() : target(nullptr) {};
	Ref(Ast* target) : target(target) {
		if (target == nullptr)
			throw std::logic_error("target is null");
	};

	inline void SetTarget(Ast* target) { this->target = target; };
	inline Ast* GetTarget() { return this->target; };
};

class AstNop : public Ast
{
public:
	AstNop() :
		Ast(AstCode::AstNop)
	{};
};

class AstSource : public Ast
{
	SourceLanguage sourceLanguage;
	uint32_t version;
public:
	AstSource(SourceLanguage sourceLanguage, uint32_t version) :
		Ast(AstCode::AstSource),
		sourceLanguage(sourceLanguage),
		version(version)
	{};
};

class AstName : public Ast
{
public:
	Ref* target;
	std::string name;
	AstName(Ref* target, std::string name) :
		Ast(AstCode::AstName),
		target(target),
		name(name)
	{};
};

class AstString : public Ast
{
public:
	std::string str;
	AstString(std::string str) :
		Ast(AstCode::AstString),
		str(str)
	{};
};

class AstLine : public Ast
{
public:
	Ref* file;
	AstLine(Ref* file, uint32_t line, uint32_t column) :
		Ast(AstCode::AstLine),
		file(file)
	{};
};

class AstExtInstImport : public Ast
{
public:
	std::string name;
	AstExtInstImport(std::string name) :
		Ast(AstCode::AstExtInstImport),
		name(name)
	{};
};

class AstExtInst : public Ast
{
public:
	Ref* set;
	uint32_t instruction;
	std::vector<Ref*> operands;
	AstExtInst(Ref* resultType, Ref* set, uint32_t instruction, std::vector<Ref*> operands) :
		Ast(AstCode::AstExtInst, resultType),
		set(set),
		instruction(instruction),
		operands(operands)
	{};
};

class AstCapability : public Ast
{
public:
	AstCapability(Capability capability) :
		Ast(AstCode::AstCapability)
	{};
};

class AstMemoryModel : public Ast
{
public:
	AstMemoryModel(AddressingModel addressingModel, MemoryModel memoryModel) :
		Ast(AstCode::AstMemoryModel)
	{};
};

class AstEntryPoint : public Ast
{
public:
	ExecutionModel executionModel;
	Ref* entryPoint;
	std::string name;
	AstEntryPoint(ExecutionModel executionModel, Ref* entryPoint, std::string name) :
		Ast(AstCode::AstEntryPoint),
		executionModel(executionModel),
		entryPoint(entryPoint),
		name(name)
	{};
};

class AstExecutionMode : public Ast
{
public:
	Ref* entryPoint;
	ExecutionMode executionMode;
	AstExecutionMode(Ref* entryPoint, ExecutionMode executionMode) :
		Ast(AstCode::AstExecutionMode),
		entryPoint(entryPoint),
		executionMode(executionMode)
	{};
};

class AstTypeVoid : public Ast
{
public:
	AstTypeVoid() :
		Ast(AstCode::AstTypeVoid)
	{};
};

class AstTypeBool : public Ast
{
public:
	AstTypeBool() :
		Ast(AstCode::AstTypeBool)
	{};
};

class AstTypeInt : public Ast
{
public:
	uint32_t width;
	uint32_t _signed;
	AstTypeInt(uint32_t width, uint32_t _signed) :
		Ast(AstCode::AstTypeInt),
		width(width),
		_signed(_signed)
	{};
};

class AstTypeFloat : public Ast
{
public:
	uint32_t width;
	AstTypeFloat(uint32_t width) :
		Ast(AstCode::AstTypeFloat),
		width(width)
	{};
};

class AstTypeVector : public Ast
{
public:
	Ref* componentType;
	uint32_t componentCount;
	AstTypeVector(Ref* componentType, uint32_t componentCount) :
		Ast(AstCode::AstTypeVector),
		componentType(componentType),
		componentCount(componentCount)
	{};
};

class AstTypeMatrix : public Ast
{
public:
	Ref* componentType;
	uint32_t componentCount;
	AstTypeMatrix(Ref* componentType, uint32_t componentCount) :
		Ast(AstCode::AstTypeMatrix),
		componentType(componentType),
		componentCount(componentCount)
	{};
};

class AstTypePointer : public Ast
{
public:
	StorageClass storage;
	Ref* type;
	AstTypePointer(StorageClass storage, Ref* type) :
		Ast(AstCode::AstTypePointer),
		storage(storage),
		type(type)
	{};
};

class AstTypeStruct : public Ast
{
public:
	std::vector<Ref*> members;
	AstTypeStruct(std::vector<Ref*> members = {}) :
		Ast(AstCode::AstTypeStruct),
		members(members)
	{};
};

class AstTypeFunction : public Ast
{
public:
	Ref* returnType;
	std::vector<Ref*> parameters;
	AstTypeFunction(Ref* returnType, std::vector<Ref*> parameters = {}) :
		Ast(AstCode::AstTypeFunction),
		returnType(returnType),
		parameters(parameters)
	{};
};

class AstConstant : public Ast
{
public:
	uint32_t value;
	AstConstant(Ref* resultType, uint32_t value) :
		Ast(AstCode::AstConstant, resultType),
		value(value)
	{};
};

class AstFunctionParameter;

class AstFunction : public Ast
{
public:
	std::string name;
	FunctionControl functionControl;
	Ref* functionType;
	std::vector<Parameter> parameters;
	std::vector<AstFunctionParameter*> astParameters;
	AstFunction(std::string name, Ref* resultType, FunctionControl functionControl, Ref* functionType, std::vector<Parameter> parameters) :
		Ast(AstCode::AstFunction, resultType),
		functionControl(functionControl),
		functionType(functionType),
		name(name),
		parameters(parameters)
	{};
};

class AstFunctionParameter : public Ast
{
public:
	AstFunctionParameter(Ref* resultType) :
		Ast(AstCode::AstFunctionParameter, resultType)
	{};
};

class AstFunctionEnd : public Ast
{
public:
	AstFunctionEnd() :
		Ast(AstCode::AstFunctionEnd)
	{};
};

class AstFunctionCall : public Ast
{
public:
	Ref* function;
	std::vector<Ref*> operands;
	AstFunctionCall(Ref* resultType, Ref* function, std::vector<Ref*> operands) :
		Ast(AstCode::AstFunctionCall, resultType),
		function(function),
		operands(operands)
	{};
};

class AstVariable : public Ast
{
public:
	StorageClass storageClass;
	AstVariable(Ref* resultType, StorageClass storageClass) :
		Ast(AstCode::AstVariable, resultType),
		storageClass(storageClass)
	{};
};

class AstLoad : public Ast
{
public:
	Ref* pointer;
	AstLoad(Ref* resultType, Ref* pointer) :
		Ast(AstCode::AstLoad, resultType),
		pointer(pointer)
	{};
};

class AstStore : public Ast
{
public:
	Ref* pointer;
	Ref* object;
	AstStore(Ref* pointer, Ref* object) :
		Ast(AstCode::AstStore),
		pointer(pointer),
		object(object)
	{};
};

class AstAccessChain : public Ast
{
public:
	Ref* base;
	std::vector<Ref*> indexes;
	AstAccessChain(Ref* resultType, Ref* base, std::vector<Ref*> indexes) :
		Ast(AstCode::AstAccessChain, resultType),
		base(base),
		indexes(indexes)
	{};
};

class AstVectorShuffle : public Ast
{
public:
	Ref* vector1;
	Ref* vector2;
	std::vector<uint32_t> components;
	AstVectorShuffle(Ref* resultType, Ref* vector1, Ref* vector2, std::vector<uint32_t> components) :
		Ast(AstCode::AstVectorShuffle, resultType),
		vector1(vector1),
		vector2(vector2),
		components(components)
	{};
};

class AstCompositeConstruct : public Ast
{
public:
	std::vector<Ref*> constituents;
	AstCompositeConstruct(Ref* resultType, std::vector<Ref*> constituents) :
		Ast(AstCode::AstCompositeConstruct, resultType),
		constituents(constituents)
	{};
};

class AstCompositeExtract : public Ast
{
public:
	Ref* composite;
	std::vector<uint32_t> indexes;
	AstCompositeExtract(Ref* resultType, Ref* composite, std::vector<uint32_t> indexes) :
		Ast(AstCode::AstCompositeExtract, resultType),
		composite(composite),
		indexes(indexes)
	{};
};

class AstConvertFToS : public Ast
{
public:
	Ref* floatValue;
	AstConvertFToS(Ref* resultType, Ref* floatValue) :
		Ast(AstCode::AstConvertFToS, resultType),
		floatValue(floatValue)
	{};
};

class AstConvertSToF : public Ast
{
public:
	Ref* signedValue;
	AstConvertSToF(Ref* resultType, Ref* signedValue) :
		Ast(AstCode::AstConvertSToF, resultType),
		signedValue(signedValue)
	{};
};

class AstSNegate : public Ast
{
public:
	Ref* operand;
	AstSNegate(Ref* resultType, Ref* operand) :
		Ast(AstCode::AstSNegate, resultType),
		operand(operand)
	{};
};

class AstFNegate : public Ast
{
public:
	Ref* operand;
	AstFNegate(Ref* resultType, Ref* operand) :
		Ast(AstCode::AstFNegate, resultType),
		operand(operand)
	{};
};

class AstBinary : public Ast
{
public:
	Ref* operand1;
	Ref* operand2;
	AstBinary(AstCode code, Ref* resultType, Ref* operand1, Ref* operand2) :
		Ast(code, resultType),
		operand1(operand1),
		operand2(operand2)
	{};
};

class AstIAdd : public AstBinary
{
public:
	AstIAdd(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstIAdd, resultType, operand1, operand2)
	{};
};

class AstFAdd : public AstBinary
{
public:
	AstFAdd(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFAdd, resultType, operand1, operand2)
	{};
};

class AstFSub : public AstBinary
{
public:
	AstFSub(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFSub, resultType, operand1, operand2)
	{};
};

class AstIMul : public AstBinary
{
public:
	AstIMul(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstIMul, resultType, operand1, operand2)
	{};
};

class AstFMul : public AstBinary
{
public:
	AstFMul(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFMul, resultType, operand1, operand2)
	{};
};

class AstSDiv : public AstBinary
{
public:
	AstSDiv(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstSDiv, resultType, operand1, operand2)
	{};
};

class AstFDiv : public AstBinary
{
public:
	AstFDiv(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFDiv, resultType, operand1, operand2)
	{};
};

class AstFMod : public AstBinary
{
public:
	AstFMod(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFMod, resultType, operand1, operand2)
	{};
};

class AstMatrixTimesScaler : public AstBinary
{
public:
	AstMatrixTimesScaler(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstMatrixTimesScaler, resultType, operand1, operand2)
	{};
};

class AstVectorTimesMatrix : public AstBinary
{
public:
	AstVectorTimesMatrix(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstVectorTimesMatrix, resultType, operand1, operand2)
	{};
};

class AstMatrixTimesVector : public AstBinary
{
public:
	AstMatrixTimesVector(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstMatrixTimesVector, resultType, operand1, operand2)
	{};
};

class AstMatrixTimesMatrix : public AstBinary
{
public:
	AstMatrixTimesMatrix(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstMatrixTimesMatrix, resultType, operand1, operand2)
	{};
};

class AstDot : public AstBinary
{
public:
	AstDot(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstDot, resultType, operand1, operand2)
	{};
};

class AstLogicalOr : public AstBinary
{
public:
	AstLogicalOr(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstLogicalOr, resultType, operand1, operand2)
	{};
};

class AstLogicalAnd : public AstBinary
{
public:
	AstLogicalAnd(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstLogicalAnd, resultType, operand1, operand2)
	{};
};

class AstLogicalNot : public Ast
{
public:
	Ref* operand1;
	AstLogicalNot(Ref* resultType, Ref* operand1) :
		Ast(AstCode::AstLogicalNot, resultType),
		operand1(operand1)
	{};
};

class AstIEqual : public AstBinary
{
public:
	AstIEqual(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstIEqual, resultType, operand1, operand2)
	{};
};

class AstSGreaterThan : public AstBinary
{
public:
	AstSGreaterThan(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstSGreaterThan, resultType, operand1, operand2)
	{};
};

class AstSLessThan : public AstBinary
{
public:
	AstSLessThan(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstSLessThan, resultType, operand1, operand2)
	{};
};

class AstSLessThanEqual : public AstBinary
{
public:
	AstSLessThanEqual(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstSLessThanEqual, resultType, operand1, operand2)
	{};
};

class AstFOrdEqual : public AstBinary
{
public:
	AstFOrdEqual(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFOrdEqual, resultType, operand1, operand2)
	{};
};

class AstFOrdLessThan : public AstBinary
{
public:
	AstFOrdLessThan(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFOrdLessThan, resultType, operand1, operand2)
	{};
};

class AstFOrdLessThanEqual : public AstBinary
{
public:
	AstFOrdLessThanEqual(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFOrdLessThanEqual, resultType, operand1, operand2)
	{};
};

class AstFOrdGreaterThan : public AstBinary
{
public:
	AstFOrdGreaterThan(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFOrdGreaterThan, resultType, operand1, operand2)
	{};
};

class AstFOrdGreaterThanEqual : public AstBinary
{
public:
	AstFOrdGreaterThanEqual(Ref* resultType, Ref* operand1, Ref* operand2) :
		AstBinary(AstCode::AstFOrdGreaterThanEqual, resultType, operand1, operand2)
	{};
};

class AstLoopMerge : public Ast
{
public:
	Ref* mergeBlock;
	Ref* continueBlock;
	LoopControl loopControl;
	AstLoopMerge(Ref* mergeBlock, Ref* continueBlock, LoopControl loopControl) :
		Ast(AstCode::AstLoopMerge),
		mergeBlock(mergeBlock),
		continueBlock(continueBlock),
		loopControl(loopControl)
	{};
};

class AstSelectionMerge : public Ast
{
public:
	Ref* mergeBlock;
	SelectionControl selectionControl;
	AstSelectionMerge(Ref* mergeBlock, SelectionControl selectionControl) :
		Ast(AstCode::AstSelectionMerge),
		mergeBlock(mergeBlock),
		selectionControl(selectionControl)
	{};
};

class AstLabel : public Ast
{
public:
	AstLabel() :
		Ast(AstCode::AstLabel)
	{};
};

class AstBranch : public Ast
{
public:
	Ref* targetLabel;
	AstBranch(Ref* targetLabel) :
		Ast(AstCode::AstBranch),
		targetLabel(targetLabel)
	{};
};

class AstBranchConditional : public Ast
{
public:
	Ref* condition;
	Ref* trueLabel;
	Ref* falseLabel;
	AstBranchConditional(Ref* condition, Ref* trueLabel, Ref* falseLabel) :
		Ast(AstCode::AstBranchConditional),
		condition(condition),
		trueLabel(trueLabel),
		falseLabel(falseLabel)
	{};
};

class AstReturn : public Ast
{
public:
	AstReturn() :
		Ast(AstCode::AstReturn)
	{};
};

class AstReturnValue : public Ast
{
public:
	Ref* value;
	AstReturnValue(Ref* value) :
		Ast(AstCode::AstReturnValue),
		value(value)
	{};
};
