// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "SpirvVM.h"
#include <stdint.h>
#include <unordered_map>
#include "../../utilities.h"

uint32_t SpirvVM::ReadWord()
{
    return *ip++;
}

void SpirvVM::registerName(uint32_t id, std::string name)
{
    literalName2ID.insert_or_assign(name, id);
    literalID2Name.insert_or_assign(id, name);
}

void Chunk::write(uint32_t word)
{
    code.push_back(word);
}

Chunk::Chunk(){}

Chunk::Chunk(fs::path loadPath)
{
    size_t dataLen = 0;
    const char* data = readFile(loadPath.generic_string().c_str(), &dataLen);
    this->code.resize(dataLen / 4);
    memcpy(this->code.data(), data, dataLen);
}

void SpirvVM::disassembleChunk(Chunk* chunk, const char* name)
{
    printf("== %s ==\n", name);

    for (size_t offset = 0; offset < chunk->code.size();) {
        offset = disassembleInstruction(chunk, offset);
    }
}

void SpirvVM::Print(const char* name, uint16_t opLength, size_t offset) {
    printf("%s\n", name);
}

void SpirvVM::PrintOpSource(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    uint32_t sourceLanguage = chunk->code[offset + 1];
    uint32_t version = chunk->code[offset + 2];
    printf("%s %d %d\n", opName, sourceLanguage, version);
}

void SpirvVM::PrintOpName(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    uint32_t targetID = chunk->code[offset + 1];
    const char* name = (const char*)&chunk->code[offset + 2];
    std::string target = getLiteralName(targetID);
    registerName(targetID, std::string(name));
    printf("%s <%s> %s\n", opName, target.c_str(), name);
}

void SpirvVM::PrintOpString(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    std::string target = getLiteralName(chunk->code[offset + 1]);
    const char* str = (const char*)&chunk->code[offset + 2];
    printf("<%s> %s \"%s\"\n", target.c_str(), opName, str);
}

void SpirvVM::PrintOpExtInstImport(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string target = getLiteralName(chunk->code[offset + 1]);
    const char* name = (const char*)&chunk->code[offset + 2];
    printf("%s <%s> \"%s\"\n", opName, target.c_str(), name);
}

void SpirvVM::PrintOpExtInst(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string set = getLiteralName(chunk->code[offset + 3]);
    uint32_t instruction = chunk->code[offset + 4];

    printf("<%s> %s <%s> <%s> %d", result.c_str(), opName, resultType.c_str(), set.c_str(), instruction);

    for (int i = 5; i < opLength; i++)
    {
        std::string operand = getLiteralName(chunk->code[offset + i]);
        printf(" <%s>", operand.c_str());
    }
    printf("\n");
}

void SpirvVM::PrintOpCapability(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    Capability capability = (Capability)chunk->code[offset + 1];
    printf("%s %d\n", opName, capability);
}

void SpirvVM::PrintOpMemoryModel(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    AddressingModel addressingModel = (AddressingModel)chunk->code[offset + 1];
    MemoryModel memoryModel = (MemoryModel)chunk->code[offset + 2];
    printf("%s %d %d\n", opName, addressingModel, memoryModel);
}

void SpirvVM::PrintOpEntryPoint(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    ExecutionModel executionModel = (ExecutionModel)chunk->code[offset + 1];
    std::string entryPoint = getLiteralName(chunk->code[offset + 2]);
    const char* name = (const char*)&chunk->code[offset + 3];
    printf("%s %d <%s> \"%s\"\n", opName, executionModel, entryPoint.c_str(), name);
}

void SpirvVM::PrintOpExecutionMode(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string entryPoint = getLiteralName(chunk->code[offset + 1]);
    ExecutionMode executionMode = (ExecutionMode)chunk->code[offset + 2];
    printf("%s <%s> %d\n", opName, entryPoint.c_str(), executionMode);
}

void SpirvVM::PrintOpLine(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string file = getLiteralName(chunk->code[offset + 1]);
    uint32_t line = chunk->code[offset + 2];
    uint32_t column = chunk->code[offset + 3];
    printf("%s <%s> %d %d\n", opName, file.c_str(), line, column);
}

void SpirvVM::PrintOpTypeVoid(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    uint32_t resultID = chunk->code[offset + 1];
    registerName(resultID, "void");
    std::string result = getLiteralName(resultID);
    printf("<%s> %s\n", result.c_str(), opName);
}

void SpirvVM::PrintOpTypeBool(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    uint32_t resultID = chunk->code[offset + 1];
    registerName(resultID, "bool");
    std::string result = getLiteralName(resultID);
    printf("<%s> %s\n", result.c_str(), opName);
}

void SpirvVM::PrintOpTypeInt(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    uint32_t resultId = chunk->code[offset + 1];
    uint32_t width = chunk->code[offset + 2];
    uint32_t _signed = chunk->code[offset + 3];
    
    char buffer[10];
    snprintf(buffer, sizeof(buffer), "%s%d_t", _signed ? "int" : "uint", width);
    registerName(resultId, std::string(buffer));
    
    std::string result = getLiteralName(resultId);
    printf("<%s> %s %d %d\n", result.c_str(), opName, width, _signed);
}

void SpirvVM::PrintOpTypeFloat(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    uint32_t resultId = chunk->code[offset + 1];
    uint32_t width = chunk->code[offset + 2];

    char buffer[10];
    snprintf(buffer, sizeof(buffer), "f%d_t", width);
    registerName(resultId, std::string(buffer));

    std::string result = getLiteralName(resultId);
    printf("<%s> %s %d\n", result.c_str(), opName, width);
}

void SpirvVM::PrintOpTypeVector(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    uint32_t resultId = chunk->code[offset + 1];
    std::string componentType = getLiteralName(chunk->code[offset + 2]);
    uint32_t componentWidth = chunk->code[offset + 3];

    char buffer[128];
    snprintf(buffer, sizeof(buffer), "v%d%s", componentWidth, componentType.c_str());
    registerName(resultId, std::string(buffer));

    std::string result = getLiteralName(resultId);
    printf("<%s> %s <%s> %d\n", result.c_str(), opName, componentType.c_str(), componentWidth);
}

void SpirvVM::PrintOpTypeMatrix(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    uint32_t resultId = chunk->code[offset + 1];
    std::string componentType = getLiteralName(chunk->code[offset + 2]);
    uint32_t componentWidth = chunk->code[offset + 3];

    char buffer[128];
    snprintf(buffer, sizeof(buffer), "m%d%s", componentWidth, componentType.c_str());
    registerName(resultId, std::string(buffer));

    std::string result = getLiteralName(resultId);
    printf("<%s> %s <%s> %d\n", result.c_str(), opName, componentType.c_str(), componentWidth);
}

void SpirvVM::PrintOpTypeStruct(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    std::string result = getLiteralName(chunk->code[offset + 1]);
    printf("<%s> %s", result.c_str(), opName);
    for (int i = 2; i < opLength; ++i)
    {
        std::string index = getLiteralName(chunk->code[offset + i]);
        printf(" <%s>", index.c_str());
    }
    printf("\n");
}

void SpirvVM::PrintOpTypePointer(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    std::string result = getLiteralName(chunk->code[offset + 1]);
    uint32_t storageClass = chunk->code[offset + 2];
    std::string type = getLiteralName(chunk->code[offset + 3]);
    printf("<%s> %s %d <%s>\n", result.c_str(), opName, storageClass, type.c_str());
}

void SpirvVM::PrintOpVariable(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    uint32_t storageClass = chunk->code[offset + 3];
    printf("<%s> %s <%s> storageClass:%d\n", result.c_str(), opName, resultType.c_str(), storageClass);
}

void SpirvVM::PrintOpLoad(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string pointer = getLiteralName(chunk->code[offset + 3]);
    printf("<%s> %s <%s> <%s>\n", result.c_str(), opName, resultType.c_str(), pointer.c_str());
}

void SpirvVM::PrintOpStore(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string pointer = getLiteralName(chunk->code[offset + 1]);
    std::string object = getLiteralName(chunk->code[offset + 2]);
    printf("%s <%s> <%s>\n", opName, pointer.c_str(), object.c_str());
}

void SpirvVM::PrintOpAccessChain(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string base = getLiteralName(chunk->code[offset + 3]);
    printf("<%s> %s <%s> <%s>", result.c_str(), opName, resultType.c_str(), base.c_str());
    for (int i = 4; i < opLength; ++i)
    {
        std::string index = getLiteralName(chunk->code[offset + i]);
        printf(" <%s>", index.c_str());
    }
    printf("\n");
}

void SpirvVM::PrintOpVectorShuffle(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string vector1 = getLiteralName(chunk->code[offset + 3]);
    std::string vector2 = getLiteralName(chunk->code[offset + 4]);
    printf("<%s> %s <%s> <%s> <%s>", result.c_str(), opName, resultType.c_str(), vector1.c_str(), vector2.c_str());
    for (int i = 5; i < opLength; ++i)
    {
        printf(" %d", chunk->code[offset + i]);
    }
    printf("\n");
}

void SpirvVM::PrintOpCompositeConstruct(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    printf("<%s> %s <%s>", result.c_str(), opName, resultType.c_str());
    for (int i = 3; i < opLength; ++i)
    {
        std::string parameter = getLiteralName(chunk->code[offset + i]);
        printf(" <%s>", parameter.c_str());
    }
    printf("\n");
}

void SpirvVM::PrintOpCompositeExtract(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string composite = getLiteralName(chunk->code[offset + 3]);
    printf("<%s> %s <%s> <%s>", result.c_str(), opName, resultType.c_str(), composite.c_str());
    for (int i = 4; i < opLength; ++i)
    {
        printf(" %d", chunk->code[offset + i]);
    }
    printf("\n");
}

void SpirvVM::PrintOpConvertSToF(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string signedValue = getLiteralName(chunk->code[offset + 3]);
    printf("<%s> %s <%s> <%s>\n", result.c_str(), opName, resultType.c_str(), signedValue.c_str());
}

void SpirvVM::PrintOpTypeFunction(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    std::string result = getLiteralName(chunk->code[offset + 1]);
    std::string returnType = getLiteralName(chunk->code[offset + 2]);
    printf("<%s> %s <%s>", result.c_str(), opName, returnType.c_str());
    for (int i = 3; i < opLength; ++i)
    {
        std::string parameter = getLiteralName(chunk->code[offset + i]);
        printf(" <%s>", parameter.c_str());
    }
    printf("\n");
}

void SpirvVM::PrintOpUnary(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    std::string type = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string operand = getLiteralName(chunk->code[offset + 3]);
    printf("<%s> %s <%s> <%s>\n", result.c_str(), opName, type.c_str(), operand.c_str());
}

void SpirvVM::PrintOpMath(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    std::string type = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string operand1 = getLiteralName(chunk->code[offset + 3]);
    std::string operand2 = getLiteralName(chunk->code[offset + 4]);
    printf("<%s> %s <%s> <%s> <%s>\n", result.c_str(), opName, type.c_str(), operand1.c_str(), operand2.c_str());
}

void SpirvVM::PrintOpLoopMerge(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string mergeBlock = getLiteralName(chunk->code[offset + 1]);
    std::string continueBlock = getLiteralName(chunk->code[offset + 2]);
    uint32_t loopControl = chunk->code[offset + 3];
    printf("%s <%s> <%s> %d\n", opName, mergeBlock.c_str(), continueBlock.c_str(), loopControl);
}

void SpirvVM::PrintOpSelectionMerge(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string mergeBlock = getLiteralName(chunk->code[offset + 1]);
    uint32_t selectionControl = chunk->code[offset + 2];
    printf("%s <%s> %d\n", opName, mergeBlock.c_str(), selectionControl);
}

void SpirvVM::PrintOpLabel(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string result = getLiteralName(chunk->code[offset + 1]);
    printf("<%s> %s\n", result.c_str(), opName);
}

void SpirvVM::PrintOpBranch(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string targetLabel = getLiteralName(chunk->code[offset + 1]);
    printf("%s <%s>\n", opName, targetLabel.c_str());
}

void SpirvVM::PrintOpBranchConditional(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string condition = getLiteralName(chunk->code[offset + 1]);
    std::string trueBlock = getLiteralName(chunk->code[offset + 2]);
    std::string falseBlock = getLiteralName(chunk->code[offset + 3]);
    printf("%s <%s> <%s> <%s>\n", opName, condition.c_str(), trueBlock.c_str(), falseBlock.c_str());
}

void SpirvVM::PrintOpReturn(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    printf("%s\n", opName);
}

void SpirvVM::PrintOpReturnValue(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string value = getLiteralName(chunk->code[offset + 1]);
    printf("%s <%s>\n", opName, value.c_str());
}

void SpirvVM::PrintOpConstant(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset) {
    std::string type = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    uint32_t lit = chunk->code[offset + 3];
    printf("<%s> %s <%s> literal:%d\n", result.c_str(), opName, type.c_str(), lit);
}

void SpirvVM::PrintOpFunction(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
 
    uint32_t functionControl = chunk->code[offset + 3];
    std::string type = getLiteralName(chunk->code[offset + 4]);
    printf("<%s> %s <%s> %d <%s>\n", result.c_str(), opName, resultType.c_str(), functionControl, type.c_str());
}

void SpirvVM::PrintOpFunctionParameter(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    printf("<%s> %s <%s>\n", result.c_str(), opName, resultType.c_str());
}

void SpirvVM::PrintOpFunctionEnd(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    printf("%s\n", opName);
}

void SpirvVM::PrintOpFunctionCall(const char* opName, uint16_t opLength, Chunk* chunk, size_t offset)
{
    std::string resultType = getLiteralName(chunk->code[offset + 1]);
    std::string result = getLiteralName(chunk->code[offset + 2]);
    std::string function = getLiteralName(chunk->code[offset + 3]);
    printf("<%s> %s <%s> <%s>", result.c_str(), opName, resultType.c_str(), function.c_str());
    for (int i = 4; i < opLength; ++i)
    {
        std::string argument = getLiteralName(chunk->code[offset + i]);
        printf(" <%s>", argument.c_str());
    }
    printf("\n");
}

#define PRINT(x) case OpCode::x: Print##x(#x, opCodeWords, chunk, offset); break
#define PRINT2(x, y) case OpCode::x: Print##y(#x, opCodeWords, chunk, offset); break
size_t SpirvVM::disassembleInstruction(Chunk* chunk, size_t offset)
{

    uint16_t opCodeWords;
    OpCode instruction;
    decodeOpCode(chunk->code[offset], instruction, opCodeWords);

	printf("-------| ");
	size_t offsetMax = offset + opCodeWords;
	for(size_t idx=offset; idx < offsetMax; idx++) {
		printf("0x%08X ", chunk->code[idx]);
	}

    printf("\n0x%04zX | ", offset);

    switch (instruction) {
        PRINT(OpSource);
        PRINT(OpName);
        PRINT(OpString);
        PRINT(OpExtInstImport);
        PRINT(OpExtInst);
        PRINT(OpCapability);
        PRINT(OpMemoryModel);
        PRINT(OpEntryPoint);
        PRINT(OpExecutionMode);
        PRINT(OpLine);
        PRINT(OpTypeVoid);
        PRINT(OpTypeBool);
        PRINT(OpTypeInt);
        PRINT(OpTypeFloat);
        PRINT(OpTypeVector);
        PRINT(OpTypeMatrix);
        PRINT(OpTypeStruct);
        PRINT(OpTypePointer);
        PRINT(OpTypeFunction);
        PRINT(OpConstant);
        PRINT(OpFunction);
        PRINT(OpFunctionParameter);
        PRINT(OpFunctionEnd);
        PRINT(OpFunctionCall);
        PRINT(OpVariable);
        PRINT(OpLoad);
        PRINT(OpStore);
        PRINT(OpAccessChain);
        PRINT(OpVectorShuffle);
        PRINT(OpCompositeConstruct);
        PRINT(OpCompositeExtract);
        PRINT(OpConvertSToF);
        PRINT2(OpSNegate, OpUnary);
        PRINT2(OpFNegate, OpUnary);
        PRINT2(OpIAdd, OpMath);
        PRINT2(OpFAdd, OpMath);
        PRINT2(OpFSub, OpMath);
        PRINT2(OpIMul, OpMath);
        PRINT2(OpFMul, OpMath);
        PRINT2(OpSDiv, OpMath);
        PRINT2(OpFDiv, OpMath);
        PRINT2(OpFMod, OpMath);
        PRINT2(OpMatrixTimesScaler, OpMath);
        PRINT2(OpVectorTimesMatrix, OpMath);
        PRINT2(OpMatrixTimesVector, OpMath);
        PRINT2(OpMatrixTimesMatrix, OpMath);
        PRINT2(OpDot, OpMath);
        PRINT2(OpSGreaterThan, OpMath);
        //PRINT2(OpSGreaterThanEqual, OpMath);
        PRINT2(OpSLessThan, OpMath);
        PRINT2(OpSLessThanEqual, OpMath);
        PRINT2(OpFOrdEqual, OpMath);
        PRINT2(OpFOrdLessThan, OpMath);
        PRINT2(OpFOrdLessThanEqual, OpMath);
        PRINT2(OpFOrdGreaterThan, OpMath);
        PRINT(OpLoopMerge);
        PRINT(OpSelectionMerge);
        PRINT(OpLabel);
        PRINT(OpBranch);
        PRINT(OpBranchConditional);
        PRINT(OpReturn);
        PRINT(OpReturnValue);
        default: printf("Unknown opcode 0x%X\n", instruction); break;
    }
    return offset + opCodeWords;
}

Lit SpirvVM::getLiteral(std::string name)
{
    return {};// literalName2ID[name];
}

std::string SpirvVM::getLiteralName(uint32_t idx)
{
    auto found = literalID2Name.find(idx);
    if (found == literalID2Name.end()) return std::to_string(idx);
    return found->second;
}

InterpretResult ExOpTypeVoid(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t result = chunk->code[offset + 1];
    DataType* type = new DataTypeVoid();
    //vm.results.insert_or_assign(result, type);
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult ExOpTypeBool(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t result = chunk->code[offset + 1];
    DataType* type = new DataTypeBool();
    //vm.results.insert_or_assign(result, type);
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult ExOpTypeInt(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t result = chunk->code[offset + 1];
    uint32_t width = chunk->code[offset + 2];
    uint32_t _signed = chunk->code[offset + 3];
    DataTypeInt* type = new DataTypeInt(width, _signed);
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult ExOpTypePointer(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t result = chunk->code[offset + 1];
    uint32_t storageClass = chunk->code[offset + 2];
    uint32_t typeID = chunk->code[offset + 3];
    
    DataType* subType = nullptr;

    DataType* ptype = new DataTypePointer(StorageClass::Private, subType);
    //vm.results.insert_or_assign(result, ptype);
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult ExOpTypeFunction(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t result = chunk->code[offset + 1];
    uint32_t returnTypeId = chunk->code[offset + 2];
    
    DataType* returnType = nullptr;
    std::vector<DataType*> paramTypes;
    
    DataType* ptype = new DataTypeFunction(returnType, paramTypes);
    //vm.results.insert_or_assign(result, ptype);
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult ExOpConstant(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t typeID = chunk->code[offset + 1];
    uint32_t result = chunk->code[offset + 2];
    uint32_t value = chunk->code[offset + 3];
    //Type type;
    //InterpretResult res;
    //if ((res = vm.get(typeID, type)) != InterpretResult::OK) return res;
    //Lit _literal = Lit(type);
    //_literal.write(value);
    //vm.results.insert_or_assign(result, _literal);
    //return InterpretResult::OK;
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult ExOpFunction(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    return InterpretResult::OK;
}

InterpretResult ExOpVariable(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t resultType = chunk->code[offset + 1];
    uint32_t result = chunk->code[offset + 2];
    uint32_t storageClass = chunk->code[offset + 3];
    //Type type;
    //InterpretResult res;
    //if ((res = vm.get(resultType, type)) != InterpretResult::OK) return res;
    //Lit _literal = Lit(type);
    //vm.results.insert_or_assign(result, _literal);
    //return InterpretResult::OK;
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult ExOpIAdd(SpirvVM& vm, Chunk* chunk, size_t offset)
{
    uint32_t typeID = chunk->code[offset + 1];
    uint32_t result = chunk->code[offset + 2];
    uint32_t op1 = chunk->code[offset + 3];
    uint32_t op2 = chunk->code[offset + 4];
    DataType* type = nullptr;
    //if ((res = vm.get(typeID, type)) != InterpretResult::OK) return res;
    //Lit _literal = Lit(type);
    //
    //Lit operand1, operand2;
    //if ((res = vm.get(op1, operand1)) != InterpretResult::OK) return res;
    //if ((res = vm.get(op2, operand2)) != InterpretResult::OK) return res;
    //
    //if (!evalOpIAdd(type, _literal.data(type.size), operand1.data(type.size), operand2.data(type.size)))
    //    return InterpretResult::COMPILE_ERROR;

    //vm.results.insert_or_assign(result, _literal);
    return InterpretResult::RUNTIME_ERROR;
}

InterpretResult SpirvVM::run(Chunk* chunk)
{
#define READ_WORD() (*ip++)
    InterpretResult res = InterpretResult::OK;
    for (size_t offset = 0; offset < chunk->code.size();) {
        OpCode instruction;
        uint16_t opCodeWords;
        decodeOpCode(chunk->code[offset], instruction, opCodeWords);

        switch (instruction) {
        case OpCode::OpName: break; //Skip all names for now
        case OpCode::OpTypeVoid: res = ExOpTypeVoid(*this, chunk, offset); break;
        case OpCode::OpTypeBool: res = ExOpTypeBool(*this, chunk, offset); break;
        case OpCode::OpTypeInt: res = ExOpTypeInt(*this, chunk, offset); break;
        case OpCode::OpTypePointer: res = ExOpTypePointer(*this, chunk, offset); break;
        case OpCode::OpTypeFunction: res = ExOpTypeFunction(*this, chunk, offset); break;
        case OpCode::OpConstant: res = ExOpConstant(*this, chunk, offset); break;
        case OpCode::OpFunction: res = ExOpFunction(*this, chunk, offset); break;
        case OpCode::OpVariable: res = ExOpVariable(*this, chunk, offset); break;
        case OpCode::OpIAdd: res = ExOpIAdd(*this, chunk, offset); break;
        default: {
            err = "Unknown OpCode (" + std::to_string((uint16_t)instruction) + ")";
            return InterpretResult::RUNTIME_ERROR;
        } break;
        }

        if (res != InterpretResult::OK)
        {
            return res;
        }

        offset += opCodeWords;
    }
#undef READ_WORD
    return  InterpretResult::OK;
}