// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include "SpirvCompiler.h"

namespace SpirV
{
	class Assembler
	{
		uint32_t flattenResult = 0;
		std::vector<std::tuple<FuncDef, AstFunction*>> funcs;

        AstList list;
        Chunk flatten(Ast* ast);
        Chunk flatten(AstList list);
        uint32_t NextResult();
        uint32_t GetID(Ast* ast, bool generateIfNull = true);
        uint32_t GetID(Ref* ref, bool generateIfNull = false);
	public:
		uint32_t PeekResult();
		Assembler(AstList list, std::vector<std::tuple<FuncDef, AstFunction*>> funcs);
		Chunk Process();
	};
}