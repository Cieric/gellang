// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "SpirvCompiler.h"
#include "../CompilerUtils.h"
#include "../../hash.h"
#include <tuple>
#include <limits>
#include <string>
#include <cassert>
#include <unordered_map>

namespace SpirV
{
    std::vector<std::tuple<FuncDef, AstFunction*>> Compiler::GetFuncs()
    {
        return funcs;
    }

    Compiler::Compiler(Program* program) :
        program(program)
	{}


    DataType* GetType(Ast* _ast)
    {
        switch (_ast->opCode)
        {
        case AstCode::AstTypeBool: {
            AstTypeBool* ast = (AstTypeBool*)_ast;
            return new DataTypeBool();
        } break;
        case AstCode::AstTypeInt: {
            AstTypeInt* ast = (AstTypeInt*)_ast;
            return new DataTypeInt(ast->width, ast->_signed);
        } break;
        case AstCode::AstTypeFloat: {
            AstTypeFloat* ast = (AstTypeFloat*)_ast;
            return new DataTypeFloat(ast->width);
        } break;
        case AstCode::AstTypeVector: {
            AstTypeVector* ast = (AstTypeVector*)_ast;
            return new DataTypeVector(GetType(ast->componentType->GetTarget()), ast->componentCount);
        } break;
        case AstCode::AstTypeMatrix: {
            AstTypeMatrix* ast = (AstTypeMatrix*)_ast;
            return new DataTypeMatrix(GetType(ast->componentType->GetTarget()), ast->componentCount);
        } break;
        case AstCode::AstTypePointer: {
            AstTypePointer* ast = (AstTypePointer*)_ast;
            return new DataTypePointer(ast->storage, GetType(ast->type->GetTarget()));
        } break;
        case AstCode::AstTypeFunction: {
            AstTypeFunction* ast = (AstTypeFunction*)_ast;
            std::vector<DataType*> ParameterTypes;
            for (auto param : ast->parameters)
                ParameterTypes.push_back(GetType(param->GetTarget()));
            return new DataTypeFunction(
                GetType(ast->returnType->GetTarget()),
                ParameterTypes
            );
        } break;
        case AstCode::AstFunctionParameter:
        case AstCode::AstVariable:
        case AstCode::AstAccessChain:
        case AstCode::AstVectorShuffle:
            return GetType(_ast->resultType->GetTarget());
        }
        CompilerError("GetType passed unhandled type!");
    }

    DataType* GetResultType(Ast* ast)
    {
        switch (ast->opCode)
        {
        case AstCode::AstString:
            return new DataTypeString();
        case AstCode::AstLogicalOr:
        case AstCode::AstLogicalAnd:
        case AstCode::AstSLessThan:
        case AstCode::AstFOrdGreaterThan:
        case AstCode::AstFOrdLessThan:
            return new DataTypeBool{ DataType::BOOL };
        }
        return GetType(ast->resultType->GetTarget());
    }

    DataType* Compiler::GetExprValueType(Expr* expr)
    {
        switch (expr->type)
        {
        case Expr::UNARY: {
            ExprUnary* unary = (ExprUnary*)expr;
            return GetExprValueType(unary->right);
        }
        case Expr::BINARY: {
            ExprBinary* bin = (ExprBinary*)expr;
            auto leftType = GetExprValueType(bin->left);
            auto rightType = GetExprValueType(bin->right);
            switch (bin->op.type)
            {
            case Token::EQL_EQL:
            case Token::PIPE_PIPE:
            case Token::AMP_AMP:
            case Token::LESS:
            case Token::LESS_EQL:
            case Token::GREAT:
            case Token::GREAT_EQL:
                return typeFromString("bool");
            }
            if (isVector(leftType) && isMatrix(rightType) || isVector(rightType) && isMatrix(leftType))
                return (leftType->type > rightType->type) ? rightType : leftType;
            return (leftType->type > rightType->type) ? leftType : rightType;
        }
        case Expr::ASSIGN: {
            ExprAssign* assign = (ExprAssign*)expr;
            return GetExprValueType(assign->value);
        }
        case Expr::GROUPING: {
            ExprGrouping* group = (ExprGrouping*)expr;
            auto inside = GetExprValueType(group->expr);
            return inside;
        }
        case Expr::SWIZZLE: {
            ExprSwizzle* swiz = (ExprSwizzle*)expr;
            int len = (int)swiz->swizzle.text.length();
            if (len == 1) return typeFromString("float");
            return typeFromString("vec" + std::to_string(len));
        }
        case Expr::MEMBER: {
            ExprMember* memb = (ExprMember*)expr;
            auto variable = GetExprValueType(memb->variable);
            if (variable->type != DataType::STRUCT)
                CompilerError("Non struct can't have a member");
            auto stuctVariable = (DataTypeStruct*)variable;
            StmtStruct* structDef = structDefs[stuctVariable->name];
            for (auto member : structDef->members)
                if (member->name == memb->member.text)
                    return member->type;
            CompilerError("This shouldn't be hitable!");
        }
        case Expr::SELECTION: {
            ExprSelection* select = (ExprSelection*)expr;
            return GetExprValueType(select->then);
        }
        case Expr::CALL: {
            ExprCall* call = (ExprCall*)expr;
            std::vector<DataType*> params;
            for (auto arg : call->arguments)
                params.push_back(GetExprValueType(arg));
            auto func = getFunc(call->callee.text, params);
            //if (func == nullptr)
            //{
            //    func = getUpcastedFunc(call->callee.text, params);
            //}
            if (func == nullptr)
                CompilerError("Failed to find function!");
            return GetType(func->resultType->GetTarget());
        } break;
        case Expr::VARIABLE: {
            ExprVariable* var = (ExprVariable*)expr;
            return var->symbolTableId->type;
        }
        case Expr::LITERAL: {
            ExprLiteral* lit = (ExprLiteral*)expr;
            if (lit->lit.type == Token::FLOAT)
                return typeFromString("float");
            if (lit->lit.type == Token::INTEGER)
                return typeFromString("int");
            if (lit->lit.type == Token::STRING)
                return typeFromString("string");
            printf("Unsupported literal type: %s", lit->lit.text.c_str());
            return nullptr;
        }
        case Expr::POSTSCRIPT: {
            ExprPostscript* lit = (ExprPostscript*)expr;
            return GetExprValueType(lit->variable);
        }
        default: 
            CompilerError("Unhandled!");
        }
    }

    AstFunction* Compiler::getFunc(std::string name, std::vector<DataType*> params)
    {
        for (int i = 0; i < funcs.size(); ++i)
        {
            bool skip = false;
            auto funcDef = std::get<FuncDef>(funcs[i]);
            if (name != funcDef.name) continue;
            if (funcDef.varargs == false)
            {
                if (params.size() != funcDef.params.size()) continue;
                for (int i = 0; i < params.size(); ++i)
                {
                    auto callParam = params[i];
                    auto funcParam = funcDef.params[i];
                    if (funcParam->castable)
                    {
                        if (!DataTypeUpcastable(callParam, funcParam))
                        {
                            skip = true;
                            break;
                        }
                    }
                    else
                    {
                        if (!DataTypesEqual(callParam, funcParam))
                        {
                            skip = true;
                            break;
                        }
                    }
                }
                if (skip) continue;
            }
            return std::get<AstFunction*>(funcs[i]);
        }

        return nullptr;
    }

    //AstFunction* Compiler::getUpcastedFunc(std::string name, std::vector<DataType*> params)
    //{
    //    for (int i = 0; i < funcs.size(); ++i)
    //    {
    //        bool skip = false;
    //        auto funcDef = std::get<FuncDef>(funcs[i]);
    //        if (name != funcDef.name) continue;
    //        if (funcDef.varargs == true) continue;
    //        if (params.size() != funcDef.params.size()) continue;
    //        for (int i = 0; i < params.size(); ++i)
    //        {
    //            auto param = params[i];
    //            auto storedParam = funcDef.params[i];
    //            if (!DataTypeUpcastable(param, storedParam))
    //            {
    //                skip = true;
    //                break;
    //            }
    //        }
    //        if (skip) continue;
    //        return std::get<AstFunction*>(funcs[i]);
    //    }
    //
    //    return nullptr;
    //}

    ExtFunc* Compiler::getExtFunc(std::string name, std::vector<DataType*> params)
    {
        for (int i = 0; i < extFuncs.size(); ++i)
        {
            bool skip = false;
            auto funcDef = std::get<FuncDef>(extFuncs[i]);
            if (name != funcDef.name) continue;
            if (funcDef.varargs == false)
            {
                if (params.size() != funcDef.params.size()) continue;
                for (int i = 0; i < params.size(); ++i)
                {
                    auto param = params[i];
                    auto storedParam = funcDef.params[i];
                    if (!DataTypesEqual(param, storedParam))
                    {
                        skip = true;
                        break;
                    }
                    if (skip) continue;
                }
            }
            return &std::get<ExtFunc>(extFuncs[i]);
        }
        
        return nullptr;
    }

    void Compiler::addFunc(std::string name, std::vector<DataType*> params, bool varargs, AstFunction* func)
    {
        if (varargs)
        {
            funcs.push_back({ FuncDef{
                name,
                varargs
            }, func });
        }
        else
        {
            funcs.push_back({ FuncDef{
                name,
                params,
            }, func });
        }
    }

    Ast* Compiler::Type2Ast(DataType* type, AstList& list)
    {
        if (type->constant) CompilerError("Consts are currently broken!");

        for (int i = 0; i < types.size(); ++i)
        {
            auto type2 = std::get<DataType*>(types[i]);
            auto ast = std::get<Ast*>(types[i]);
            if (DataTypesEqual(type, type2))
            {
                return ast;
            }
        }
        switch (type->type)
        {
        case DataType::VOID: {
            DataTypeVoid* _type = (DataTypeVoid*)type;
            auto temp = new AstTypeVoid();
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::BOOL: {
            DataTypeBool* _type = (DataTypeBool*)type;
            auto temp = new AstTypeBool();
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::INT: {
            DataTypeInt* _type = (DataTypeInt*)type;
            auto temp = new AstTypeInt(_type->Width, _type->Signedness);
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::FLOAT: {
            DataTypeFloat* _type = (DataTypeFloat*)type;
            auto temp = new AstTypeFloat(_type->Width);
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::VECTOR: {
            DataTypeVector* _type = (DataTypeVector*)type;
            auto temp = new AstTypeVector(new Ref(Type2Ast(_type->ComponentType, list)), _type->ComponentCount);
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::MATRIX: {
            DataTypeMatrix* _type = (DataTypeMatrix*)type;
            auto temp = new AstTypeMatrix(new Ref(Type2Ast(_type->ComponentType, list)), _type->ComponentCount);
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::POINTER: {
            DataTypePointer* _type = (DataTypePointer*)type;
            auto temp = new AstTypePointer(_type->storageClass, new Ref(Type2Ast(_type->ComponentType, list)));
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::STRUCT: {
            DataTypeStruct* _type = (DataTypeStruct*)type;
            std::vector<Ref*> memberTypes;
            for (auto member : _type->members)
            {
                auto typeAst = Type2Ast(member, list);
                memberTypes.push_back(new Ref(typeAst));
            }
            auto temp = new AstTypeStruct(memberTypes);
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        case DataType::FUNCTION: {
            DataTypeFunction* _type = (DataTypeFunction*)type;
            Ast* returnType = Type2Ast(_type->ReturnType, list);
            std::vector<Ref*> paramTypes;
            for (auto paramType : _type->ParameterTypes)
            {
                auto typeAst = Type2Ast(paramType, list);
                paramTypes.push_back(new Ref(typeAst));
            }
            auto temp = new AstTypeFunction(new Ref(returnType), paramTypes);
            types.push_back({ type, temp });
            list.push_back(temp);
            return temp;
        }
        default:
            CompilerError("Error!");
        }

        return nullptr;
    }
    
    DataType* convert(Token::Type type)
    {
        switch (type)
        {
        case Token::Type::KEYWORD:
        case Token::Type::BOOLEAN: return new DataTypeBool();
        case Token::Type::INTEGER: return new DataTypeInt(32, 1);
        case Token::Type::FLOAT: return new DataTypeFloat(32);
        default:
            CompilerError("Error!");
        }
    }

    std::string string_replace(const std::string& s, const std::string& findS, const std::string& replaceS)
    {
        std::string result = s;
        auto pos = s.find(findS);
        if (pos == std::string::npos) {
            return result;
        }
        result.replace(pos, findS.length(), replaceS);
        return string_replace(result, findS, replaceS);
    }

    std::string ParseEscapeCodes(std::string s)
    {
        static std::vector< std::pair< std::string, std::string > > patterns = {
            { "\\\\" , "\\" },
            { "\\n", "\n" },
            { "\\r", "\r" },
            { "\\t", "\t" },
            { "\\\"", "\"" }
        };
        std::string result = s;
        for (const auto& p : patterns) {
            result = string_replace(result, p.first, p.second);
        }
        return result;
    }
    
    AstBinary* Compiler::GetMathOpMatrix(Token::Type type, DataType* left, DataType* right)
    {
        if (left->type == DataType::MATRIX) left = getBaseDataType(left);
        if (right->type == DataType::MATRIX) right = getBaseDataType(right);
        if (DataTypesEqual(left, right))
        {
            switch (type)
            {
            case Token::Type::PLUS:
                break;
            case Token::Type::MINUS:
                break;
            case Token::Type::STAR:
                if (left->type == DataType::FLOAT) return new AstMatrixTimesMatrix(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::SLASH:
                break;
            }
        }
        return nullptr;
    }

    AstBinary* Compiler::GetMathOpVector(Token::Type type, DataType* left, DataType* right)
    {
        if (left->type == DataType::VECTOR) left = getBaseDataType(left);
        if (right->type == DataType::VECTOR) right = getBaseDataType(right);
        if (DataTypesEqual(left, right))
        {
            switch (type)
            {
            case Token::Type::PLUS:
                if (left->type == DataType::FLOAT) return new AstFAdd(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::MINUS:
                if (left->type == DataType::FLOAT) return new AstFSub(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::STAR:
                if (left->type == DataType::FLOAT) return new AstFMul(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::SLASH:
                if (left->type == DataType::FLOAT) return new AstFDiv(new Ref(), new Ref(), new Ref());
                break;
            }
        }
        return nullptr;
    }

    AstBinary* Compiler::GetMathOp(Token::Type type, DataType* left, DataType* right)
    {
        if (left->type == DataType::POINTER) left = ((DataTypePointer*)left)->ComponentType;
        if (right->type == DataType::POINTER) right = ((DataTypePointer*)right)->ComponentType;

        if (DataTypesEqual(left, right)) {}
        else if (DataTypeUpcastable(left, right)) left = right;
        else if (DataTypeUpcastable(right, left)) right = left;

        if (DataTypesEqual(left, right))
        {
            if (left->type == DataType::VECTOR) return GetMathOpVector(type, left, right);
            if (left->type == DataType::MATRIX) return GetMathOpMatrix(type, left, right);

            switch (type)
            {
            case Token::Type::EQL_EQL:
                if (left->type == DataType::INT) return new AstIEqual(new Ref(), new Ref(), new Ref());
                if (left->type == DataType::FLOAT) return new AstFOrdEqual(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::PLUS:
                if (left->type == DataType::INT) return new AstIAdd(new Ref(), new Ref(), new Ref());
                if (left->type == DataType::FLOAT) return new AstFAdd(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::MINUS:
                if (left->type == DataType::FLOAT) return new AstFSub(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::STAR:
                if (left->type == DataType::FLOAT) return new AstFMul(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::SLASH:
                if (left->type == DataType::INT) return new AstSDiv(new Ref(), new Ref(), new Ref());
                if (left->type == DataType::FLOAT) return new AstFDiv(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::LESS:
                if (left->type == DataType::INT) return new AstSLessThan(new Ref(), new Ref(), new Ref());
                if (left->type == DataType::FLOAT) return new AstFOrdLessThan(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::LESS_EQL:
                if (left->type == DataType::INT) return new AstSLessThanEqual(new Ref(), new Ref(), new Ref());
                if (left->type == DataType::FLOAT) return new AstFOrdLessThanEqual(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::GREAT:
                if (left->type == DataType::INT) return new AstSGreaterThan(new Ref(), new Ref(), new Ref());
                if (left->type == DataType::FLOAT) return new AstFOrdGreaterThan(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::GREAT_EQL:
                if (left->type == DataType::FLOAT) return new AstFOrdGreaterThanEqual(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::AMP_AMP:
                if (left->type == DataType::BOOL) return new AstLogicalAnd(new Ref(), new Ref(), new Ref());
                break;
            case Token::Type::PIPE_PIPE:
                if (left->type == DataType::BOOL) return new AstLogicalOr(new Ref(), new Ref(), new Ref());
                break;
            }
        }
        if (isMatrix(left) && isVector(right))
            return new AstMatrixTimesVector(new Ref(), new Ref(), new Ref());
        if (isVector(left) && isMatrix(right))
            return new AstVectorTimesMatrix(new Ref(), new Ref(), new Ref());
        if ((isMatrix(left) && isScaler(right)) || (isScaler(left) && isMatrix(right)))
            return new AstMatrixTimesScaler(new Ref(), new Ref(), new Ref());
        return nullptr;
    }

    void Compiler::ProcessUpcast(AstBinary* op, AstList& list)
    {
        DataType* op1Type = GetResultType(op->operand1->GetTarget());
        DataType* op2Type = GetResultType(op->operand2->GetTarget());
        if (DataTypesEqual(op1Type, op2Type)) return;

        switch (op->opCode)
        {
        case AstCode::AstFOrdEqual:
        case AstCode::AstFAdd:
        case AstCode::AstFSub: 
        case AstCode::AstFMul:
        case AstCode::AstFDiv: {
            if (isVector(op1Type) ^ isVector(op2Type))
            {
                if (!isVector(op1Type))
                {
                    auto newType = Copy(op2Type);
                    setBaseType(newType, DataType::FLOAT);
                    op->operand1->SetTarget(Cast(op->operand1->GetTarget(), op1Type, newType, list));
                }
                else
                {
                    auto newType = Copy(op1Type);
                    setBaseType(newType, DataType::FLOAT);
                    op->operand2->SetTarget(Cast(op->operand2->GetTarget(), op2Type, newType, list));
                }
            }

            if (getBaseType(op1Type) != DataType::FLOAT)
            {//Fix Op 1 to be a scaler float
                auto newType = Copy(op1Type);
                setBaseType(newType, DataType::FLOAT);
                op->operand1->SetTarget(Cast(op->operand1->GetTarget(), op1Type, newType, list));
            }
            if (getBaseType(op2Type) != DataType::FLOAT)
            {//Fix Op 2 to be a scaler float
                auto newType = Copy(op2Type);
                setBaseType(newType, DataType::FLOAT);
                op->operand2->SetTarget(Cast(op->operand2->GetTarget(), op2Type, newType, list));
            }
        } break;

        case AstCode::AstFOrdLessThan:
        case AstCode::AstFOrdGreaterThan:
        case AstCode::AstLogicalAnd:
        case AstCode::AstLogicalOr: {
            if (isVector(op1Type) ^ isVector(op2Type))
            {
                if (!isVector(op1Type))
                {
                    auto newType = Copy(op2Type);
                    setBaseType(newType, DataType::BOOL);
                    op->operand1->SetTarget(Cast(op->operand1->GetTarget(), op1Type, newType, list));
                }
                else
                {
                    auto newType = Copy(op1Type);
                    setBaseType(newType, DataType::BOOL);
                    op->operand2->SetTarget(Cast(op->operand2->GetTarget(), op2Type, newType, list));
                }
            }

            if (getBaseType(op1Type) != DataType::BOOL)
            {//Fix Op 1 to be a scaler float
                auto newType = Copy(op1Type);
                setBaseType(newType, DataType::BOOL);
                op->operand1->SetTarget(Cast(op->operand1->GetTarget(), op1Type, newType, list));
            }
            if (getBaseType(op2Type) != DataType::BOOL)
            {//Fix Op 2 to be a scaler float
                auto newType = Copy(op2Type);
                setBaseType(newType, DataType::BOOL);
                op->operand2->SetTarget(Cast(op->operand2->GetTarget(), op2Type, newType, list));
            }
        } break;
        case AstCode::AstMatrixTimesScaler: {
            if (!isMatrix(op1Type) || !isScaler(op2Type)) CompilerError("But how?");
        } break;
        case AstCode::AstMatrixTimesVector: {
            if (!isMatrix(op1Type) || !isVector(op2Type)) CompilerError("But how?");
        } break;
        case AstCode::AstVectorTimesMatrix: {
            if (!isVector(op1Type) || !isMatrix(op2Type)) CompilerError("But how?");
        } break;
        case AstCode::AstMatrixTimesMatrix: {
            if (!isMatrix(op1Type) || !isMatrix(op2Type)) CompilerError("But how?");
        } break;
        default:
            CompilerError("Unknown casting information for type: %d\n", op->opCode);
        }
    }

    Ast* Compiler::ParseExprBinary(ExprBinary* exprBinary, AstList& list)
    {
        DataType* lOpType = GetExprValueType(exprBinary->left);
        DataType* rOpType = GetExprValueType(exprBinary->right);
        AstBinary* node = GetMathOp(exprBinary->op.type, lOpType, rOpType);

        if (node->opCode == AstCode::AstMatrixTimesScaler && (isScaler(lOpType) && isMatrix(rOpType)))
        {
            auto temp = exprBinary->left;
            exprBinary->left = exprBinary->right;
            exprBinary->right = temp;
            auto temp2 = lOpType;
            lOpType = rOpType;
            rOpType = temp2;
        }

        auto lOpTypeB = (lOpType->type == DataType::POINTER) ? ((DataTypePointer*)lOpType)->ComponentType : lOpType;
        auto rOpTypeB = (rOpType->type == DataType::POINTER) ? ((DataTypePointer*)rOpType)->ComponentType : rOpType;

        DataType* resultType = (lOpTypeB->type > rOpTypeB->type) ? lOpTypeB : rOpTypeB;
        if (lOpTypeB->type == DataType::VECTOR && rOpTypeB->type == DataType::MATRIX)
            resultType = lOpTypeB;
        if (lOpTypeB->type == DataType::MATRIX && rOpTypeB->type == DataType::VECTOR)
            resultType = rOpTypeB;

        node->resultType->SetTarget(Type2Ast(resultType, list));
        node->operand1->SetTarget(ParseExpr(exprBinary->left, list));
        node->operand2->SetTarget(ParseExpr(exprBinary->right, list));

        ProcessUpcast(node, list);

        list.push_back(node);
        return node;
    }

    Ast* Compiler::ParseExprLiteral(ExprLiteral* expr, AstList& list)
    {
        std::string& text = expr->lit.text;
        if (expr->lit.type == Token::STRING) {
            std::string text = ParseEscapeCodes(expr->lit.text);
            auto temp = new AstString(text);
            list.push_back(temp);
            return temp;
        }
        uint32_t value;
        switch (expr->lit.type)
        {
        case Token::KEYWORD:
            value = expr->lit.text == "true";
            break;
        case Token::INTEGER:
            value = stoi(expr->lit.text);
            break;
        case Token::FLOAT:
            *((float*)&value) = stof(expr->lit.text);
            break;
        default:
            CompilerError("Error!");
        }
        Ast* type = Type2Ast(convert(expr->lit.type), list);
        auto _const = new AstConstant(new Ref(type), value);
        list.push_back(_const);
        // auto var = new AstVariable(new Ref(type), StorageClass::Function);
        // list.push_back(var);
        // auto store = new AstStore(new Ref(var), new Ref(_const));
        // list.push_back(store);
        //auto nop = new AstNop();
        //list.push_back(nop);
        return _const;
    }

    Ast* Compiler::ParseExprUnaryPlusPlus(Expr* exprOp, AstList& list)
    {
        DataType* rOpType = GetExprValueType(exprOp);

        switch (getBaseType(rOpType))
        {
        case DataType::INT: {
            DataType* resultType = getBaseDataType(rOpType);
            Ast* resultTypeAst = Type2Ast(resultType, list);
            Ast* op1 = ParseAssignableExpr(exprOp, list);
            auto _const = new AstConstant(new Ref(resultTypeAst), 1);
            list.push_back(_const);
            auto op = new AstIAdd(new Ref(resultTypeAst), new Ref(op1), new Ref(_const));
            list.push_back(op);
            auto store = new AstStore(new Ref(op1), new Ref(op));
            list.push_back(store);
            return op1;
        }break;
        case DataType::FLOAT: {
            DataType* resultType = getBaseDataType(rOpType);
            Ast* resultTypeAst = Type2Ast(resultType, list);
            Ast* op1 = ParseAssignableExpr(exprOp, list);
            float temp = 1.0;
            auto _const = new AstConstant(new Ref(resultTypeAst), *(int*)&temp);
            list.push_back(_const);
            auto op = new AstFAdd(new Ref(resultTypeAst), new Ref(op1), new Ref(_const));
            list.push_back(op);
            auto store = new AstStore(new Ref(op1), new Ref(op));
            list.push_back(store);
            return op1;
        }break;
        }

        CompilerError("Unhandled Operator!");
        return nullptr;
    }

    Ast* Compiler::ParseExprUnaryMinus(Expr* exprOp, AstList& list)
    {
        DataType* rOpType = GetExprValueType(exprOp);

        switch (getBaseType(rOpType))
        {
        case DataType::FLOAT: {
            DataType* resultType = getBaseDataType(rOpType);
            Ast* resultTypeAst = Type2Ast(resultType, list);
            Ast* op1 = ParseExpr(exprOp, list);
            auto op = new AstFNegate(new Ref(resultTypeAst), new Ref(op1));
            list.push_back(op);
            return op;
        }break;
        case DataType::INT: {
            DataType* resultType = getBaseDataType(rOpType);
            Ast* resultTypeAst = Type2Ast(resultType, list);
            Ast* op1 = ParseExpr(exprOp, list);
            auto op = new AstSNegate(new Ref(resultTypeAst), new Ref(op1));
            list.push_back(op);
            return op;
        }break;
        }

        CompilerError("Unhandled Operator!");
        return nullptr;
    }


    Ast* Compiler::ParseExprUnaryBang(Expr* exprOp, AstList& list)
    {
        DataType* rOpType = GetExprValueType(exprOp);

        switch (getBaseType(rOpType))
        {
        case DataType::BOOL: {
            DataType* resultType = getBaseDataType(rOpType);
            Ast* resultTypeAst = Type2Ast(resultType, list);
            Ast* op1 = ParseExpr(exprOp, list);
            auto op = new AstLogicalNot(new Ref(resultTypeAst), new Ref(op1));
            list.push_back(op);
            return op;
        }break;
        }

        CompilerError("Unhandled Operator!");
        return nullptr;
    }

    Ast* Compiler::ParseExprUnary(ExprUnary* exprUnary, AstList& list)
    {
        switch (exprUnary->lit.type)
        {
        case Token::PLUS_PLUS:
            return ParseExprUnaryPlusPlus(exprUnary->right, list);
        case Token::MINUS:
            return ParseExprUnaryMinus(exprUnary->right, list);
        case Token::BANG:
            return ParseExprUnaryBang(exprUnary->right, list);
        }
        CompilerError("Unhandled Operator!");
        return nullptr;
    }

    Ast* Compiler::ParseExprVariable(ExprVariable* exprVariable, AstList& list)
    {
        Ast* variable = *varMap->get(exprVariable->name.text);
        return variable;
    }

    Ast* Compiler::Cast(Ast* ast, DataType* fromType, DataType* toType, AstList& list)
    {
        if (fromType->type == DataType::INT && toType->type == DataType::FLOAT)
        {
            auto type = Type2Ast(toType, list);
            auto temp = new AstConvertSToF(new Ref(type), new Ref(ast));
            list.push_back(temp);
            return temp;
        }

        if (fromType->type == DataType::INT && toType->type == DataType::INT)
            return ast;

        if (toType->type == DataType::VECTOR && DataTypesEqual(fromType, getBaseDataType(toType)))
        {
            Ref* r = new Ref(ast);
            auto type = new Ref(Type2Ast(toType, list));
            std::vector<Ref*> refs(((DataTypeVector*)toType)->ComponentCount, r);
            auto temp = new AstCompositeConstruct(type, refs);
            list.push_back(temp);
            return temp;
        }

        CompilerError("Failed to convert!");
    }

    Ast* Compiler::ParseExprAssign(ExprAssign* exprAssign, AstList& list)
    {
        AstList temp;
        auto name = ParseAssignableExpr(exprAssign->name, temp);
        auto value = ParseExpr(exprAssign->value, list);
        {
            auto nameType = GetType(name);
            auto valueType = GetResultType(value);
            if (name->opCode != AstCode::AstVectorShuffle &&
                !DataTypesEqual(nameType, valueType) &&
                (nameType->type != DataType::POINTER ||
                    !DataTypesEqual(((DataTypePointer*)nameType)->ComponentType, valueType)))
            {

                CompilerError("Casting not supported on an assignment");
                //value = Cast(value, valueType, nameType, list);
                return nullptr;
            }
        }
        if (name->opCode == AstCode::AstVectorShuffle)
        {
            ((AstVectorShuffle*)name)->vector2->SetTarget(value);
        }
        list.insert(list.end(), temp.begin(), temp.end());
        switch (name->opCode)
        {
        case AstCode::AstAccessChain:
        case AstCode::AstVariable:
        case AstCode::AstFunctionParameter:
            list.push_back(new AstStore(new Ref(name), new Ref(value)));
        }
        return name;
    }

    Ast* Compiler::ParseExprCall(ExprCall* expr, AstList& list)
    {
        //AstList argList;
        std::vector<Ref*> refs;
        std::vector<DataType*> paramTypes;
        for (Expr* arg : expr->arguments)
        {
            refs.push_back(new Ref());
            paramTypes.push_back(GetExprValueType(arg));
        }
        {
            auto function = getFunc(expr->callee.text, paramTypes);
            if (function != nullptr)
            {
                std::vector<std::pair<Ast*, Ast*>> outParams;
                for (int i=0; i<expr->arguments.size(); ++i)
                {
                    auto& outParam = function->parameters[i];
                    Expr* arg = expr->arguments[i];
                    Ast* param;
                    if (outParam.copySpecifier != CopySpecifier::IN)
                    {
                        param = ParseAssignableExpr(arg, list);
                        DataType* type = GetResultType(param);
                        DataType* ptrType = new DataTypePointer(StorageClass::Function, type);
                        auto ptrTypeAst = Type2Ast(ptrType, list);
                        auto newParam = new AstVariable(new Ref(ptrTypeAst), StorageClass::Output);
                        list.push_back(newParam);
                        auto store = new AstStore(new Ref(newParam), new Ref(param));
                        list.push_back(store);
                        outParams.push_back({ param, newParam });
                        param = newParam;
                    }
                    else
                    {
                        param = ParseExpr(arg, list);
                        DataType* type = GetResultType(param);
                        if (!DataTypesEqual(outParam.type, type))
                        {
                            if (outParam.type->castable)
                            {
                                auto casted = Cast(param, type, outParam.type, list);
                                auto nTypeAst = Type2Ast(outParam.type, list);
                                param = new AstVariable(new Ref(nTypeAst), StorageClass::Function);
                                list.push_back(param);
                                list.push_back(new AstStore(new Ref(param), new Ref(casted)));
                            }
                            else
                            {
                                CompilerError("Incorrect Type passed to function!");
                            }
                        }
                    }
                    refs[i]->SetTarget(param);
                }

                auto funcCall = new AstFunctionCall(
                    function->resultType,
                    new Ref(function),
                    refs
                );
                list.push_back(funcCall);

                for (auto outParam : outParams)
                {
                    auto type = GetResultType(outParam.first);
                    auto typeAst = Type2Ast(type, list);
                    auto load = new AstLoad(new Ref(typeAst), new Ref(outParam.second));
                    list.push_back(load);
                    auto store = new AstStore(new Ref(outParam.first), new Ref(load));
                    list.push_back(store);
                }
                
                //for (int paramIdx = 0; paramIdx < funcParams.size(); ++paramIdx)
                //{
                //    if (outParam.copySpecifier != CopySpecifier::IN)
                //    {
                //        auto outType = Type2Ast(outParam.type, list);
                //        auto name = ParseAssignableExpr(funcParam, list);
                //        auto value = new AstLoad(new Ref(outType), new Ref(function->astParameters[paramIdx]));
                //        list.push_back(value);
                //        switch (name->opCode)
                //        {
                //        case AstCode::AstVectorShuffle:
                //            ((AstVectorShuffle*)name)->vector2->SetTarget(value);
                //            break;
                //        case AstCode::AstAccessChain:
                //        case AstCode::AstVariable:
                //        case AstCode::AstFunctionParameter:
                //            list.push_back(new AstStore(new Ref(name), new Ref(value)));
                //        }
                //    }
                //}
                return funcCall;
            }
        }
        //{
        //    auto function = getUpcastedFunc(expr->callee.text, paramTypes);
        //    if (function != nullptr)
        //    {
        //        DataTypeFunction* funcType = (DataTypeFunction*)GetType(function->functionType->GetTarget());
        //        for (int i = 0; i < refs.size(); ++i)
        //        {
        //            auto ref = refs[i];
        //            auto refType = GetResultType(ref->GetTarget());
        //            auto paramType = funcType->ParameterTypes[i];
        //            if (!DataTypesEqual(refType, paramType))
        //            {
        //                ref->SetTarget(Cast(ref->GetTarget(), refType, paramType, list));
        //            }
        //        }
        //        auto funcCall = new AstFunctionCall(
        //            function->resultType,
        //            new Ref(function),
        //            refs
        //        );
        //        list.push_back(funcCall);
        //        return funcCall;
        //    }
        //}
        //{
        //    auto extFunc = getExtFunc(expr->callee.text, paramTypes);
        //    if (extFunc != nullptr)
        //    {
        //        auto funcCall = new AstExtInst(
        //            new Ref(extFunc->returnType),
        //            new Ref(extFunc->extention),
        //            extFunc->funcId,
        //            refs
        //        );
        //        list.push_back(funcCall);
        //        return funcCall;
        //    }
        //}
        CompilerError("Failed to find function!");
        return nullptr;
    }

    Ast* Compiler::ParseExprSwizzle(ExprSwizzle* exprSwizzle, AstList& list)
    {
        std::string swizzle = exprSwizzle->swizzle.text;
        std::vector<Ast*> extractedValues;
        auto value = ParseExpr(exprSwizzle->variable, list);
        DataType* valueType = GetExprValueType(exprSwizzle);
        Ast* valueTypeAst = Type2Ast(valueType, list);
        auto singleType = getBaseDataType(valueType);
        Ast* singleTypeAst = Type2Ast(singleType, list);

        for (char c : swizzle)
        {
            switch (c)
            {
            case 'x': case 'r': case 'u':
                extractedValues.push_back(new AstCompositeExtract(new Ref(singleTypeAst), new Ref(value), { 0 }));
                break;
            case 'y': case 'g': case 'v':
                extractedValues.push_back(new AstCompositeExtract(new Ref(singleTypeAst), new Ref(value), { 1 }));
                break;
            case 'z': case 'b': case 's':
                extractedValues.push_back(new AstCompositeExtract(new Ref(singleTypeAst), new Ref(value), { 2 }));
                break;
            case 'w': case 'a': case 't':
                extractedValues.push_back(new AstCompositeExtract(new Ref(singleTypeAst), new Ref(value), { 3 }));
                break;
            }
        }
        if (swizzle.size() == 1)
        {
            auto extracted = extractedValues[0];
            list.push_back(extracted);
            return extracted;
        }
        std::vector<Ref*> refs;
        for (auto value : extractedValues)
        {
            list.push_back(value);
            refs.push_back(new Ref(value));
        }
        auto composite = new AstCompositeConstruct(new Ref(valueTypeAst), refs);
        list.push_back(composite);
        return composite;
    }

    Ast* Compiler::ParseExprMember(ExprMember* exprMember, AstList& list)
    {
        std::string member = exprMember->member.text;
        auto valueAst = ParseExpr(exprMember->variable, list);
        DataType* valueType = GetExprValueType(exprMember->variable);

        if (valueType->type != DataType::STRUCT)
            CompilerError("Non struct can't have a member");
        auto stuctVariable = (DataTypeStruct*)valueType;
        StmtStruct* structDef = structDefs[stuctVariable->name];
        Symbol* memberSymbol = nullptr;
        uint32_t i = 0;
        for (; i < structDef->members.size(); ++i)
            if (structDef->members[i]->name == exprMember->member.text)
            {
                memberSymbol = structDef->members[i];
                break;
            }
        if(memberSymbol == nullptr)
            CompilerError("This shouldn't be hitable!");

        Ast* memberTypeAst = Type2Ast(memberSymbol->type, list);
        DataType* memberPtrType = new DataTypePointer(StorageClass::Function, memberSymbol->type);
        Ast* memberPtrTypeAst = Type2Ast(memberPtrType, list);

        Ast* intType = Type2Ast(typeFromString("int"), list);
        Ast* constant = new AstConstant(new Ref(intType), i);
        list.push_back(constant);

        auto composite = new AstAccessChain(new Ref(memberPtrTypeAst), new Ref(valueAst), { new Ref(constant) });
        list.push_back(composite);
        auto load = new AstLoad(new Ref(memberTypeAst), new Ref(composite));
        list.push_back(load);
        return load;
    }

    Ast* Compiler::ParseExprPostscriptPlusPlus(Expr* exprOp, AstList& list)
    {
        DataType* rOpType = GetExprValueType(exprOp);

        switch (getBaseType(rOpType))
        {
        case DataType::INT: {
            auto left = ParseAssignableExpr(exprOp, list);
            auto datatype = GetExprValueType(exprOp);
            auto type = Type2Ast(datatype, list);
            auto const_1 = new AstConstant(new Ref(type), 1);
            list.push_back(const_1);
            auto inc = new AstIAdd(new Ref(type), new Ref(left), new Ref(const_1));
            list.push_back(inc);
            auto store = new AstStore(new Ref(left), new Ref(inc));
            list.push_back(store);
            return left;
        }break;
        case DataType::FLOAT: {
            auto left = ParseAssignableExpr(exprOp, list);
            auto datatype = GetExprValueType(exprOp);
            auto type = Type2Ast(datatype, list);
            float temp = 1.0f;
            auto const_1 = new AstConstant(new Ref(type), *(int32_t*)&temp);
            list.push_back(const_1);
            auto inc = new AstFAdd(new Ref(type), new Ref(left), new Ref(const_1));
            list.push_back(inc);
            auto store = new AstStore(new Ref(left), new Ref(inc));
            list.push_back(store);
            return left;
        }break;
        }

        CompilerError("Unhandled Operator!");
        return nullptr;
    }

    Ast* Compiler::ParseExprPostscript(ExprPostscript* exprPostScript, AstList& list)
    {
        switch (exprPostScript->op.type)
        {
        case Token::PLUS_PLUS: {
            return ParseExprPostscriptPlusPlus(exprPostScript->variable, list);
        }
        }
        CompilerError("Failed to handle Operator!");
        return nullptr;
    }

    Ast* Compiler::ParseExprSelection(ExprSelection* exprSelection, AstList& list)
    {
        DataType* valueType = GetExprValueType(exprSelection);
        auto type = Type2Ast(valueType, list);
        //%21 = AstVariable %_ptr_Function_float Function
        auto var = new AstVariable(new Ref(type), StorageClass::Function);
        list.push_back(var);

        auto trueLabel = new AstLabel();
        auto elseLabel = new AstLabel();
        auto lastLabel = new AstLabel();
        ////Test
        //%16 = AstLoad %float %test
        //%19 = AstFOrdGreaterThan %bool %16 %float_0_5
        //      AstSelectionMerge %23 None
        //      AstBranchConditional %19 %22 %29
        auto condition = ParseExpr(exprSelection->test, list);
        auto merge = new AstSelectionMerge(new Ref(lastLabel), SelectionControl::None);
        list.push_back(merge);
        auto conditional = new AstBranchConditional(new Ref(condition), new Ref(trueLabel), new Ref(elseLabel));
        list.push_back(conditional);

        ////Then
        //%22 = AstLabel
        //%27 = AstAccessChain %_ptr_Output_float %fragColor %uint_0
        //%28 = AstLoad %float %27
        //      AstStore %21 %28
        //      AstBranch %23
        list.push_back(trueLabel);
        auto trueExpr = ParseExpr(exprSelection->then, list);
        list.push_back(new AstStore(new Ref(var), new Ref(trueExpr)));
        list.push_back(new AstBranch(new Ref(lastLabel)));

        ////Else
        //%29 = AstLabel
        //%31 = AstAccessChain %_ptr_Output_float %fragColor %uint_1
        //%32 = AstLoad %float %31
        //      AstStore %21 %32
        //      AstBranch %23
        list.push_back(elseLabel);
        auto falseExpr = ParseExpr(exprSelection->elseThen, list);
        list.push_back(new AstStore(new Ref(var), new Ref(falseExpr)));
        list.push_back(new AstBranch(new Ref(lastLabel)));

        ////End
        //%23 = AstLabel
        list.push_back(lastLabel);

        return var;
    }

    Ast* Compiler::ParseExprSequence(ExprSequence* exprSequence, AstList& list)
    {
        Ast* ret = nullptr;
        assert(exprSequence->sequence.size() > 0);
        for (auto expr : exprSequence->sequence)
        {
            ret = ParseExpr(expr, list);
        }
        return ret;
    }

    Ast* Compiler::ParseExpr(Expr* expr, AstList& list)
    {
        switch (expr->type)
        {
        case Expr::BINARY: {
            ExprBinary* _expr = (ExprBinary*)expr;
            return ParseExprBinary(_expr, list);
        } break;
        case Expr::LITERAL: {
            ExprLiteral* _expr = (ExprLiteral*)expr;
            return ParseExprLiteral(_expr, list);
        } break;
        case Expr::UNARY: {
            ExprUnary* _expr = (ExprUnary*)expr;
            return ParseExprUnary(_expr, list);
        } break;
        case Expr::GROUPING: {
            ExprGrouping* _expr = (ExprGrouping*)expr;
            return ParseExpr(_expr->expr, list);
        } break;
        case Expr::VARIABLE: {
            ExprVariable* _expr = (ExprVariable*)expr;
            return ParseExprVariable(_expr, list);
        } break;
        case Expr::ASSIGN: {
            ExprAssign* _expr = (ExprAssign*)expr;
            return ParseExprAssign(_expr, list);
        } break;
        case Expr::CALL: {
            ExprCall* _expr = (ExprCall*)expr;
            return ParseExprCall(_expr, list);
        } break;
        case Expr::SWIZZLE: {
            ExprSwizzle* _expr = (ExprSwizzle*)expr;
            return ParseExprSwizzle(_expr, list);
        } break;
        case Expr::MEMBER: {
            ExprMember* _expr = (ExprMember*)expr;
            return ParseExprMember(_expr, list);
        } break;
        case Expr::POSTSCRIPT: {
            ExprPostscript* _expr = (ExprPostscript*)expr;
            return ParseExprPostscript(_expr, list);
        } break;
        case Expr::SELECTION: {
            ExprSelection* _expr = (ExprSelection*)expr;
            return ParseExprSelection(_expr, list);
        } break;
        case Expr::SEQUENCE: {
            ExprSequence* _expr = (ExprSequence*)expr;
            return ParseExprSequence(_expr, list);
        } break;
        default:
            CompilerError("Error!");
        }
        return nullptr;
    }

    Ast* Compiler::ParseAssignableExprVariable(ExprVariable* exprVariable, AstList& list)
    {
        Ast* type = Type2Ast(exprVariable->symbolTableId->type, list);
        Ast* variable = *varMap->get(exprVariable->name.text);
        if (variable->opCode == AstCode::AstVariable)
        {
            return variable;
        }
        if (variable->opCode == AstCode::AstFunctionParameter)
        {
            return variable;
        }
        CompilerError("Type is not assignable!");
    }

    Ast* Compiler::ParseAssignableExprSwizzle(ExprSwizzle* exprSwizzle, AstList& list)
    {
        std::string swizzle = exprSwizzle->swizzle.text;
        std::vector<Ast*> extractedValues;
        auto value = ParseAssignableExpr(exprSwizzle->variable, list);
        DataType* valueType = GetExprValueType(exprSwizzle->variable);
        DataType* valueBaseType = ((DataTypeVector*)valueType)->ComponentType;
        uint32_t valueBaseSize = ((DataTypeVector*)valueType)->ComponentCount;

        if (swizzle.size() > 1)
        {

            std::vector<uint32_t> swizzleIDs;
            for (uint32_t i = 0; i < valueBaseSize; ++i)
                swizzleIDs.push_back(i);

            for (uint32_t i = 0; i < swizzle.size(); ++i)
            {
                switch (swizzle[i])
                {
                case 'x': case 'r': case 'u':
                    swizzleIDs[0] = valueBaseSize + i;
                    break;
                case 'y': case 'g': case 'v':
                    swizzleIDs[1] = valueBaseSize + i;
                    break;
                case 'z': case 'b': case 's':
                    swizzleIDs[2] = valueBaseSize + i;
                    break;
                case 'w': case 'a': case 't':
                    swizzleIDs[3] = valueBaseSize + i;
                    break;
                }
            }

            DataTypeVector* swizzleType = (DataTypeVector*)Copy(valueType);
            swizzleType->ComponentCount = valueBaseSize;
            Ast* swizzleTypeAst = Type2Ast(swizzleType, list);

            auto ValueAst = new AstLoad(new Ref(swizzleTypeAst), new Ref(value));
            list.push_back(ValueAst);
            Ref* retRef = new Ref();
            auto VectorShuffle = new AstVectorShuffle(new Ref(swizzleTypeAst), new Ref(ValueAst), retRef, swizzleIDs);
            list.push_back(VectorShuffle);
            auto StoreAst = new AstStore(new Ref(value), new Ref(VectorShuffle));
            list.push_back(StoreAst);

            return VectorShuffle;
        }
        else
        {
            uint32_t swizzleID = 0;
            switch (swizzle[0])
            {
            case 'x': case 'r': case 'u': swizzleID = 0; break;
            case 'y': case 'g': case 'v': swizzleID = 1; break;
            case 'z': case 'b': case 's': swizzleID = 2; break;
            case 'w': case 'a': case 't': swizzleID = 3; break;
            }

            DataType* valuePtr = new DataTypePointer(StorageClass::Function, valueBaseType);

            Ast* valuePtrAst = Type2Ast(valuePtr, list);

            Ast* constAst = Type2Ast(typeFromString("uint"), list);
            auto Constant = new AstConstant(new Ref(constAst), swizzleID);
            list.push_back(Constant);
            auto AccessChain = new AstAccessChain(new Ref(valuePtrAst), new Ref(value), { new Ref(Constant) });
            list.push_back(AccessChain);
            return AccessChain;
        }
    }

    Ast* Compiler::ParseAssignableExprArray(ExprArray* exprArray, AstList& list)
    {
        std::vector<Ast*> extractedValues;
        auto value = ParseAssignableExpr(exprArray->variable, list);
        DataType* valueType = GetExprValueType(exprArray->variable);
        DataType* valueBaseType = ((DataTypeVector*)valueType)->ComponentType;
        uint32_t valueBaseSize = ((DataTypeVector*)valueType)->ComponentCount;
        Ast* valueTypeAst = Type2Ast(valueType, list);

        uint32_t swizzleID = 0;
        DataType* valuePtr = new DataTypePointer(StorageClass::Function, valueBaseType);

        Ast* valuePtrAst = Type2Ast(valuePtr, list);

        //Ast* constAst = Type2Ast(typeFromString("uint"), list);
        auto index = ParseExpr(exprArray->index, list);
        list.push_back(index);
        auto AccessChain = new AstAccessChain(new Ref(valuePtrAst), new Ref(value), { new Ref(index) });
        list.push_back(AccessChain);
        return AccessChain;
    }

    Ast* Compiler::ParseAssignableExprMember(ExprMember* exprMember, AstList& list)
    {
        std::string member = exprMember->member.text;
        auto valueAst = ParseExpr(exprMember->variable, list);
        DataType* valueType = GetExprValueType(exprMember->variable);

        if (valueType->type != DataType::STRUCT)
            CompilerError("Non struct can't have a member");
        auto stuctVariable = (DataTypeStruct*)valueType;
        StmtStruct* structDef = structDefs[stuctVariable->name];
        Symbol* memberSymbol = nullptr;
        uint32_t i = 0;
        for (; i < structDef->members.size(); ++i)
            if (structDef->members[i]->name == exprMember->member.text)
            {
                memberSymbol = structDef->members[i];
                break;
            }
        if (memberSymbol == nullptr)
            CompilerError("This shouldn't be hitable!");

        DataType* memberTypePtr = new DataTypePointer(StorageClass::Private, memberSymbol->type);
        Ast* memberTypePtrAst = Type2Ast(memberTypePtr, list);
        Ast* intType = Type2Ast(typeFromString("int"), list);
        Ast* constant = new AstConstant(new Ref(intType), i);
        list.push_back(constant);

        auto composite = new AstAccessChain(new Ref(memberTypePtrAst), new Ref(valueAst), { new Ref(constant) });
        list.push_back(composite);
        return composite;
    }

    Ast* Compiler::ParseAssignableExpr(Expr* expr, AstList& list)
    {
        switch (expr->type)
        {
        case Expr::VARIABLE: {
            ExprVariable* _expr = (ExprVariable*)expr;
            return ParseAssignableExprVariable(_expr, list);
        } break;
        case Expr::SWIZZLE: {
            ExprSwizzle* _expr = (ExprSwizzle*)expr;
            return ParseAssignableExprSwizzle(_expr, list);
        } break;
        case Expr::ARRAY: {
            ExprArray* _expr = (ExprArray*)expr;
            return ParseAssignableExprArray(_expr, list);
        } break;
        case Expr::MEMBER: {
            ExprMember* _expr = (ExprMember*)expr;
            return ParseAssignableExprMember(_expr, list);
        } break;
        default:
            CompilerError("Error!");
        }
        return nullptr;
    }

    AstList Compiler::ParseStmtStruct(StmtStruct* stmt)
    {
        structDefs.insert_or_assign(stmt->name.text, stmt);
        AstList list{};
        std::vector<DataType*> memberTypes;
        std::vector<Ref*> memberRefs;
        for (auto member : stmt->members)
        {
            memberTypes.push_back(member->type);
            Ast* memberAst = Type2Ast(member->type, list);
            memberRefs.push_back(new Ref(memberAst));
            list.push_back(memberAst);
        }
        DataTypeStruct* type = new DataTypeStruct(stmt->name.text, memberTypes);
        auto temp = new AstTypeStruct(memberRefs);
        types.push_back({ type, temp });
        list.push_back(temp);
        return list;
    }

    AstList Compiler::ParseStmtFunc(StmtFunc* stmt)
    {
        AstList list{};
        std::vector<DataType*> parameterTypes;
        varMap = stmt->scope = varMap->push();
        this->currentFunc = stmt->name.text;

        auto returnType = Type2Ast(stmt->returnType, list);
        
        auto func = new AstFunction(stmt->name.text, new Ref(returnType), FunctionControl::None, new Ref(), stmt->parameters);
        
        AstList params;
        for (auto param : stmt->parameters)
        {
            parameterTypes.push_back(param.type);
            auto opType = Type2Ast(param.type, list);
            auto temp = new AstFunctionParameter(new Ref(opType));
            func->astParameters.push_back(temp);
            params.push_back(temp);
            *varMap->def(param.name) = temp;
            asmRefs.insert_or_assign("<" + param.name + ">", temp);
        }

        DataTypeFunction* funcType = new DataTypeFunction(stmt->returnType, parameterTypes);
        auto funcTypeAst = Type2Ast(funcType, list);

        func->functionType->SetTarget(funcTypeAst);

        list.push_back(func);
        types.push_back({funcType, func});

        list.insert(list.end(), params.begin(), params.end());

        AstList body = ParseStmt(stmt->body);
        list.insert(list.end(), body.begin(), body.end());
        if (func->resultType->GetTarget()->opCode == AstCode::AstTypeVoid)
        {
            list.push_back(new AstReturn());
        }
        list.push_back(new AstFunctionEnd());
        addFunc(stmt->name.text, parameterTypes, stmt->varargs, func);

        this->currentFunc = "";
        varMap = varMap->pop();
        return list;
    }

    AstList Compiler::ParseStmtWhile(StmtWhile* stmt)
    {
/*
      OpBranch %18

//LoopStart
%18 = OpLabel
      OpLoopMerge %20 %21 None

//???
      OpBranch %22
%22 = OpLabel

// Conditional
%23 = OpLoad %int %i
%26 = OpSLessThan %bool %23 %int_4

//Branch
      OpBranchConditional %26 %19 %20

//LoopBody
%19 = OpLabel
%27 = OpLoad %int %i
%30 = OpAccessChain %_ptr_Output_float %fragColor %27
      OpStore %30 %float_0
%31 = OpLoad %int %i
%33 = OpIAdd %int %31 %int_1
      OpStore %i %33
      OpBranch %21

//LoopContinue
%21 = OpLabel
      OpBranch %18

//LoopExit
%20 = OpLabel
*/
        AstList list = {};

        auto LoopStart = new AstLabel();
        auto LoopBody = new AstLabel();
        auto LoopContinue = new AstLabel();
        auto LoopExit = new AstLabel();
        auto temp = lastExit;
        lastExit = LoopExit;

        list.push_back(new AstBranch(new Ref(LoopStart)));
        list.push_back(LoopStart);
        list.push_back(new AstLoopMerge(new Ref(LoopExit), new Ref(LoopContinue), LoopControl::None));
        
        auto unknownLabel = new AstLabel();
        list.push_back(new AstBranch(new Ref(unknownLabel)));
        list.push_back(unknownLabel);

        auto condition = ParseExpr(stmt->condition, list);

        list.push_back(new AstBranchConditional(new Ref(condition), new Ref(LoopBody), new Ref(LoopExit)));
        list.push_back(LoopBody);

        auto body = ParseStmt(stmt->body);
        list.insert(list.end(), body.begin(), body.end());

        list.push_back(new AstBranch(new Ref(LoopContinue)));
        list.push_back(LoopContinue);
        list.push_back(new AstBranch(new Ref(LoopStart)));
        list.push_back(LoopExit);

        lastExit = temp;

        return list;
    }

    AstList Compiler::ParseStmtBlock(StmtBlock* stmt)
    {
        AstList chunk;
        varMap = stmt->scope = varMap->push();
        for (auto stmt : stmt->stmts)
        {
            
            AstList ast = ParseStmt(stmt);
            chunk.insert(chunk.end(), ast.begin(), ast.end());
        }
        varMap = varMap->pop();
        return chunk;
    }

    AstList Compiler::ParseStmtExpr(StmtExpr* stmt)
    {
        AstList list;
        ParseExpr(stmt->expr, list);
        return list;
    }

    AstList Compiler::ParseStmtDecl(StmtDecl* stmt)
    {
        AstList list{};
        for (auto decl : stmt->varDecls)
        {
            Ref* ref = new Ref();
            DataType* type = new DataTypePointer(StorageClass::Private, stmt->type);
            auto typeAst = Type2Ast(type, list);
            list.push_back(new AstName(ref, decl.first->name));
            StorageClass storageClass = (currentFunc == "") ? StorageClass::Private : StorageClass::Function;
            ref->SetTarget(new AstVariable(new Ref(typeAst), storageClass));
            list.push_back(ref->GetTarget());
            if (decl.second != nullptr)
            {
                auto object = ParseExpr(decl.second, list);
                {
                    auto nameType = stmt->type;
                    auto valueType = GetResultType(object);
                    if (!DataTypesEqual(nameType, valueType) &&
                        (nameType->type != DataType::POINTER || !DataTypesEqual(((DataTypePointer*)nameType)->ComponentType, valueType)))
                    {
                        object = Cast(object, valueType, nameType, list);
                    }
                }
                list.push_back(new AstStore(ref, new Ref(object)));
            }
            else if(stmt->type->type == DataType::STRUCT)
            {
                auto typeAst = Type2Ast(((DataTypePointer*)type)->ComponentType, list);
                std::vector<Ref*> constituents;
                for(auto member : ((DataTypeStruct*)stmt->type)->members)
                {
                    //memberType to 0 value
                    switch(member->type)
                    {
                        case DataType::INT:
                        case DataType::FLOAT:{
                            auto memberAst = Type2Ast(member, list);
                            auto value = new AstConstant(new Ref(memberAst), 0);
                            list.push_back(value);
                            constituents.push_back(new Ref(value));
                        } break;
                        case DataType::VECTOR: {
                            auto subvalueAst = Type2Ast(((DataTypeVector*)member)->ComponentType, list);
                            auto subvalue = new AstConstant(new Ref(subvalueAst), 0);
                            auto memberAst = Type2Ast(member, list);
                            list.push_back(subvalue);
                            auto subconstituents = std::vector<Ref*>();
                            int size = ((DataTypeVector*)member)->ComponentCount;
                            for(int i = 0; i < size; i++)
                            {
                                subconstituents.push_back(new Ref(subvalue));
                            }
                            auto value = new AstCompositeConstruct(new Ref(memberAst), subconstituents);
                            list.push_back(value);
                            constituents.push_back(new Ref(value));
                        } break;

                        default:
                            CompilerError("Unknown type");
                    }
                }
                auto object = new AstCompositeConstruct(new Ref(typeAst), constituents);
                list.push_back(object);
                list.push_back(new AstStore(ref, new Ref(object)));
            }
            *varMap->def(decl.first->name) = (AstVariable*)ref->GetTarget();
        }
        return list;
    }

    AstList Compiler::ParseStmtIf(StmtIf* stmt)
    {
        AstList list;
        auto condition = ParseExpr(stmt->condition, list);
        auto trueLabel = new AstLabel();
        auto elseLabel = new AstLabel();
        auto lastLabel = new AstLabel(); 
        list.push_back(new AstSelectionMerge(new Ref(lastLabel), SelectionControl::None));
        list.push_back(new AstBranchConditional(new Ref(condition), new Ref(trueLabel), new Ref(elseLabel)));
        list.push_back(trueLabel);
        auto thenBlock = ParseStmt(stmt->thenBranch);
        list.insert(list.end(), thenBlock.begin(), thenBlock.end());
        list.push_back(new AstBranch(new Ref(lastLabel)));
        list.push_back(elseLabel);
        if (stmt->elseBranch != nullptr)
        {
            auto elseBlock = ParseStmt(stmt->elseBranch);
            list.insert(list.end(), elseBlock.begin(), elseBlock.end());
        }
        list.push_back(new AstBranch(new Ref(lastLabel)));
        list.push_back(lastLabel);

        return list;
    }

    AstList Compiler::ParseStmtReturn(StmtReturn* stmt)
    {
        AstList list{};
        if (stmt->value == nullptr)
        {
            list.push_back(new AstReturn());
        }
        else
        {
            Ast* expr = ParseExpr(stmt->value, list);
            if(expr->resultType->GetTarget()->opCode == AstCode::AstTypePointer) {
                auto pointerSubType = ((AstTypePointer*)expr->resultType->GetTarget())->type;
                expr = new AstLoad(pointerSubType, new Ref(expr));
                list.push_back(expr);
            }
            list.push_back(new AstReturnValue(new Ref(expr)));
        }
        return list;
    }

    AstList Compiler::ParseStmtBreak(StmtBreak* stmt)
    {
        AstList list;
        list.push_back(new AstBranch(new Ref(lastExit)));
        return list;
    }

    StorageClass StorageClassFromText(std::string text)
    {
        switch (hash(text.c_str()))
        {
        case const_hash("Function"):
            return StorageClass::Function;
        default:
            return (StorageClass)-1;
        }
    }
 
    AstList Compiler::ParseStmtAst(StmtAsm* stmt)
    {
        AstList list{};
        switch (hash(stmt->opcode.text.c_str()))
        {
        case const_hash("ExtInstImport"): {
            auto temp = new AstExtInstImport(stmt->parameters[0].text);
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("ExtInst"): {

            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;

            auto foundSet = asmRefs.find(stmt->parameters[1].text);
            Ast* set = (foundSet != asmRefs.end()) ? foundSet->second : nullptr;

            auto instruction = std::stoi(stmt->parameters[2].text);
            
            std::vector<Ref*> constituents;
            for (int i = 3; i < stmt->parameters.size(); ++i)
                constituents.push_back(new Ref(asmRefs[stmt->parameters[i].text]));

            auto temp = new AstExtInst(new Ref(type), new Ref(set), instruction, constituents);
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("TypeVoid"): {
            auto temp = new AstTypeVoid();
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("TypeInt"): {
            auto temp = new AstTypeInt(std::stoi(stmt->parameters[0].text), std::stoi(stmt->parameters[1].text));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("TypeFloat"): {
            auto temp = new AstTypeFloat(std::stoi(stmt->parameters[0].text));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("TypeVector"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;
            auto temp = new AstTypeVector(new Ref(type), std::stoi(stmt->parameters[1].text));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("TypeMatrix"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;
            auto temp = new AstTypeMatrix(new Ref(type), std::stoi(stmt->parameters[1].text));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("TypeStruct"): {
            std::vector<Ref*> memberTypes;
            for (int i = 1; i < stmt->parameters.size(); ++i)
                memberTypes.push_back(new Ref(asmRefs[stmt->parameters[i].text]));
            auto temp = new AstTypeStruct(memberTypes);
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("TypePointer"): {
            auto foundType = asmRefs.find(stmt->parameters[1].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;
            auto temp = new AstTypePointer(StorageClassFromText(stmt->parameters[0].text), new Ref(type));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("Constant"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;
            
            uint32_t data;
            switch (type->opCode)
            {
            case AstCode::AstTypeInt: {
                int fData = std::stoi(stmt->parameters[1].text);
                data = *(uint32_t*)(&fData);
            }break;
            case AstCode::AstTypeFloat: {
                float fData = std::stof(stmt->parameters[1].text);
                data = *(uint32_t*)(&fData);
            }break;
            default:
                CompilerError("Not handled!");
            }
            auto temp = new AstConstant(new Ref(type), data);
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("Variable"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;
            auto temp = new AstVariable(new Ref(type), StorageClassFromText(stmt->parameters[1].text));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("CompositeConstruct"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;
            std::vector<Ref*> constituents;
            for (int i = 1; i < stmt->parameters.size(); ++i)
                constituents.push_back(new Ref(asmRefs[stmt->parameters[i].text]));
            auto temp = new AstCompositeConstruct(new Ref(type), constituents);
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("CompositeExtract"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;

            auto foundComposite = asmRefs.find(stmt->parameters[1].text);
            Ast* composite = (foundComposite != asmRefs.end()) ? foundComposite->second : nullptr;

            std::vector<uint32_t> indexes;
            for (int i = 2; i < stmt->parameters.size(); ++i)
                indexes.push_back(std::stoul(stmt->parameters[i].text));

            auto temp = new AstCompositeExtract(new Ref(type), new Ref(composite), indexes);
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("Load"): {
            auto foundResultType = asmRefs.find(stmt->parameters[0].text);
            Ast* resultType = (foundResultType != asmRefs.end()) ? foundResultType->second : nullptr;
            auto foundPointer = asmRefs.find(stmt->parameters[1].text);
            Ast* pointer = (foundPointer != asmRefs.end()) ? foundPointer->second : nullptr;
            auto temp = new AstLoad(new Ref(resultType), new Ref(pointer));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("Store"): {
            auto foundPointer = asmRefs.find(stmt->parameters[0].text);
            Ast* Pointer = (foundPointer != asmRefs.end()) ? foundPointer->second : nullptr;
            auto foundObject = asmRefs.find(stmt->parameters[1].text);
            Ast* Object = (foundObject != asmRefs.end()) ? foundObject->second : nullptr;
            auto temp = new AstStore(new Ref(Pointer), new Ref(Object));
            list.push_back(temp);
        } break;
        case const_hash("AccessChain"): {
            auto foundResultType = asmRefs.find(stmt->parameters[0].text);
            Ast* resultType = (foundResultType != asmRefs.end()) ? foundResultType->second : nullptr;
            auto foundBase = asmRefs.find(stmt->parameters[1].text);
            Ast* base = (foundBase != asmRefs.end()) ? foundBase->second : nullptr;
            std::vector<Ref*> indexes;
            for (int i = 2; i < stmt->parameters.size(); ++i)
                indexes.push_back(new Ref(asmRefs[stmt->parameters[i].text]));
            auto temp = new AstAccessChain(new Ref(resultType), new Ref(base), indexes);
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("ConvertSToF"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;

            auto foundX = asmRefs.find(stmt->parameters[1].text);
            Ast* x = (foundX != asmRefs.end()) ? foundX->second : nullptr;

            auto temp = new AstConvertSToF(new Ref(type), new Ref(x));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("ConvertFToS"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;

            auto foundX = asmRefs.find(stmt->parameters[1].text);
            Ast* x = (foundX != asmRefs.end()) ? foundX->second : nullptr;

            auto temp = new AstConvertFToS(new Ref(type), new Ref(x));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("Dot"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;

            auto foundX = asmRefs.find(stmt->parameters[1].text);
            Ast* x = (foundX != asmRefs.end()) ? foundX->second : nullptr;

            auto foundY = asmRefs.find(stmt->parameters[2].text);
            Ast* y = (foundY != asmRefs.end()) ? foundY->second : nullptr;

            auto temp = new AstDot(new Ref(type), new Ref(x), new Ref(y));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("Mod"): {
            auto foundType = asmRefs.find(stmt->parameters[0].text);
            Ast* type = (foundType != asmRefs.end()) ? foundType->second : nullptr;

            auto foundX = asmRefs.find(stmt->parameters[1].text);
            Ast* x = (foundX != asmRefs.end()) ? foundX->second : nullptr;

            auto foundY = asmRefs.find(stmt->parameters[2].text);
            Ast* y = (foundY != asmRefs.end()) ? foundY->second : nullptr;

            auto temp = new AstFMod(new Ref(type), new Ref(x), new Ref(y));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("ReturnValue"): {
            auto foundObject = asmRefs.find(stmt->parameters[0].text);
            Ast* Object = (foundObject != asmRefs.end()) ? foundObject->second : nullptr;
            auto temp = new AstReturnValue(new Ref(Object));
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;
        case const_hash("Return"): {
            auto temp = new AstReturn();
            if (stmt->result.text.size() > 0)
                asmRefs.insert_or_assign(stmt->result.text, temp);
            list.push_back(temp);
        } break;

        default:
            CompilerError("inline opcode (%s) not supported!", stmt->opcode.text.c_str());
        }
        return list;
    }

    AstList Compiler::ParseStmt(Stmt* stmt)
    {
        switch (stmt->type)
        {
        case StmtType::EXPR: {
            StmtExpr* _stmt = (StmtExpr*)stmt;
            return ParseStmtExpr(_stmt);
        } break;
        case StmtType::DECL: {
            StmtDecl* _stmt = (StmtDecl*)stmt;
            return ParseStmtDecl(_stmt);
        } break;
        case StmtType::BLOCK: {
            StmtBlock* _stmt = (StmtBlock*)stmt;
            return ParseStmtBlock(_stmt);
        } break;
        case StmtType::IF: {
            StmtIf* _stmt = (StmtIf*)stmt;
            return ParseStmtIf(_stmt);
        } break;
        case StmtType::WHILE: {
            StmtWhile* _stmt = (StmtWhile*)stmt;
            return ParseStmtWhile(_stmt);
        } break;
        case StmtType::STRUCT: {
            StmtStruct* _stmt = (StmtStruct*)stmt;
            return ParseStmtStruct(_stmt);
        } break;
        case StmtType::FUNC: {
            StmtFunc* _stmt = (StmtFunc*)stmt;
            asmRefs.clear();
            return ParseStmtFunc(_stmt);
        } break;
        case StmtType::RETURN: {
            StmtReturn* _stmt = (StmtReturn*)stmt;
            return ParseStmtReturn(_stmt);
        } break;
        case StmtType::BREAK: {
            StmtBreak* _stmt = (StmtBreak*)stmt;
            return ParseStmtBreak(_stmt);
        } break;
        case StmtType::ASM: {
            StmtAsm* _stmt = (StmtAsm*)stmt;
            return ParseStmtAst(_stmt);
        }
        default:
            CompilerError("Stmt Type (%d) not yet implemented!", stmt->type);
        }
        return {};
    }

    AstList Compiler::ParseProgram(Program* program)
    {
        AstList list;
        varMap = varMap->push();
        for (auto stmt : program->stmts)
        {
            AstList ast = ParseStmt(stmt);
            list.insert(list.end(), ast.begin(), ast.end());
        }
        varMap = varMap->pop();
        return list;
    }

	AstList Compiler::Process()
	{
        AstList list;
        auto debugPrintf = new AstExtInstImport("NonSemantic.DebugPrintf");
        list.push_back(debugPrintf);
        extFuncs.push_back({
            FuncDef("printf", true),
            ExtFunc{
                debugPrintf,
                1,
                Type2Ast(new DataTypeVoid(), list),
                true,
                {}
            }
        });
        auto prog = ParseProgram(program);
        for (auto func : funcs)
        {
            FuncDef funcDef = std::get<FuncDef>(func);
            AstFunction* funcPtr = std::get<AstFunction*>(func);
            std::string fnName = funcDef.name + "(";
            for (auto param : funcDef.params)
            {
                std::string paramName = ShortName(param) + ";";
                fnName += paramName;
            }
            list.push_back(new AstEntryPoint(ExecutionModel::Kernel, new Ref(funcPtr), fnName));
            list.push_back(new AstExecutionMode(new Ref(funcPtr), ExecutionMode::OriginUpperLeft));
            //funcDeclChunk.write(OpName(GetID(funcPtr), fnName));
            list.push_back(new AstName(new Ref(funcPtr), fnName));
        }
        list.insert(list.end(), prog.begin(), prog.end());
        return list;
	}

    FuncDef::FuncDef(std::string name, bool varargs) :
        name(name),
        varargs(varargs),
        params({})
    {}

    FuncDef::FuncDef(std::string name, std::vector<DataType*> params) :
        name(name),
        varargs(false),
        params(params)
    {}
}