// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "SpirvChunkOptimizer.h"

namespace SpirV
{
    Optimizer::Optimizer(Chunk chunk)
    {
        this->inputChunk = chunk;
    }

    Chunk Optimizer::Process()
    {
        return this->inputChunk;
    }
}