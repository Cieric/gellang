// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include "../../parser.h"
#include "SpirvVM.h"
#include "../../DataType.h"
#include "SpirvAstOps.h"
#include "../ScopeMap.h"

namespace SpirV
{
	class Optimizer
	{
        Chunk inputChunk;
        Chunk processedChunk;
    public:
		Optimizer(Chunk chunk);
		Chunk Process();
	};
}