// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "SpirvAssembler.h"
#include "CompilerUtils.h"
#include <cassert>
#include <limits>

namespace SpirV
{
    Assembler::Assembler(AstList list, std::vector<std::tuple<FuncDef, AstFunction*>> funcs)
    {
        this->list = list;
        this->funcs = funcs;
    }

    uint32_t Assembler::NextResult()
    {
        return flattenResult++;
    }

    uint32_t Assembler::GetID(Ast* ast, bool generateIfNull)
    {
        if (ast->result == std::numeric_limits<uint32_t>::max())
        {
            if (!generateIfNull)
                CompilerError("Ast Node doesnt have an ID yet!");
            ast->result = NextResult();
        }
        return ast->result;
    }

    uint32_t Assembler::GetID(Ref* ref, bool generateIfNull)
    {
        return GetID(ref->GetTarget(), generateIfNull);
    }

    Chunk Assembler::flatten(Ast* ast)
    {
        Chunk chunk;
        switch (ast->opCode)
        {
        case AstCode::AstNop: {
            AstNop* _ast = (AstNop*)ast;
            chunk.write(OpNop());
        } break;
        case AstCode::AstEntryPoint: {
            AstEntryPoint* _ast = (AstEntryPoint*)ast;
            chunk.write(OpEntryPoint(
                _ast->executionModel,
                GetID(_ast->entryPoint, true),
                _ast->name
            ));
        } break;
        case AstCode::AstExecutionMode: {
            AstExecutionMode* _ast = (AstExecutionMode*)ast;
            chunk.write(OpExecutionMode(
                GetID(_ast->entryPoint, true),
                _ast->executionMode
            ));
        } break;
        case AstCode::AstName: {
            AstName* _ast = (AstName*)ast;
            chunk.write(OpName(
                GetID(_ast->target, true),
                _ast->name
            ));
        } break;
        case AstCode::AstString: {
            AstString* _ast = (AstString*)ast;
            chunk.write(OpString(
                GetID(_ast),
                _ast->str
            ));
        } break;
        case AstCode::AstExtInstImport: {
            AstExtInstImport* _ast = (AstExtInstImport*)ast;
            chunk.write(OpExtInstImport(
                GetID(_ast),
                _ast->name
            ));
        } break;
        case AstCode::AstExtInst: {
            AstExtInst* _ast = (AstExtInst*)ast;
            std::vector<uint32_t> paramIds;
            for (auto operands : _ast->operands)
                paramIds.push_back(GetID(operands));
            chunk.write(OpExtInst(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->set),
                _ast->instruction,
                paramIds
            ));
        } break;
        case AstCode::AstTypeVoid: {
            AstTypeVoid* _ast = (AstTypeVoid*)ast;
            chunk.write(OpTypeVoid(
                GetID(_ast)
            ));
        } break;
        case AstCode::AstTypeBool: {
            AstTypeBool* _ast = (AstTypeBool*)ast;
            chunk.write(OpTypeBool(
                GetID(_ast)
            ));
        } break;
        case AstCode::AstTypeInt: {
            AstTypeInt* _ast = (AstTypeInt*)ast;
            chunk.write(OpTypeInt(
                GetID(_ast),
                _ast->width,
                _ast->_signed
            ));
        } break;
        case AstCode::AstTypeFloat: {
            AstTypeFloat* _ast = (AstTypeFloat*)ast;
            chunk.write(OpTypeFloat(
                GetID(_ast),
                _ast->width
            ));
        } break;
        case AstCode::AstTypeVector: {
            AstTypeVector* _ast = (AstTypeVector*)ast;
            chunk.write(OpTypeVector(
                GetID(_ast),
                GetID(_ast->componentType),
                _ast->componentCount
            ));
        } break;
        case AstCode::AstTypeMatrix: {
            AstTypeMatrix* _ast = (AstTypeMatrix*)ast;
            chunk.write(OpTypeMatrix(
                GetID(_ast),
                GetID(_ast->componentType),
                _ast->componentCount
            ));
        } break;
        case AstCode::AstTypeStruct: {
            AstTypeStruct* _ast = (AstTypeStruct*)ast;
            std::vector<uint32_t> memberIds;
            for (auto operands : _ast->members)
                memberIds.push_back(GetID(operands));
            chunk.write(OpTypeStruct(
                GetID(_ast),
                memberIds
            ));
        } break;
        case AstCode::AstTypePointer: {
            AstTypePointer* _ast = (AstTypePointer*)ast;
            chunk.write(OpTypePointer(
                GetID(_ast),
                _ast->storage,
                GetID(_ast->type)
            ));
        } break;
        case AstCode::AstTypeFunction: {
            AstTypeFunction* _ast = (AstTypeFunction*)ast;
            std::vector<uint32_t> paramIds;
            for (auto param : _ast->parameters)
                paramIds.push_back(GetID(param));
            chunk.write(OpTypeFunction(
                GetID(_ast),
                GetID(_ast->returnType),
                paramIds
            ));
        } break;
        case AstCode::AstConstant: {
            AstConstant* _ast = (AstConstant*)ast;
            chunk.write(OpConstant(
                GetID(_ast->resultType),
                GetID(_ast),
                _ast->value
            ));
        } break;
        case AstCode::AstFunction: {
            AstFunction* _ast = (AstFunction*)ast;
            chunk.write(OpFunction(
                GetID(_ast->resultType),
                GetID(_ast),
                _ast->functionControl,
                GetID(_ast->functionType)
            ));
        } break;
        case AstCode::AstFunctionParameter: {
            AstFunctionParameter* _ast = (AstFunctionParameter*)ast;
            chunk.write(OpFunctionParameter(
                GetID(_ast->resultType),
                GetID(_ast)
            ));
        } break;
        case AstCode::AstFunctionEnd: {
            AstFunctionEnd* _ast = (AstFunctionEnd*)ast;
            chunk.write(OpFunctionEnd());
        } break;
        case AstCode::AstFunctionCall: {
            AstFunctionCall* _ast = (AstFunctionCall*)ast;
            std::vector<uint32_t> operandIds;
            for (auto operands : _ast->operands)
                operandIds.push_back(GetID(operands));
            chunk.write(OpFunctionCall(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->function),
                operandIds
            ));
        } break;
        case AstCode::AstVariable: {
            AstVariable* _ast = (AstVariable*)ast;
            assert(_ast->resultType);
            assert(_ast->resultType->GetTarget());
            assert(_ast->resultType->GetTarget()->opCode == AstCode::AstTypePointer);
            chunk.write(OpVariable(
                GetID(_ast->resultType),
                GetID(_ast),
                _ast->storageClass
            ));
        } break;
        case AstCode::AstLoad: {
            AstLoad* _ast = (AstLoad*)ast;
            chunk.write(OpLoad(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->pointer)
            ));
        } break;
        case AstCode::AstStore: {
            AstStore* _ast = (AstStore*)ast;
            chunk.write(OpStore(
                GetID(_ast->pointer),
                GetID(_ast->object)
            ));
        } break;
        case AstCode::AstAccessChain: {
            AstAccessChain* _ast = (AstAccessChain*)ast;
            std::vector<uint32_t> indexesIds;
            for (auto index : _ast->indexes)
                indexesIds.push_back(GetID(index));
            chunk.write(OpAccessChain(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->base),
                indexesIds
            ));
        } break;
        case AstCode::AstVectorShuffle: {
            AstVectorShuffle* _ast = (AstVectorShuffle*)ast;
            chunk.write(OpVectorShuffle(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->vector1),
                GetID(_ast->vector2),
                _ast->components
            ));
        } break;
        case AstCode::AstCompositeConstruct: {
            AstCompositeConstruct* _ast = (AstCompositeConstruct*)ast;
            std::vector<uint32_t> constituentsIds;
            for (auto constituent : _ast->constituents)
                constituentsIds.push_back(GetID(constituent));
            chunk.write(OpCompositeConstruct(
                GetID(_ast->resultType),
                GetID(_ast),
                constituentsIds
            ));
        } break;
        case AstCode::AstCompositeExtract: {
            AstCompositeExtract* _ast = (AstCompositeExtract*)ast;
            chunk.write(OpCompositeExtract(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->composite),
                _ast->indexes
            ));
        } break;
        case AstCode::AstConvertFToS: {
            AstConvertFToS* _ast = (AstConvertFToS*)ast;
            chunk.write(OpConvertFToS(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->floatValue)
            ));
        } break;
        case AstCode::AstConvertSToF: {
            AstConvertSToF* _ast = (AstConvertSToF*)ast;
            chunk.write(OpConvertSToF(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->signedValue)
            ));
        } break;
        case AstCode::AstSNegate: {
            AstSNegate* _ast = (AstSNegate*)ast;
            chunk.write(OpSNegate(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand)
            ));
        } break;
        case AstCode::AstFNegate: {
            AstFNegate* _ast = (AstFNegate*)ast;
            chunk.write(OpFNegate(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand)
            ));
        } break;
        case AstCode::AstIAdd: {
            AstIAdd* _ast = (AstIAdd*)ast;
            chunk.write(OpIAdd(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFAdd: {
            AstFAdd* _ast = (AstFAdd*)ast;
            chunk.write(OpFAdd(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFSub: {
            AstFSub* _ast = (AstFSub*)ast;
            chunk.write(OpFSub(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFMul: {
            AstFMul* _ast = (AstFMul*)ast;
            chunk.write(OpFMul(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstSDiv: {
            AstSDiv* _ast = (AstSDiv*)ast;
            chunk.write(OpSDiv(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFDiv: {
            AstFDiv* _ast = (AstFDiv*)ast;
            chunk.write(OpFDiv(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFMod: {
            AstFMod* _ast = (AstFMod*)ast;
            chunk.write(OpFMod(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstMatrixTimesScaler: {
            AstMatrixTimesScaler* _ast = (AstMatrixTimesScaler*)ast;
            chunk.write(OpMatrixTimesScaler(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstMatrixTimesVector: {
            AstMatrixTimesVector* _ast = (AstMatrixTimesVector*)ast;
            chunk.write(OpMatrixTimesVector(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstVectorTimesMatrix: {
            AstVectorTimesMatrix* _ast = (AstVectorTimesMatrix*)ast;
            chunk.write(OpVectorTimesMatrix(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstMatrixTimesMatrix: {
            AstMatrixTimesMatrix* _ast = (AstMatrixTimesMatrix*)ast;
            chunk.write(OpMatrixTimesMatrix(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstDot: {
            AstDot* _ast = (AstDot*)ast;
            chunk.write(OpDot(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstLogicalOr: {
            AstLogicalOr* _ast = (AstLogicalOr*)ast;
            chunk.write(OpLogicalOr(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstLogicalAnd: {
            AstLogicalAnd* _ast = (AstLogicalAnd*)ast;
            chunk.write(OpLogicalAnd(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstLogicalNot: {
            AstLogicalNot* _ast = (AstLogicalNot*)ast;
            chunk.write(OpLogicalNot(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1)
            ));
        } break;
        case AstCode::AstSLessThan: {
            AstSLessThan* _ast = (AstSLessThan*)ast;
            chunk.write(OpSLessThan(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstSLessThanEqual: {
            AstSLessThanEqual* _ast = (AstSLessThanEqual*)ast;
            chunk.write(OpSLessThanEqual(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstSGreaterThan: {
            AstSGreaterThan* _ast = (AstSGreaterThan*)ast;
            chunk.write(OpSGreaterThan(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFOrdEqual: {
            AstFOrdEqual* _ast = (AstFOrdEqual*)ast;
            chunk.write(OpFOrdEqual(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFOrdLessThan: {
            AstFOrdLessThan* _ast = (AstFOrdLessThan*)ast;
            chunk.write(OpFOrdLessThan(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFOrdLessThanEqual: {
            AstFOrdLessThanEqual* _ast = (AstFOrdLessThanEqual*)ast;
            chunk.write(OpFOrdLessThanEqual(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFOrdGreaterThan: {
            AstFOrdGreaterThan* _ast = (AstFOrdGreaterThan*)ast;
            chunk.write(OpFOrdGreaterThan(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstFOrdGreaterThanEqual: {
            AstFOrdGreaterThanEqual* _ast = (AstFOrdGreaterThanEqual*)ast;
            chunk.write(OpFOrdGreaterThanEqual(
                GetID(_ast->resultType),
                GetID(_ast),
                GetID(_ast->operand1),
                GetID(_ast->operand2)
            ));
        } break;
        case AstCode::AstLoopMerge: {
            AstLoopMerge* _ast = (AstLoopMerge*)ast;
            chunk.write(OpLoopMerge(
                GetID(_ast->mergeBlock, true),
                GetID(_ast->continueBlock, true),
                _ast->loopControl
            ));
        } break;
        case AstCode::AstSelectionMerge: {
            AstSelectionMerge* _ast = (AstSelectionMerge*)ast;
            chunk.write(OpSelectionMerge(
                GetID(_ast->mergeBlock, true),
                _ast->selectionControl
            ));
        } break;
        case AstCode::AstLabel: {
            AstLabel* _ast = (AstLabel*)ast;
            chunk.write(OpLabel(
                GetID(_ast, true)
            ));
        } break;
        case AstCode::AstBranch: {
            AstBranch* _ast = (AstBranch*)ast;
            chunk.write(OpBranch(
                GetID(_ast->targetLabel, true)
            ));
        } break;
        case AstCode::AstBranchConditional: {
            AstBranchConditional* _ast = (AstBranchConditional*)ast;
            chunk.write(OpBranchConditional(
                GetID(_ast->condition),
                GetID(_ast->trueLabel, true),
                GetID(_ast->falseLabel, true)
            ));
        } break;
        case AstCode::AstReturn: {
            AstReturn* _ast = (AstReturn*)ast;
            chunk.write(OpReturn());
        } break;
        case AstCode::AstReturnValue: {
            AstReturnValue* _ast = (AstReturnValue*)ast;
            chunk.write(OpReturnValue(
                GetID(_ast->value)
            ));
        } break;
        default: CompilerError("sdfsdf");
        }
        return chunk;
    }

    Chunk Assembler::flatten(AstList list)
    {
        Chunk chunk;
        
        for (size_t i = 0; i < list.size(); ++i)
        {
            Ast* ast = list[i];
            chunk.write(flatten(ast));
        }
        return chunk;
    }

    uint32_t Assembler::PeekResult()
    {
        return flattenResult;
    }

    Chunk Assembler::Process()
    {
        Chunk code;
        Chunk headerChunk = Chunk();
        
        headerChunk.write(OpCapability(Capability::Shader));
        headerChunk.write(OpExtInstImport(NextResult(), "GLSL.std.420"));
        headerChunk.write(OpMemoryModel(AddressingModel::Logical, MemoryModel::GLSL450));
        headerChunk.write(OpSource(SourceLanguage::GLSL, 420));

        Chunk funcDeclChunk = Chunk();
        for (auto func : funcs)
        {
            FuncDef funcDef = std::get<FuncDef>(func);
            AstFunction* funcPtr = std::get<AstFunction*>(func);
            funcDeclChunk.write(OpEntryPoint(ExecutionModel::Kernel, GetID(funcPtr), funcDef.name));
            funcDeclChunk.write(OpExecutionMode(GetID(funcPtr), ExecutionMode::OriginUpperLeft));
            std::string fnName = funcDef.name + "(";
            for (auto param : funcDef.params)
            {
                std::string paramName = ShortName(param) + ";";
                fnName += paramName;
            }
            funcDeclChunk.write(OpName(GetID(funcPtr), fnName));
        
        }
        headerChunk.write(funcDeclChunk);

        code.write(headerChunk);
        code.write(flatten(this->list));

        return code;
    }
}