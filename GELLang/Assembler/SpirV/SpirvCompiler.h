// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include "../../parser.h"
#include "SpirvVM.h"
#include "../../DataType.h"
#include "SpirvAstOps.h"
#include "../ScopeMap.h"

namespace SpirV
{
	struct FuncDef
	{
		std::string name;
		bool varargs = false;
		std::vector<DataType*> params;
		FuncDef(std::string, bool);
		FuncDef(std::string, std::vector<DataType*>);
	};

	struct ExtFunc
	{
		AstExtInstImport* extention;
		uint32_t funcId;
		Ast* returnType;
		bool varargs = false;
		std::vector<DataType*> ParameterTypes;
	};

	typedef std::vector<Ast*> AstList;

	class Compiler
	{
		Program* program;
		std::string currentFunc = "";

		std::vector<std::tuple<DataType*, Ast*>> types;
		std::vector<std::tuple<FuncDef, AstFunction*>> funcs;
		std::vector<std::tuple<FuncDef, ExtFunc>> extFuncs;
		std::unordered_map<std::string, Ast*> asmRefs;
		std::unordered_map<std::string, StmtStruct*> structDefs;
		ScopeMap<Ast*>* varMap;
		AstLabel* lastExit = nullptr;

		AstFunction* getFunc(std::string name, std::vector<DataType*> params);
		//AstFunction* getUpcastedFunc(std::string name, std::vector<DataType*> params);
		ExtFunc* getExtFunc(std::string name, std::vector<DataType*> params);
		
		void addFunc(std::string name, std::vector<DataType*> params, bool varargs, AstFunction* func);

		Ast* Type2Ast(DataType* type, AstList& list);

		AstBinary* GetMathOpMatrix(Token::Type type, DataType* left, DataType* right);
		AstBinary* GetMathOpVector(Token::Type type, DataType* left, DataType* right);
		AstBinary* GetMathOp(Token::Type type, DataType* left, DataType* right);
		void ProcessUpcast(AstBinary* op, AstList& list);
		Ast* ParseExprBinary(ExprBinary* exprLiteral, AstList& list);
		Ast* ParseExprLiteral(ExprLiteral* exprLiteral, AstList& list);
		Ast* ParseExprUnaryPlusPlus(Expr* exprOp, AstList& list);
		Ast* ParseExprUnaryMinus(Expr* exprROp, AstList& list);
		Ast* ParseExprUnaryBang(Expr* exprROp, AstList& list);
		Ast* ParseExprUnary(ExprUnary* exprUnary, AstList& list);
		Ast* ParseExprVariable(ExprVariable* exprVariable, AstList& list);
		Ast* Cast(Ast* ast, DataType* fromType, DataType* toType, AstList& list);
		Ast* Assign(Expr* ptr, Ast* obj, AstList& list);
		Ast* ParseExprAssign(ExprAssign* exprCall, AstList& list);
		Ast* ParseExprCall(ExprCall* exprCall, AstList& list);
		Ast* ParseExprSwizzle(ExprSwizzle* exprSwizzle, AstList& list);
		Ast* ParseExprMember(ExprMember* exprMember, AstList& list);
		Ast* ParseExprPostscriptPlusPlus(Expr* exprOp, AstList& list);
		Ast* ParseExprPostscript(ExprPostscript* exprSwizzle, AstList& list);
		Ast* ParseExprSelection(ExprSelection* exprSelection, AstList& list);
		Ast* ParseExprSequence(ExprSequence* exprSequence, AstList& list);
		Ast* ParseExpr(Expr* expr, AstList& list);

		Ast* ParseAssignableExprVariable(ExprVariable* exprVariable, AstList& list);
		Ast* ParseAssignableExprSwizzle(ExprSwizzle* exprSwizzle, AstList& list);
		Ast* ParseAssignableExprArray(ExprArray* exprSwizzle, AstList& list);
		Ast* ParseAssignableExprMember(ExprMember* exprMember, AstList& list);
		Ast* ParseAssignableExpr(Expr* expr, AstList& list);

		AstList ParseStmtStruct(StmtStruct* stmt);
		AstList ParseStmtFunc(StmtFunc* stmt);
		AstList ParseStmtWhile(StmtWhile* stmt);
		AstList ParseStmtBlock(StmtBlock* stmt);
		AstList ParseStmtExpr(StmtExpr* stmt);
		AstList ParseStmtDecl(StmtDecl* stmt);
		AstList ParseStmtIf(StmtIf* stmt);
		AstList ParseStmtReturn(StmtReturn* stmt);
		AstList ParseStmtBreak(StmtBreak* stmt);
		AstList ParseStmtAst(StmtAsm* stmt);
		AstList ParseStmt(Stmt* stmt);
		AstList ParseProgram(Program* program);

		uint32_t NextResult();
		uint32_t GetID(Ast* ast, bool generateIfNull = true);
		uint32_t GetID(Ref* ref, bool generateIfNull = false);
		Chunk flatten(Ast* ast);
		Chunk flatten(AstList list);

		DataType* GetExprValueType(Expr* expr);
	public:
		std::vector<std::tuple<FuncDef, AstFunction*>> GetFuncs();
		Compiler(Program* program);
		AstList Process();
	};
}