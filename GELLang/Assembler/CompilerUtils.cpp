// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "CompilerUtils.h"
#include <stdarg.h>

int CompilerError(const char* format, ...)
{
	va_list args;
	fprintf(stderr, "Compiler Error: ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	fprintf(stderr, "\n");
    throw new NotImplemented();
}