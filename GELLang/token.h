// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <unordered_set>
#include <string>
#include <list>
#include <vector>

static const char EOT = 4;

struct Token
{
	enum Type
	{
		UNKNOWN,
		KEYWORD,
		IDENT,
		FLOAT,
		INTEGER,
		BOOLEAN,
		LPAREN,//(
		RPAREN,//)
		LBRACE,//{
		RBRACE,//}
		LBRACKET,//[
		RBRACKET,//]
		DOT,
		COMMA,
		SEMICOLON,

		EQL,
		EQL_EQL,

		BANG,
		BANG_EQL,

		PLUS,
		PLUS_EQL,
		PLUS_PLUS,

		MINUS,
		MINUS_EQL,
		MINUS_MINUS,

		STAR,
		STAR_EQL,

		SLASH,
		SLASH_EQL,

		MOD,
		MOD_EQL,

		CARROT,
		CARROT_EQL,

		LESS,
		LESS_EQL,
		LESS_LESS,
		LESS_LESS_EQL,

		GREAT,
		GREAT_EQL,
		GREAT_GREAT,
		GREAT_GREAT_EQL,

		PIPE_PIPE,
		PIPE,
		AMP_AMP,
		AMP,

		QUERY,
		COLON,

		NEWLINE,
		WHITESPACE,
		PREPROCESSOR_TOKEN,

		BACK_SLASH,
		AT,

		STRING,
		
		EOT
	} type;
	size_t line;
	std::string file;
	std::string text;
};

static const Token NULL_TOKEN{ Token::UNKNOWN, (size_t)-1, "" };

static const std::unordered_set<std::string> keywords = {
	"in", "out", "inout", "if", "else", "for", "while", "do", "break",
	"return", "true", "false", "const"
};

static const std::unordered_set<std::string> types = {
	"void", "string", "bool",
	"float", "vec2", "vec3", "vec4",
	"int", "ivec2", "ivec3", "ivec4",
	"uint", "uvec2", "uvec3", "uvec4",
	"uint8",
	"mat2", "mat3", "mat4",
	"mat2x2", "mat2x3", "mat2x4",
	"mat3x2", "mat3x3", "mat3x4",
	"mat4x2", "mat4x3", "mat4x4",
	"dmat2", "dmat3", "dmat4",
	"dmat2x2", "dmat2x3", "dmat2x4",
	"dmat3x2", "dmat3x3", "dmat3x4",
	"dmat4x2", "dmat4x3", "dmat4x4",
};

static std::string to_string(Token::Type type)
{
	return std::vector<std::string> {
		"UNKNOWN",
		"KEYWORD",
		"IDENT",
		"FLOAT",
		"INTEGER",
		"BOOLEAN",
		"LPAREN",//(
		"RPAREN",//)
		"LBRACE",//{
		"RBRACE",//}
		"LBRACKET",//[
		"RBRACKET",//]
		"DOT",
		"COMMA",
		"SEMICOLON",
			
		"EQL",
		"EQL_EQL",
			
		"BANG",
		"BANG_EQL",
			
		"PLUS",
		"PLUS_EQL",
		"PLUS_PLUS",
			
		"MINUS",
		"MINUS_EQL",
		"MINUS_MINUS",
			
		"STAR",
		"STAR_EQL",
			
		"SLASH",
		"SLASH_EQL",
			
		"MOD",
		"MOD_EQL",
			
		"CARROT",
		"CARROT_EQL",
			
		"LESS",
		"LESS_EQL",
		"LESS_LESS",
		"LESS_LESS_EQL",
			
		"GREAT",
		"GREAT_EQL",
		"GREAT_GREAT",
		"GREAT_GREAT_EQL",
			
		"PIPE_PIPE",
		"PIPE",
		"AMP_AMP",
		"AMP",
			
		"QUERY",
		"COLON",
			
		"NEWLINE",
		"WHITESPACE",
		"PREPROCESSOR_TOKEN",
			
		"STRING",
			
		"EOT"
	}[type];
}

using Tokens = std::list<Token>;
using TokensIter = Tokens::iterator;