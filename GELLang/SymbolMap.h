// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <unordered_map>
#include <string>
#include "DataType.h"

struct Symbol
{
    std::string name;
    DataType* type;
    bool isConst;
    bool isFunc;
};

struct SymbolMap
{
    SymbolMap* parent;
    std::unordered_map<std::string, Symbol*> symbols;

    Symbol* get(std::string symbolName);
    Symbol* def(std::string symbolName);
    SymbolMap* push();
    SymbolMap* pop();
};