// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <ctype.h>

inline unsigned constexpr const_hash(char const* input) {
	return *input ?
		static_cast<unsigned int>(*input) + 33 * const_hash(input + 1) :
		5381;
}

inline unsigned constexpr operator "" _h(char const* input) {
	return const_hash(input);
}

inline unsigned hash(char const* input) {
	return *input ?
		static_cast<unsigned int>(*input) + 33 * const_hash(input + 1) :
		5381;
}

inline unsigned hash_lower(char const* input) {
	return *input ?
		static_cast<unsigned int>(tolower(*input)) + 33 * const_hash(input + 1) :
		5381;
}