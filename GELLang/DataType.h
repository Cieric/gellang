// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <stdint.h>
#include <vector>
#include <string>
#include "Assembler/SpirV/SpirvOpParamTypes.h"

struct DataType
{
	enum Type
	{
		VOID,
		BOOL,
		INT,
		FLOAT,
		VECTOR,
		MATRIX,
		POINTER,
		STRUCT,
		FUNCTION,
		STRING,
	} type;
	bool castable;
	bool constant;
	DataType(Type type, bool constant = false) :
		type(type),
		constant(constant),
		castable(false)
	{}
};

struct DataTypeVoid : DataType
{
	DataTypeVoid() :
		DataType(Type::VOID, false)
	{}
};
struct DataTypeBool : DataType
{
	DataTypeBool(bool constant = false) :
		DataType(Type::BOOL, constant)
	{}
};
struct DataTypeInt : DataType
{
	uint32_t Width;
	uint32_t Signedness;
	DataTypeInt(uint32_t Width, uint32_t Signedness) :
		DataType(Type::INT),
		Width(Width),
		Signedness(Signedness)
	{}
	DataTypeInt(bool constant, uint32_t Width, uint32_t Signedness) :
		DataType(Type::INT, constant),
		Width(Width),
		Signedness(Signedness)
	{}
};
struct DataTypeString : DataType
{
	DataTypeString() :
		DataType(Type::STRING, true)
	{}
};
struct DataTypeFloat : DataType
{
	uint32_t Width;
	DataTypeFloat(uint32_t Width) :
		DataType(Type::FLOAT),
		Width(Width)
	{}
	DataTypeFloat(bool constant, uint32_t Width) :
		DataType(Type::FLOAT, constant),
		Width(Width)
	{}
};
struct DataTypeVector : DataType
{
	DataType* ComponentType;
	uint32_t ComponentCount;
	DataTypeVector(DataType* ComponentType, uint32_t ComponentCount) :
		DataType(Type::VECTOR),
		ComponentType(ComponentType),
		ComponentCount(ComponentCount)
	{}
	DataTypeVector(bool constant, DataType* ComponentType, uint32_t ComponentCount) :
		DataType(Type::VECTOR, constant),
		ComponentType(ComponentType),
		ComponentCount(ComponentCount)
	{}
};
struct DataTypeMatrix : DataType
{
	DataType* ComponentType;
	uint32_t ComponentCount;
	DataTypeMatrix(DataType* ComponentType, uint32_t ComponentCount) :
		DataType(Type::MATRIX),
		ComponentType(ComponentType),
		ComponentCount(ComponentCount)
	{}
	DataTypeMatrix(bool constant, DataType* ComponentType, uint32_t ComponentCount) :
		DataType(Type::MATRIX, constant),
		ComponentType(ComponentType),
		ComponentCount(ComponentCount)
	{}
};
struct DataTypePointer : DataType
{
	StorageClass storageClass;
	DataType* ComponentType;
	DataTypePointer(StorageClass storageClass, DataType* ComponentType) :
		DataType(Type::POINTER),
		storageClass(storageClass),
		ComponentType(ComponentType)
	{}
	DataTypePointer(bool constant, StorageClass storageClass, DataType* ComponentType) :
		DataType(Type::POINTER, constant),
		storageClass(storageClass),
		ComponentType(ComponentType)
	{}
};
struct DataTypeStruct : DataType
{
	std::string name;
	std::vector<DataType*> members;
	DataTypeStruct(std::string name, std::vector<DataType*> members) :
		DataType(Type::STRUCT),
		name(name),
		members(members)
	{}
};
struct DataTypeFunction : DataType
{
	DataType* ReturnType;
	std::vector<DataType*> ParameterTypes;
	DataTypeFunction(DataType* ReturnType, std::vector<DataType*> ParameterTypes) :
		DataType(Type::FUNCTION),
		ReturnType(ReturnType),
		ParameterTypes(ParameterTypes)
	{}
};

DataType* Copy(DataType* other);

DataType::Type getBaseType(DataType* type);
DataType* getBaseDataType(DataType* type);
void setBaseType(DataType* type, DataType::Type newBaseType);

bool isScaler(DataType* type);
bool isVector(DataType* type);
bool isMatrix(DataType* type);

std::string ShortName(DataType* datatype);

bool DataTypesEqual(DataType* dt1, DataType* dt2);
bool DataTypeUpcastable(DataType* dt1, DataType* dt2);