// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <thread>
#include "tokenizer.h"
#include "token.h"
#include "preprocessor.h"
#include "parser.h"
#include "utilities.h"
#include "OS.h"
#include "hash.h"

#include "Assembler/SpirV/SpirvVM.h"
#include "Assembler/SpirV/SpirvCompiler.h"
#include "Assembler/SpirV/SpirvAssembler.h"
#include "Assembler/SpirV/SpirvChunkOptimizer.h"

struct Args
{
	char* input = nullptr;
	char* output = nullptr;
};

template<typename T>
bool toNumber(T& ret, const char* arg, int base = 10)
{

	for (const char* c = arg; *c != '\0'; c++) {
		if (isdigit(*c) == 0) return false;
	}
	return true;
}

bool isTruthy(char* arg, bool* ret)
{
	switch (hash_lower(arg))
	{
	case const_hash("true"):
	case const_hash("1"):
		*ret = true;
		return true;

	case const_hash("false"):
	case const_hash("0"):
		*ret = false;
		return true;
	}

	return false;
}

const char* ParseArgs(Args& output, int argc, char** argv)
{
	for (int i = 1; i < argc;)
	{
		switch (hash(argv[i]))
		{
		case const_hash("-o"):
		case const_hash("--output"): {
			output.output = argv[i + 1];
			i += 2;
		} break;
		default: {
			if (output.input) return "Multiple inputs provided when only one is supported!";
			output.input = argv[i];
			i += 1;
		}break;
		}
	}
	return nullptr;
}

int main(int argc, char** argv)
{
	Args args = Args();
	const char* err;
	if ((err = ParseArgs(args, argc, argv)) != nullptr)
	{
		fprintf(stdout, "Error: %s\n", err);
		return -1;
	}

	if(args.input == nullptr)
	{
		fprintf(stdout, "Error: No input file provided!\n");
		return -1;
	}

	if(args.output == nullptr)
	{
		fprintf(stdout, "Error: No output file provided!\n");
		return -1;
	}

	std::ifstream file;
	file.open(args.input, std::ios::in | std::ios::binary);
	if (errno)
	{
		std::cerr << "Error: " << std::strerror(errno);
		return 1;
	}
	std::stringstream source;
	source << file.rdbuf();
	file.close();

	Tokenizer tokenizer(std::move(source));
	tokenizer.filename = args.input;
	auto tokens = tokenizer.GetAll();

	if (tokenizer.errors.size() > 0)
	{
		for (std::string err : tokenizer.errors)
			printf("Error: %s\n", err.c_str());
		return 2;
	}

	Preprocessor preprocessor(tokens);
	auto preprocessedTokens = preprocessor.process();

	if (preprocessor.errors.size() > 0)
	{
		for (std::string err : preprocessor.errors)
			printf("Error: %s\n", err.c_str());
		return 3;
	}

	Parser parser(preprocessedTokens);

	auto program = parser.process();

	SpirV::Compiler* compiler = new SpirV::Compiler(program);
	SpirV::AstList astList = compiler->Process();
	auto funcs = compiler->GetFuncs();

	SpirV::Assembler* assembler = new SpirV::Assembler(astList, funcs);
	Chunk chunk = assembler->Process();
	uint32_t bound = assembler->PeekResult();

	SpirV::Optimizer optimizer(chunk);
	Chunk optimizedChunk = optimizer.Process(); 
	
	SpirvVM vm;
	vm.disassembleChunk(&chunk, "compiled chunk");

	auto fptr = fopen(args.output, "wb");
	if (fptr == NULL)
	{
		printf("Failed to open output file!");
		return 4;
	}

	static const uint32_t SpvMagicNumber = 0x07230203;
	fwrite(&SpvMagicNumber, 1, sizeof(SpvMagicNumber), fptr);
	static const uint32_t SpvVersion = 0x00010500;
	fwrite(&SpvVersion, 1, sizeof(SpvVersion), fptr);
	static const uint32_t SpvGenerator = -1;
	fwrite(&SpvGenerator, 1, sizeof(SpvGenerator), fptr);
	uint32_t SpvBound = bound;
	fwrite(&SpvBound, 1, sizeof(SpvBound), fptr);
	static const uint32_t SpvNULL = 0;
	fwrite(&SpvNULL, 1, sizeof(SpvNULL), fptr);
		
	fwrite(chunk.code.data(), chunk.code.size(), sizeof(uint32_t), fptr);
	fclose(fptr);

	delete compiler;

	return EXIT_SUCCESS;
}
