// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "DataType.h"
#include "CompilerUtils.h"

DataType* Copy(DataType* other)
{
    switch (other->type)
    {
    case DataType::BOOL: return new DataTypeFloat(*(DataTypeFloat*)other);
    case DataType::INT: return new DataTypeFloat(*(DataTypeFloat*)other);
    case DataType::FLOAT: return new DataTypeFloat(*(DataTypeFloat*)other);
    case DataType::STRING: return new DataTypeString(*(DataTypeString*)other);
    case DataType::VECTOR: {
        auto temp = new DataTypeVector(*(DataTypeVector*)other);
        temp->ComponentType = Copy(temp->ComponentType);
        return temp;
    }
    case DataType::MATRIX: {
        auto temp = new DataTypeMatrix(*(DataTypeMatrix*)other);
        temp->ComponentType = Copy(temp->ComponentType);
        return temp;
    }
    case DataType::POINTER: {
        auto temp = new DataTypePointer(*(DataTypePointer*)other);
        temp->ComponentType = Copy(temp->ComponentType);
        return temp;
    }
    }
    CompilerError("Implement later!");
}

DataType::Type getBaseType(DataType* type)
{
    if (type->type == DataType::VECTOR)
        return getBaseType(((DataTypeVector*)type)->ComponentType);
    if (type->type == DataType::MATRIX)
        return getBaseType(((DataTypeVector*)type)->ComponentType);
    return type->type;
}

DataType* getBaseDataType(DataType* type)
{
    if (type->type == DataType::VECTOR)
        return getBaseDataType(((DataTypeVector*)type)->ComponentType);
    if (type->type == DataType::MATRIX)
        return getBaseDataType(((DataTypeVector*)type)->ComponentType);
    if (type->type == DataType::POINTER)
        return getBaseDataType(((DataTypeVector*)type)->ComponentType);
    return type;
}

void setBaseType(DataType* type, DataType::Type newBaseType)
{
    if (type->type == DataType::VECTOR)
        return setBaseType(((DataTypeVector*)type)->ComponentType, newBaseType);
    if (type->type == DataType::MATRIX)
        return setBaseType(((DataTypeVector*)type)->ComponentType, newBaseType);
    if (type->type == DataType::POINTER)
        return setBaseType(((DataTypeVector*)type)->ComponentType, newBaseType);
    type->type = newBaseType;
}

bool isScaler(DataType* type)
{
    switch (type->type)
    {
    case DataType::BOOL:
    case DataType::INT:
    case DataType::FLOAT:
        return true;
    }
    return false;
}

bool isVector(DataType* type)
{
    switch (type->type)
    {
    case DataType::VECTOR:
        return true;
    }
    return false;
}

bool isMatrix(DataType* type)
{
    switch (type->type)
    {
    case DataType::MATRIX:
        return true;
    }
    return false;
}

std::string ShortName(DataType* datatype)
{
    if (datatype->constant)
    {
        auto ndt = Copy(datatype);
        ndt->constant = false;
        auto ret = "!" + ShortName(ndt);
        delete ndt;
        return ret;
    }
    switch (datatype->type)
    {
    case DataType::BOOL: {
        return "b";
    }
    case DataType::INT: {
        DataTypeInt* data = (DataTypeInt*)datatype;
        return "i" + std::to_string(data->Width);
    }
    case DataType::FLOAT: {
        DataTypeFloat* data = (DataTypeFloat*)datatype;
        return "f" + std::to_string(data->Width);
    }
    case DataType::VECTOR: {
        DataTypeVector* data = (DataTypeVector*)datatype;
        return "v" + std::to_string(data->ComponentCount) + ShortName(data->ComponentType);
    }
    case DataType::MATRIX: {
        DataTypeVector* data = (DataTypeVector*)datatype;
        return "m" + std::to_string(data->ComponentCount) + ShortName(data->ComponentType);
    }
    case DataType::POINTER: {
        DataTypePointer* data = (DataTypePointer*)datatype;
        return "p" + ShortName(data->ComponentType);
    }
    }
    return "ERR";
}

bool DataTypesEqual(DataType* dt1, DataType* dt2)
{
    if (dt1 == nullptr || dt2 == nullptr)
        return dt1 == dt2;
    if (dt1->type != dt2->type) return false;
    switch (dt1->type)
    {
    case DataType::VOID:
    case DataType::BOOL:
        return true;
    case DataType::FLOAT: {
        DataTypeFloat* dtf1 = (DataTypeFloat*)dt1;
        DataTypeFloat* dtf2 = (DataTypeFloat*)dt2;
        return dtf1->Width == dtf2->Width;
    }
    case DataType::INT: {
        DataTypeInt* dtf1 = (DataTypeInt*)dt1;
        DataTypeInt* dtf2 = (DataTypeInt*)dt2;
        return dtf1->Signedness == dtf2->Signedness && dtf1->Width == dtf2->Width;
    }
    case DataType::POINTER: {
        DataTypePointer* dtf1 = (DataTypePointer*)dt1;
        DataTypePointer* dtf2 = (DataTypePointer*)dt2;
        return dtf1->storageClass == dtf2->storageClass && DataTypesEqual(dtf1->ComponentType, dtf2->ComponentType);
    }
    case DataType::STRUCT: {
        DataTypeStruct* dts1 = (DataTypeStruct*)dt1;
        DataTypeStruct* dts2 = (DataTypeStruct*)dt2;
        if (dts1->members.size() != dts2->members.size()) return false;
        for (int i = 0; i < dts1->members.size(); ++i)
        {
            if (!DataTypesEqual(dts1->members[i], dts2->members[i])) return false;
        }
        return true;
    }
    case DataType::FUNCTION: {
        DataTypeFunction* dtf1 = (DataTypeFunction*)dt1;
        DataTypeFunction* dtf2 = (DataTypeFunction*)dt2;
        if (!DataTypesEqual(dtf1->ReturnType, dtf2->ReturnType)) return false;
        if (dtf1->ParameterTypes.size() != dtf2->ParameterTypes.size()) return false;
        for (int i = 0; i < dtf1->ParameterTypes.size(); ++i)
        {
            if (!DataTypesEqual(dtf1->ParameterTypes[i], dtf2->ParameterTypes[i])) return false;
        }
        return true;
    }
    case DataType::VECTOR: {
        DataTypeVector* dtv1 = (DataTypeVector*)dt1;
        DataTypeVector* dtv2 = (DataTypeVector*)dt2;
        if (!DataTypesEqual(dtv1->ComponentType, dtv2->ComponentType)) return false;
        if (dtv1->ComponentCount != dtv2->ComponentCount) return false;
        return true;
    }
    case DataType::MATRIX: {
        DataTypeMatrix* dtm1 = (DataTypeMatrix*)dt1;
        DataTypeMatrix* dtm2 = (DataTypeMatrix*)dt2;
        if (!DataTypesEqual(dtm1->ComponentType, dtm2->ComponentType)) return false;
        if (dtm1->ComponentCount != dtm2->ComponentCount) return false;
        return true;
    }
    default:
        CompilerError("Unhandled!");
        //return true;
    }
}

bool DataTypeUpcastable(DataType* dt1, DataType* dt2)
{
    if (dt1 == nullptr || dt2 == nullptr)
        return dt1 == dt2;
    //if (dt1->type != dt2->type) return false;
    if (DataTypesEqual(dt1, dt2)) return true;
    if (dt1->type == DataType::INT && dt2->type == DataType::INT) return true;
    if (dt1->type == DataType::INT && dt2->type == DataType::FLOAT) return true;
    if (dt2->type == DataType::VECTOR && DataTypeUpcastable(dt1, getBaseDataType(dt2))) return true;
    return false;
}