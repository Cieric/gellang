// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include <iostream>

char* readFile(const char* filepath, size_t* length);
const char *sstrstr(const char *haystack, const char *needle, size_t length);