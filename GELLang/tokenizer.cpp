// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "tokenizer.h"
#include <ctype.h>
#include <assert.h>
#include <algorithm>

Tokenizer::Tokenizer(std::stringstream&& _source) :
	source(std::move(_source)), buffer("")
{
	source >> std::noskipws ;
}

Token Tokenizer::Next()
{
	buffer = "";
	char c = Advance();
	switch (c)
	{
	case 0:
	case EOT: return BuildToken( Token::EOT );

	case '(': return BuildToken(Token::LPAREN);
	case ')': return BuildToken(Token::RPAREN);
	case '{': return BuildToken(Token::LBRACE);
	case '}': return BuildToken(Token::RBRACE);
	case '[': return BuildToken(Token::LBRACKET);
	case ']': return BuildToken(Token::RBRACKET);
	case ',': return BuildToken(Token::COMMA);
	case ';': return BuildToken(Token::SEMICOLON);


	case '=': return BuildToken(Check('=') ? (Token::EQL_EQL) : (Token::EQL));
	case '!': return BuildToken(Check('=') ? (Token::BANG_EQL) : (Token::BANG));
	
	case '+': return BuildToken(Check('=') ? (Token::PLUS_EQL) : Check('+') ? (Token::PLUS_PLUS) : (Token::PLUS));
	case '-': return BuildToken(Check('=') ? (Token::MINUS_EQL) : Check('-') ? (Token::MINUS_MINUS) : (Token::MINUS));
	
	case '*': return BuildToken(Check('=') ? (Token::STAR_EQL) : (Token::STAR));
	case '%': return BuildToken(Check('=') ? (Token::MOD_EQL) : (Token::MOD));
	case '^': return BuildToken(Check('=') ? (Token::CARROT_EQL) : (Token::CARROT));

	case '<': return BuildToken(Check('<') ? (Token::LESS_LESS) : (Check('=') ? (Token::LESS_EQL) : (Token::LESS)));
	case '>': return BuildToken(Check('>') ? (Token::GREAT_GREAT) : (Check('=') ? (Token::GREAT_EQL) : (Token::GREAT)));

	case '?': return BuildToken(Token::QUERY);
	case ':': return BuildToken(Token::COLON);

	case '|': {
		Consume('|', "Expected a '|' but got a " + c);
		return BuildToken(Token::PIPE_PIPE);
	}

	case '@': return BuildToken(Token::AT);
	case '&': return BuildToken(Check('&') ? (Token::AMP_AMP) : (Token::AMP));

	case '/': {
		if (Check('/')) {
			SkipComment();
			return Next();
		}
		if (Check('*')) {
			SkipComment(true);
			return Next();
		}

		return BuildToken(Check('=') ? (Token::SLASH_EQL) : (Token::SLASH));
	}

	case '\\':{
		if(Peek() == '\r' || Peek() == '\n')
		{
			if (Peek() == '\r') Advance();
			if (Peek() == '\n') Advance();
			NewLine();
		}
		return BuildToken(Token::BACK_SLASH);
	}

	case '#': return BuildToken(Token::PREPROCESSOR_TOKEN);

	//Skip whitespace
	case '\r':
		if(Peek() == '\n') Advance();
	case '\n':
	{
		auto token = BuildToken(Token::NEWLINE, "");
		NewLine();
		return token;
	}

	case '"': {
		while (Advance() != '"');
		auto token = BuildToken(Token::STRING);
		token.text = token.text.substr(1, token.text.size() - 2);
		return token;
	} break;

	case '\t':
	case ' ':
		while (CheckAny({' ','\t'}));
		return BuildToken(Token::WHITESPACE);
	
	default: {
		if (isalpha(c) || c=='_') return ParseWord(c);
		if (isdigit(c) || c=='.') return ParseNumber(c);
		printf("Unknown character: \"%c\" %02hhX\n", c, c);
	}
	}

	return BuildToken(Token::UNKNOWN);
}

Tokens Tokenizer::GetAll()
{
	Tokens tokens;
	Token token;
	while ((token = Next()).type != Token::EOT)
		tokens.push_back(token);
	tokens.push_back(token);
	return tokens;
}

Token Tokenizer::BuildToken(Token::Type type)
{
	return Token{ type, line, filename, buffer };
}

Token Tokenizer::BuildToken(Token::Type type, std::string text)
{
	return Token{ type, line, filename, text };
}

char Tokenizer::Advance()
{
	char c;
	source >> c;
	buffer += c;
	column++;
	if(source.eof()) c = EOT;
	return c;
}

void Tokenizer::NewLine()
{
	line++;
	column = 0;
}

void Tokenizer::setError(std::string error)
{
	errors.push_back(error);
}

bool Tokenizer::Consume(char c, std::string err)
{
	if (Check(c))
		return true;
	setError(err);
	return false;
}

bool Tokenizer::Check(char c)
{
	if (source.peek() == c)
	{
		buffer += c;
		source.ignore(1);
		return true;
	}
	return false;
}

char Tokenizer::Prev()
{
	return buffer[buffer.size() - 1];
}

bool Tokenizer::CheckAny(std::vector<char> s)
{
	if (std::find(s.begin(), s.end(), source.peek()) != s.end()) {
		buffer += source.peek();
		source.ignore(1);
		return true;
	}
	return false;
}

bool Tokenizer::MatchAny(std::vector<char> s)
{
	return std::find(s.begin(), s.end(), source.peek()) != s.end();
}

char Tokenizer::Peek()
{
	if(source.eof()) return EOT;
	return source.peek();
}

Token Tokenizer::ParseWord(char c)
{
	while(isalnum(Peek()) || Peek() == '_') Advance();
	if (keywords.find(buffer) != keywords.end()) return BuildToken(Token::KEYWORD);
	if (types.find(buffer) != types.end()) return BuildToken(Token::KEYWORD);
	if (buffer == "__inlineasm") return BuildToken(Token::KEYWORD);
	if (buffer == "struct") return BuildToken(Token::KEYWORD);
	return BuildToken(Token::IDENT);
}

bool ishex(char c)
{
	return isdigit(c) || (c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F');
}

Token Tokenizer::ParseNumber(char c)
{
	//integerConstant:
	//	decimalConstant integerSuffix_opt
	//	octalConstant integerSuffix_opt
	//	hexadecimalConstant integerSuffix_opt
	//integerSuffix:
	//	[uU]
	//decimalConstant:
	//	nonzeroDigit
	//  decimalConstant digit
	//octalConstant:
	//	'0'
	//	octalConstant octalDigit
	//hexadecimalConstant:
	//	"0x" hexadecimalDigit
	//  "0X" hexadecimalDigit
	//	hexadecimalConstant hexadecimalDigit
	//digit:
	//	'0'
	//	nonzeroDigit
	//nonzeroDigit:
	//	[1-9]
	//octalDigit:
	//	[0-7]
	//hexadecimalDigit:
	//	[0-9a-fA-F]

	//floatingConstant:
	//	fractionalConstant exponentPart_opt floatingSuffix_opt
	//	digitSequence exponentPart floatingSuffix_opt
	//fractionalConstant:
	//	digitSequence '.' digitSequence
	//	digitSequence '.'
	//  '.' digitSequence
	//exponentPart:
	//	'e' sign digitSequence
	//	'E' sign digitSequence
	//sign:
	//	[+-]
	//digitSequence:
	//	digit
	//	digitSequence digit
	//floatingSuffix:
	//	(f|F|lf|LF)
	
	//[int][float][hex][octal][dot]
	
	bool hex = false;
	
	if (c == '0') //Hex or Octal
	{
		if (CheckAny({'x', 'X'}))
		{
			c = Peek();
			hex = true;
			while (ishex(c))
			{
				Advance();
				c = Peek();
			}
		}
		else
		{
			c = Peek();
			while (c >= '0' && c <= '7')
			{
				Advance();
				c = Peek();
			}
		}

		c = Peek();
		if (!hex && MatchAny({ '.', 'l', 'L', 'f', 'F', 'e' , 'E' })) // well actually it's a float
		{
			if (Check('.'))
			{
				c = Peek();
				while (isdigit(c)) {
					Advance();
					c = Peek();
				}
			}

			if (CheckAny({ 'e', 'E' }))
			{
				c = Peek();
				while (isdigit(c)) {
					Advance();
					c = Peek();
				}
			}

			if (c == 'l' && Peek() == 'f') c = Advance(), Advance();
			if (c == 'f') c = Advance();
			if (c == 'L' && Peek() == 'F') c = Advance(), Advance();
			if (c == 'F') c = Advance();

			return BuildToken(Token::FLOAT);
		}

		return BuildToken(Token::INTEGER);
	}
	else
	{
		bool isDot = true;
		if(c == '.' && !isdigit(Peek()))
			return BuildToken(Token::DOT);

		bool isFloat = false;

		if (c == '.') isFloat = true;

		c = Peek();
		while (isdigit(c)) {
			isDot = false;
			Advance();
			c = Peek();
		}

		if (MatchAny({'.', 'l', 'L', 'f', 'F', 'e' , 'E' })) // well actually it's a float
		{
			isFloat = true;
			if (Check('.'))
			{
				c = Peek();
				while (isdigit(c)) {
					isDot = false;
					Advance();
					c = Peek();
				}
			}

			if (CheckAny({ 'e', 'E' }))
			{
				c = Peek();
				while (isdigit(c)) {
					isDot = false;
					Advance();
					c = Peek();
				}
			}

			if (!isDot)
			{
				if (c == 'l' && Peek() == 'f') c = Advance(), Advance();
				if (c == 'f') c = Advance();
				if (c == 'L' && Peek() == 'F') c = Advance(), Advance();
				if (c == 'F') c = Advance();
			}
		}
		return BuildToken(isFloat?Token::FLOAT:Token::INTEGER);
	}
}

void Tokenizer::SkipComment(bool mutiline)
{
	//TODO: Redo this
	if(mutiline)
	{
		while(true)
		{
			char c = Advance();
			if(c == '\r' || c == '\n')
			{
				if(c == '\r') Advance();
				if(c == '\n') Advance();
				NewLine();
			}
			else if(c == '*')
			{
				c = Advance();
				if(c == '/')
					break;
			}
			else if (c == EOT)
			{
				break;
			}
		}
	}
	else
	{
		while(true)
		{
			char c = Advance();
			if(c == '\\')
			{
				if(Peek() == '\r') c = Advance();
				if(Peek() == '\n') c = Advance();
				NewLine();
			}
			else if(c == '\r' || c == '\n')
			{
				if (c == '\r' && Peek() == '\n') Advance();
				NewLine();
				break;
			}
			else if (c == EOT)
			{
				break;
			}
		}
	}
}