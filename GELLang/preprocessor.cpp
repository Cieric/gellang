// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "preprocessor.h"
#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
#include <sstream>
#include <cstring>
#include <assert.h>
#include <fstream>
#include <deque>
#include "tokenizer.h"
#include "utilities.h"

Token Preprocessor::token()
{
    if (tokens.size() == 0)
        return NULL_TOKEN;
    //printf("%d %s\n", tokens.size(), tokens.front().text.c_str());
    return tokens.front();
};

Preprocessor::Preprocessor(Tokens tokens) :
    tokens(tokens.begin(), tokens.end())
{
};

void Preprocessor::Advance()
{
    tokens.pop_front();
}

bool Preprocessor::Check(Token::Type type)
{
    if(token().type == type)
    {
        Advance();
        return true;
    }
    return false;
}

Token Preprocessor::Consume(Token::Type type, std::string error)
{
    auto temp = token();
    if (!Check(type))
        setError(error);
    return temp;
}

Tokens Preprocessor::process()
{
    Tokens output;

    bool onNewLine = true;
    
    while(tokens.size() > 0)
    {
        switch(token().type)
        {
            case Token::PREPROCESSOR_TOKEN: {
                Consume(Token::PREPROCESSOR_TOKEN, "Critical Error: Token was an PREPROCESSOR_TOKEN now it's not WTF.");
                Token command = token();
                if (!Check(Token::IDENT) && !Check(Token::KEYWORD))
                    setError("Identifier expected, found " + to_string(token().type));

                if (outputCode == 0 && command.text == "define")
                {
                    parseDefine();
                }
                else if (command.text == "include")
                {
                    Tokens includeTokens = parseInclude();
                    std::copy(includeTokens.begin(), std::prev(includeTokens.end()), std::inserter(tokens, tokens.begin()));
                }
                else if (command.text == "ifdef")
                {
                    if (outputCode > 0)
                        outputCode++;
                    else
                        parseIfDefine();
                    Advance();
                }
                else if (command.text == "ifndef")
                {
                    if (outputCode > 0)
                        outputCode++;
                    else
                        parseIfNDefine();
                    Advance();
                }
                else if (command.text == "if")
                {
                    if (outputCode > 0)
                        outputCode++;
                    else
                        parseIf();
                    Advance();
                }
                else if (command.text == "else")
                {
                    if (outputCode == 1)
                        outputCode = 0;
                    else if (outputCode == 0)
                        outputCode = 1;

                    Advance();
                }
                else if (command.text == "endif")
                {
                    if (outputCode > 0)
                        outputCode -= 1;
                    Advance();
                }
                else if (command.text == "version")
                {
                    while (token().type == Token::WHITESPACE) Advance();
                    Token version = Consume(Token::INTEGER, "Error: Integer expected for version, found " + to_string(token().type));
                    while (token().type == Token::WHITESPACE) Advance();
                    Advance();
                    std::vector<Token> includeTokens = {
                        Token{Token::PREPROCESSOR_TOKEN, version.line, version.file, "#"},
                        Token{Token::KEYWORD, version.line, version.file, "include"},
                        Token{Token::WHITESPACE, version.line, version.file, " "},
                        Token{Token::LESS, version.line, version.file, "<"},
                        Token{Token::STRING, version.line, version.file, "versions/GLSL" + version.text + ".glsl" },
                        Token{Token::GREAT, version.line, version.file, ">"},
                        Token{Token::NEWLINE, version.line, version.file, "\n"}
                    };
                    std::copy(includeTokens.begin(), std::prev(includeTokens.end()), std::inserter(tokens, tokens.begin()));
                }
                else if (command.text == "extension")
                {
                    if (token().type == Token::WHITESPACE) Advance();
                    auto temp1 = Consume(Token::IDENT, "Error: Identifier expected, found " + to_string(token().type));
                    if (token().type == Token::WHITESPACE) Advance();
                    auto temp2 = Consume(Token::COLON, "Error: Colon expected, found " + to_string(token().type));
                    if (token().type == Token::WHITESPACE) Advance();
                    auto temp3 = Consume(Token::IDENT, "Error: Identifier expected, found " + to_string(token().type));
                    if (token().type == Token::WHITESPACE) Advance();
                    Advance();
                }
                else
                {
                    printf("unknown command: %s\n", command.text.c_str());
                    Advance();
                }
            } break;
            case Token::KEYWORD:
            case Token::IDENT: {
                if (outputCode == 0)
                {
                    auto defineIter = defines.find(token().text);
                    if (defineIter != defines.end())
                    {
                        Advance();
                        auto define = defineIter->second;

                        auto newTokens = processDefine(define);
                        std::copy(newTokens.begin(), newTokens.end(), std::inserter(tokens, tokens.begin()));
                    }
                    else
                    {
                        output.push_back(token());
                        Advance();
                    }
                }
                else Advance();
            } break;
            case Token::NEWLINE:
            case Token::WHITESPACE:
                Advance();
                break;
            default: {
                if (outputCode == 0)
                    output.push_back(token());
                Advance();
            } break;
        }
        if (errors.size() > 0) return {};
    }

    return output;
}

Tokens Preprocessor::processDefine(Define& define)
{
    //auto _token = token;

    std::vector<Tokens> arguments;
    if (define.parameters.size() > 0)
    {
        Tokens ntokens;
        Consume(Token::LPAREN, "expected a left parenthese, but found " + to_string(token().type));
        if (errors.size() > 0) return {};
        auto& params = define.parameters;
        for (size_t i = 0; i < params.size(); i++) {
            Tokens argument;
            int nesting = 0;
            while ((token().type != Token::RPAREN && token().type != Token::COMMA) || nesting > 0)
            {
                if (token().type == Token::LPAREN) nesting++;
                if (token().type == Token::RPAREN) nesting--;
                argument.push_back(token());
                Advance();
            }
            if (i < params.size()-1)
                Consume(Token::COMMA, "Expected a COMMA, but found " + to_string(token().type));
            arguments.push_back(argument);
        }
        Consume(Token::RPAREN, "Expected a RPAREN, but found " + to_string(token().type));
        if (errors.size() > 0) return {};

        for (auto token : define.value)
        {
            if (token.type == Token::IDENT)
            {
                auto param = params.find(token.text);
                if (param != params.end())
                {
                    size_t index = std::distance(params.begin(), param);
                    //assert(index < tokens.size());
                    Tokens& tokens = arguments[index];
                    ntokens.insert(ntokens.end(), tokens.begin(), tokens.end());
                }
                else ntokens.push_back(token);
            }
            else ntokens.push_back(token);
        }
        return ntokens;
    }
    return define.value;
}

void Preprocessor::setError(std::string errorStr)
{
    errors.push_back(errorStr);
}

void Preprocessor::addDefines(Defines& _defines)
{
    defines.insert(_defines.begin(), _defines.end());
}

//std::set<std::string> getParams(TokensIter& iter)
//{
//    if (token.type == Token::LPAREN) {
//        std::set<std::string> parameters;
//        Advance();
//        while (true) {
//
//            Token param = *iter;
//            if (param.type != Token::IDENT) return {};
//            parameters.insert(param.text);
//
//            if (token.type == Token::RPAREN) { Advance(); break; }
//            if (token.type == Token::COMMA) { Advance(); continue; }
//            
//            return {};
//        }
//        return parameters;
//    }
//    return {};
//}

Tokens Preprocessor::parseDefine()
{
    while (token().type == Token::WHITESPACE) Advance();

    //Get Key
    std::string key = token().text;
    Advance();

    //Parse Parameters
    std::set<std::string> parameters;
    if (token().type == Token::LPAREN) {
        Consume(Token::LPAREN, "expected a left parenthese, but found " + to_string(token().type));
        while (true) {
            Token param = Consume(Token::IDENT, "(Line#" + std::to_string(token().line) + ") Exprected an IDENT, but found " + to_string(token().type));
            parameters.insert(param.text);

            if (Check(Token::RPAREN)) { break; }
            if (Check(Token::COMMA)) { continue; }
            setError("expected a comma or a right parentheses");
            return {};
        }
    }

    while (token().type == Token::WHITESPACE) Advance();

    Tokens value;
    while (token().type != Token::NEWLINE)
    {
        if (token().type == Token::WHITESPACE) {
            Advance(); continue;
        }
        value.push_back(token());
        Advance();
    }
    defines.insert_or_assign(key, Define{
        key,
        parameters,
        value
        });
    return Tokens{};
}

Tokens Preprocessor::parseInclude()
{
    while(token().type == Token::WHITESPACE) Advance();
    if (token().type == Token::LESS)
    {
        Advance();
        std::string path;
        if (token().type == Token::WHITESPACE) Advance();
        while (token().type != Token::GREAT && token().type != Token::NEWLINE)
        {
            path += token().text;
            Advance();
        }
        if (!Check(Token::GREAT))
        {
            setError("Expected '>' after include path!");
            return {};
        }

        std::ifstream file;
        file.open("builtins/" + path, std::ios::in | std::ios::binary);
        if (errno)
        {
            setError("file not found ("+path+")");
            return Tokens();
        }
        std::stringstream source2;
        source2 << file.rdbuf();
        file.close();
        auto tokenizer = Tokenizer(std::move(source2));
        tokenizer.filename = path;
        auto tokens = tokenizer.GetAll();
        return tokens;
    }
    return Tokens();
}

Tokens Preprocessor::parseIfDefine()
{
    while (token().type == Token::WHITESPACE) Advance();

    //Get Key
    std::string key = token().text;
    Advance();

    outputCode = defines.find(key) != defines.end();

    return Tokens();
}

Tokens Preprocessor::parseIfNDefine()
{
    while (token().type == Token::WHITESPACE) Advance();

    //Get Key
    std::string key = token().text;
    Advance();

    outputCode = defines.find(key) == defines.end();

    return Tokens();
}

bool isTruthy(std::string lit)
{
    if (lit == "false" || lit == "0" || lit == "")
        return false;
    return true;
}

Tokens Preprocessor::parseIf()
{
    while (token().type == Token::WHITESPACE) Advance();

    //Get Key
    std::string key = token().text;
    Advance();

    auto value = defines.find(key);
    if (value != defines.end())
    {
        if (value->second.value.size() < 2)
        {
            outputCode = isTruthy(value->second.value.front().text);
        }
        else
        {
            outputCode++;
        }
        
    }

    return Tokens();
}
