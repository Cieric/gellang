// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include "astNodes/expr.h"

enum CopySpecifier
{
    IN,
    OUT,
    INOUT,
};

struct Parameter
{
    DataType* type;
    CopySpecifier copySpecifier = IN;
    bool constant;
    std::string name;
};