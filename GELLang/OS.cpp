// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "OS.h"

#ifdef _WIN32
#include <Windows.h>

short getKey(char c)
{
	return GetKeyState(c);
}
#else
short getKey(char c)
{
	return 0;
}
#endif
