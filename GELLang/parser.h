// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#pragma once
#include <type_traits> // enable_if, conjuction
#include "preprocessor.h"
#include "token.h"
#include "astNodes/expr.h"
#include "astNodes/stmt.h"
#include "SymbolMap.h"

struct Program
{
    std::vector<Stmt*> stmts;
};

class Parser
{
    Tokens tokens;
    TokensIter iter;
    int counter = 0;
    SymbolMap* symbols;
public:
    std::unordered_map<std::string, DataType*> customTypes;
    std::vector<std::string> errors;
    Parser(Tokens tokens);
    Program* process();

    Stmt* declaration();
    Stmt* structDeclaration();
    std::vector<Symbol*> structVarDeclaration();
    Stmt* varDeclaration();

    Stmt* inlineAsmDeclaration(Token type, Token name);
    std::vector<Stmt*> astBlock();
    Stmt* opCode();

    Stmt* funcDeclaration(Token type, Token name);
    std::vector<Stmt*> block();
    Stmt* statement();
    Stmt* forStatement();
    Stmt* ifStatement();
    Stmt* returnStatement();
    Stmt* whileStatement();
    Stmt* doWhileStatement();
    Stmt* breakStatement();
    Stmt* printStatement();
    Stmt* expressionStatement();

    Expr* expression();
    Expr* sequence();
    Expr* assignment();
    Expr* selection();
    Expr* logicalOr();
    Expr* logicalAnd();
    Expr* bitwiseAND();
    Expr* bitwiseXOR();
    Expr* equality();
    Expr* comparison();
    Expr* bitwiseShift();
    Expr* term();
    Expr* factor();
    Expr* unary();
    StmtStruct* getStructType(Expr* expr);
    Expr* postscripts();
    Expr* primary();

private:
    DataType* typeFromString(std::string str);
    bool match(std::vector<Token::Type> types);
    bool matchKeyword(std::string name);
    bool matchAnyKeyword(std::unordered_set<std::string> names);
    template<typename T>
    bool matchAnyKeyword(T names)
    {
        if (names.find(peek().text) != names.end())
        {
            advance();
            return true;
        }
        return false;
    }
    bool check(Token::Type type);
    void error(int exitCode, const char* format, ...);
    Token consume(Token::Type type, std::string message);
    bool isAtEnd();
    Token peek();
    Token previous();
    Token advance();
};