// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "utilities.h"
#include <cstring>

char* readFile(const char* filepath, size_t* length)
{
    FILE* file = fopen(filepath, "r+");
    if (file == NULL) return nullptr;
    fseek(file, 0, SEEK_END);
    *length = ftell(file);
    fseek(file, 0, SEEK_SET);
    char* in = (char *) malloc(*length);
    if (in == nullptr) return nullptr;
    size_t bytes_read = fread(in, sizeof(char), *length, file);
    fclose(file);
    return in;
}

const char *sstrstr(const char *haystack, const char *needle, size_t length)
{
    size_t needle_length = strlen(needle);
    size_t i;
    for (i = 0; i < length; i++) {
        if (i + needle_length > length) {
            return NULL;
        }
        if (strncmp(&haystack[i], needle, needle_length) == 0) {
            return &haystack[i];
        }
    }
    return nullptr;
}