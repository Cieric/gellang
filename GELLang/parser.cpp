// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include "parser.h"
#include "CompilerUtils.h"
#include <stdarg.h>

Parser::Parser(Tokens tokens) :
    tokens(tokens),
    iter(this->tokens.begin()),
    symbols(new SymbolMap())
{}

Program* Parser::process()
{
    Program* program = new Program();

    while (!isAtEnd()) {
        program->stmts.push_back(declaration());
    }

    return program;
}

Stmt* Parser::declaration() {
    if (matchAnyKeyword(types) ||
        matchAnyKeyword(customTypes) ||
        matchKeyword("const") || 
        matchKeyword("__inlineasm")) return varDeclaration();

    if (matchKeyword("struct")) return structDeclaration();

    return statement();
}

std::string MangledFuncName(std::string name, std::vector<Parameter> params)
{
    std::string fnName = name + "(";
    for (auto param : params)
    {
        fnName += ShortName(param.type) + ";";
    }
    return fnName;
}

//std::unordered_map<std::string, StmtStruct*> structDefs;
Stmt* Parser::structDeclaration() {

    Token name = peek();
    if (!match({ Token::IDENT, Token::KEYWORD }))
    {
        error(5, "Failed to consume token, Expected struct name. but found \"%s\" (line#%d)\n", name.text.c_str(), name.line);
        std::exit(1);
    }
    consume(Token::LBRACE, "Expect '{' for body.");
    std::vector<Symbol*> symbols;
    while (matchAnyKeyword(types) ||
        matchAnyKeyword(customTypes) ||
        matchKeyword("const")) {
        auto subsymbols = structVarDeclaration();
        symbols.insert(symbols.end(), subsymbols.begin(), subsymbols.end());
    }
    consume(Token::RBRACE, "Expect '}' for body.");
    consume(Token::SEMICOLON, "Expect ';' for body.");

    std::vector<DataType*> types;
    for (auto symbol : symbols)
        types.push_back(symbol->type);

    customTypes.insert_or_assign(name.text, new DataTypeStruct(name.text, types));
    return new StmtStruct(name, symbols);
}

std::vector<Symbol*> Parser::structVarDeclaration()
{
    Token type = previous();

    bool _const = false;
    if (type.text == "const")
    {
        type = advance();
        _const = true;
    }

    Token name = peek();
    if (!match({ Token::IDENT, Token::KEYWORD }))
    {
        error(4, "Failed to consume token, Expected variable/function name. but found \"%s\" (line#%d)\n", name.text.c_str(), name.line);
        std::exit(1);
    }

    DataType* dataType = typeFromString(type.text);

    auto symbol = symbols->def(name.text);
    symbol->name = name.text;
    symbol->type = dataType;
    symbol->isConst = _const;
    symbol->isFunc = false;

    std::vector<Symbol*> varDecls;
    varDecls.push_back(symbol);
    while (match({ Token::COMMA })) {
        Token name = consume(Token::IDENT, "Expect variable/function name.");
        auto symbol = symbols->def(name.text);
        symbol->name = name.text;
        symbol->type = dataType;
        symbol->isConst = _const;
        symbol->isFunc = false;

        varDecls.push_back(symbol);
    }

    consume(Token::SEMICOLON, "Expect ';' after variable declaration.");
    while (match({ Token::SEMICOLON }));

    return varDecls;
}

Stmt* Parser::varDeclaration() {
    Token type = previous();

    bool inlineasm = false;
    if (type.text == "__inlineasm")
    {
        type = advance();
        inlineasm = true;
    }
    
    bool _const = false;
    if (type.text == "const")
    {
        type = advance();
        _const = true;
    }

    Token name = peek();
    if (!match({ Token::IDENT, Token::KEYWORD }))
    {
        error(4, "Failed to consume token, Expected variable/function name. but found \"%s\" (line#%d)\n", name.text.c_str(), name.line);
        std::exit(1);
    }

    if (match({ Token::LPAREN }))
    {
        StmtFunc* func;
        if(inlineasm)
            func = (StmtFunc*)inlineAsmDeclaration(type, name);
        else
            func = (StmtFunc*)funcDeclaration(type, name);

        std::string mangledName = MangledFuncName(name.text, func->parameters);

        auto symbol = symbols->def(mangledName);
        symbol->name = name.text;
        symbol->type = typeFromString(type.text);
        symbol->isConst = _const;
        symbol->isFunc = true;

        return func;
    }
    if (inlineasm)
    {
        error(5, "Only functions can be declared as inline assembly!");
    }

    DataType* dataType = typeFromString(type.text);

    auto symbol = symbols->def(name.text);
    symbol->name = name.text;
    symbol->type = dataType;
    symbol->isConst = _const;
    symbol->isFunc = false;

    Expr* initializer = nullptr;
    if (match({ Token::EQL }))
    {
        initializer = assignment();
    }

    std::vector<std::pair<Symbol*, Expr*>> varDecls;
    varDecls.push_back(std::make_pair(symbol, initializer));
    while (match({ Token::COMMA })) {
        Token name = consume(Token::IDENT, "Expect variable/function name.");
        auto symbol = symbols->def(name.text);
        symbol->name = name.text;
        symbol->type = dataType;
        symbol->isConst = _const;
        symbol->isFunc = false;

        Expr* initializer = nullptr;
        if (match({ Token::EQL }))
        {
            initializer = assignment();
        }
        varDecls.push_back(std::make_pair(symbol, initializer));
    }

    consume(Token::SEMICOLON, "Expect ';' after variable declaration.");
    while (match({ Token::SEMICOLON }));

    return new StmtDecl(typeFromString(type.text), varDecls);
}

Stmt* Parser::inlineAsmDeclaration(Token type, Token name)
{
    std::vector<Parameter> parameters;
    symbols = symbols->push();
    bool varargFunc = false;
    if (!check(Token::RPAREN)) {
        do {
            Parameter param;

            if (match({ Token::DOT }) && match({ Token::DOT }) && match({ Token::DOT }))
            {
                param.name = "...";
                param.type = new DataTypeVoid{};
                param.constant = true;
                varargFunc = true;
            }
            else
            {
                bool isConst = matchKeyword("const");

                if (matchAnyKeyword({"in", "out", "inout"}))
                {
                    Token copySpecifierToken = previous();
                    if (copySpecifierToken.text == "in")
                        param.copySpecifier = IN;
                    else if (copySpecifierToken.text == "out")
                        param.copySpecifier = OUT;
                    else if (copySpecifierToken.text == "inout")
                        param.copySpecifier = INOUT;
                }

                if (!matchAnyKeyword(types))
                {
                    fprintf(stderr, "Failed to consume token, expected parameter type but found %s\n", to_string(peek().type).c_str());
                    std::exit(4);
                }

                DataType* temp = typeFromString(previous().text);
                
                if (match({ Token::AT }))
                {
                    temp->castable = true;
                }

                temp->constant = isConst;

				if(param.copySpecifier != IN) {
					temp = new DataTypePointer(StorageClass::Output, temp);
				}

                param.constant = isConst;
                param.type = temp;
                param.name = consume(Token::IDENT, "Expect parameter name.").text;

                auto symbol = symbols->def(param.name);
                if (symbol == nullptr)
                    CompilerError("Symbol already defined in scope!");
                symbol->name = param.name;
                symbol->type = temp;
                symbol->isConst = param.constant;
            }

            parameters.push_back(param);
        } while (match({ Token::COMMA }));
    }
    consume(Token::RPAREN, "Expect ')' after parameters.");
    consume(Token::LBRACE, "Expect '{' for body.");
    StmtBlock* body = new StmtBlock(astBlock());
    symbols = symbols->pop();
    auto func = new StmtFunc(type, name, parameters, body);
    func->varargs = varargFunc;
    return func;
}

std::vector<Stmt*> Parser::astBlock() {
    std::vector<Stmt*> statements;
    symbols = symbols->push();
    while (!check({ Token::RBRACE }) && !isAtEnd()) {
        statements.push_back(opCode());
    }
    consume({ Token::RBRACE }, "Expect '}' after block.");
    symbols = symbols->pop();
    return statements;
}

Stmt* Parser::opCode()
{
    Token result;
    if (match({ Token::LESS }))
    {
        if (!match({ Token::IDENT, Token::KEYWORD, Token::INTEGER }))
            error(6, "Failed to get result name!");
        result = previous();
        result.text = "<" + result.text + ">";
        consume(Token::GREAT, "Expected '>'");
    }
    Token opName = consume(Token::IDENT, "Expected Op Name");
    std::vector<Token> parameters;
    while (!match({ Token::SEMICOLON }))
    {
        if (match({ Token::LESS }))
        {
            if (match({ Token::IDENT, Token::KEYWORD, Token::INTEGER }))
            {
                auto param = previous();
                param.text = "<" + param.text + ">";
                parameters.push_back(param);
                consume(Token::GREAT, "Expected '>'");
            }
            else if (match({ Token::DOT }) && match({ Token::DOT }) && match({ Token::DOT }))
            {
                auto param = previous();
                param.text = "<...>";
                parameters.push_back(param);
                consume(Token::GREAT, "Expected '>'");
            }
            else
                error(6, "Failed to get parameter name!");
        }
        else if (match({ Token::INTEGER, Token::FLOAT, Token::IDENT, Token::STRING }))
        {
            parameters.push_back(previous());
        }
        else
        {
            CompilerError("Invalid Token found!");
        }
    }
    return new StmtAsm(result, opName, parameters);
}

Stmt* Parser::funcDeclaration(Token type, Token name)
{
    std::vector<Parameter> parameters;
    symbols = symbols->push();
    if (!check(Token::RPAREN)) {
        do {
            Parameter param;
            if (matchAnyKeyword({ "const" }))
            {
                param.constant = true;
            }

            if (matchAnyKeyword({"in", "out", "inout"}))
            {
                Token copySpecifierToken = previous();
                if (copySpecifierToken.text == "in")
                    param.copySpecifier = IN;
                else if (copySpecifierToken.text == "out")
                    param.copySpecifier = OUT;
                else if (copySpecifierToken.text == "inout")
                    param.copySpecifier = INOUT;
            }

            if (!matchAnyKeyword(types))
            {
                fprintf(stderr, "Failed to consume token, expected parameter type but found %s\n", to_string(peek().type).c_str());
                std::exit(4);
            }
            
            DataType* temp = typeFromString(previous().text);
			
			if(param.copySpecifier != IN) {
				temp = new DataTypePointer(StorageClass::Output, temp);
			}
            
			param.type = temp;
            param.name = consume(Token::IDENT, "Expect parameter name.").text;

            auto symbol = symbols->def(param.name);
            if (symbol == nullptr)
                CompilerError("Symbol already defined in scope!");
            symbol->name = param.name;
            symbol->type = temp;
            symbol->isConst = param.constant;
            
            parameters.push_back(param);
        } while (match({ Token::COMMA }));
    }
    consume(Token::RPAREN, "Expect ')' after parameters.");
    consume(Token::LBRACE, "Expect '{' for body.");
    StmtBlock* body = new StmtBlock(block());
    symbols = symbols->pop();
    return new StmtFunc(type, name, parameters, body);
}

std::vector<Stmt*> Parser::block() {
    std::vector<Stmt*> statements;
    symbols = symbols->push();
    while (!check({ Token::RBRACE }) && !isAtEnd()) {
        statements.push_back(declaration());
    }
    consume({ Token::RBRACE }, "Expect '}' after block.");
    symbols = symbols->pop();
    return statements;
}

Stmt* Parser::statement()
{
    if (matchKeyword("for")) return forStatement();
    if (matchKeyword("if")) return ifStatement();
    if (matchKeyword("return")) return returnStatement();
    if (matchKeyword("while")) return whileStatement();
    if (matchKeyword("do")) return doWhileStatement();
    if (matchKeyword("break")) return breakStatement();
    if (matchKeyword("print")) return printStatement();
    if (match({ Token::LBRACE })) return new StmtBlock(block());

    return expressionStatement();
}

Stmt* Parser::forStatement() {
    consume(Token::LPAREN, "Expect '(' after 'for'.");
    Stmt* initializer;
    if (match({ Token::SEMICOLON })) {
        initializer = nullptr;
    }
    else if (matchAnyKeyword(types) || matchKeyword("const")) {
        initializer = varDeclaration();
    }
    else {
        initializer = expressionStatement();
    }

    Expr* condition = nullptr;
    if (!check(Token::SEMICOLON)) {
        condition = expression();
    }
    consume(Token::SEMICOLON, "Expect ';' after loop condition.");

    Expr* increment = nullptr;
    if (!check(Token::RPAREN)) {
        increment = expression();
    }
    consume(Token::RPAREN, "Expect ')' after for clauses.");
    Stmt* body = statement();

    if (increment != nullptr) {
        body = new StmtBlock( { body, new StmtExpr(increment) } );
    }

    if (condition == nullptr)
        condition = new ExprLiteral(true);
    body = new StmtWhile(condition, body);

    if (initializer != nullptr) {
        body = new StmtBlock( { initializer, body } );
    }

    return body;
}

Stmt* Parser::ifStatement() {
    consume(Token::LPAREN, "Expect '(' after 'if'.");
    Expr* condition = expression();
    consume(Token::RPAREN, "Expect ')' after if condition.");

    Stmt* thenBranch = statement();
    Stmt* elseBranch = nullptr;
    if (matchKeyword("else")) {
        elseBranch = statement();
    }

    return new StmtIf(condition, thenBranch, elseBranch);
}

Stmt* Parser::returnStatement() {
    Token keyword = previous();
    Expr* value = nullptr;
    if (!check({ Token::SEMICOLON })) {
        value = expression();
    }

    consume(Token::SEMICOLON, "Expect ';' after return value.");
    while (match({ Token::SEMICOLON }));
    return new StmtReturn(keyword, value);
}

Stmt* Parser::whileStatement() {
    consume(Token::LPAREN, "Expect '(' after 'while'.");
    Expr* condition = expression();
    consume(Token::RPAREN, "Expect ')' after condition.");
    Stmt* body = statement();

    return new StmtWhile(condition, body);
}

Stmt* Parser::doWhileStatement() {
    Stmt* body = statement();
    if(!matchKeyword("while")) 
        error(4, "Failed to consume token, %s but found \"%s\" (line#%d)\n", "Expect 'while' after do body.", peek().text.c_str(), peek().line);
    consume(Token::LPAREN, "Expect '(' after 'while'.");
    Expr* condition = expression();
    consume(Token::RPAREN, "Expect ')' after condition.");
    consume(Token::SEMICOLON, "Expect ';' after condition.");

    return new StmtDoWhile(body, condition);
}

Stmt* Parser::breakStatement() {
    Token keyword = previous();
    Expr* value = nullptr;
    if (!check({ Token::SEMICOLON })) {
        value = expression();
    }

    consume(Token::SEMICOLON, "Expect ';' after return value.");
    while (match({ Token::SEMICOLON }));
    
    return new StmtBreak(keyword, value);
}

Stmt* Parser::printStatement()
{
    Expr* value = expression();
    consume(Token::Type::SEMICOLON, "Expect ';' after value.");
    while (match({ Token::SEMICOLON }));
    return new StmtPrint(value);
}

Stmt* Parser::expressionStatement()
{
    Expr* expr = expression();
    consume(Token::Type::SEMICOLON, "Expect ';' after value.");
    while (match({ Token::SEMICOLON }));
    return new StmtExpr(expr);
}

Expr* Parser::expression() {
    return sequence();
}

Expr* Parser::sequence() {
    Expr* expr = assignment();

    if (check({ Token::COMMA }))
    {
        std::vector<Expr*> sequenceList;
        while (match({ Token::COMMA }))
        {
            sequenceList.push_back(assignment());
        }
        expr = new ExprSequence(sequenceList);
    }

    return expr;
}

Expr* Parser::assignment() {
    Expr* expr = selection();

    if (match({ Token::EQL })) {
        Token equals = previous();
        Expr* value = assignment();
        return new ExprAssign(expr, value);
    }

    if (match({ Token::MINUS_EQL })) {
        Token equals = previous();
        Expr* value = assignment();
        equals.type = Token::MINUS;
        return new ExprAssign(expr, new ExprBinary(expr, equals, value));
    }
    
    if (match({ Token::PLUS_EQL })) {
        Token equals = previous();
        Expr* value = assignment();
        equals.type = Token::PLUS;
        return new ExprAssign(expr, new ExprBinary(expr, equals, value));
    }
    
    if (match({ Token::STAR_EQL })) {
        Token equals = previous();
        Expr* value = assignment();
        equals.type = Token::STAR;
        return new ExprAssign(expr, new ExprBinary(expr, equals, value));
    }
    
    if (match({ Token::SLASH_EQL })) {
        Token equals = previous();
        Expr* value = assignment();
        equals.type = Token::SLASH;
        return new ExprAssign(expr, new ExprBinary(expr, equals, value));
    }

    return expr;
}

Expr* Parser::selection() {
    Expr* expr = logicalOr();

    while (match({ Token::QUERY })) {
        Token op = previous();
        Expr* left = expression();
        consume(Token::COLON, "expected a ':' after expresion in selection expr1");
        Expr* right = expression();

        expr = new ExprSelection(expr, left, right);
    }

    return expr;
}

Expr* Parser::logicalOr() {
    Expr* expr = logicalAnd();

    while (match({ Token::PIPE_PIPE })) {

        Token op = previous();
        Expr* right = logicalAnd();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::logicalAnd() {
    Expr* expr = bitwiseXOR();

    while (match({ Token::AMP_AMP })) {
        Token op = previous();
        Expr* right = equality();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::bitwiseXOR() {
    Expr* expr = bitwiseAND();

    while (match({ Token::CARROT })) {
        Token op = previous();
        Expr* right = bitwiseAND();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::bitwiseAND() {
    Expr* expr = equality();

    while (match({ Token::AMP })) {
        Token op = previous();
        Expr* right = equality();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::equality() {
    Expr* expr = comparison();

    while (match({Token::BANG_EQL, Token::EQL_EQL})) {
        Token op = previous();
        Expr* right = comparison();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::comparison() {
    Expr* expr = bitwiseShift();

    while (match({Token::GREAT, Token::GREAT_EQL, Token::LESS, Token::LESS_EQL})) {
        Token op = previous();
        Expr* right = term();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::bitwiseShift() {
    Expr* expr = term();

    while (match({ Token::LESS_LESS, Token::GREAT_GREAT })) {
        Token op = previous();
        Expr* right = term();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::term() {
    Expr* expr = factor();

    while (match({Token::MINUS, Token::PLUS})) {
        Token op = previous();
        Expr* right = factor();
        //if (op.type == Token::PLUS && expr->type == Expr::BINARY && ((ExprBinary*)expr)->op.type == Token::STAR)
        //{
        //    //error(0, "This could of been a FMA! Fix Immediately");
        //    ExprBinary* oldExpr = (ExprBinary*)expr;
        //    expr = new ExprFMA(oldExpr->left, oldExpr->right, right);
        //}
        //else
        //{
        expr = new ExprBinary(expr, op, right);
        //}
    }

    return expr;
}

Expr* Parser::factor() {
    Expr* expr = unary();

    while (match({Token::SLASH, Token::STAR, Token::MOD})) {
        Token op = previous();
        Expr* right = unary();
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::unary() {

    if (match({ Token::BANG, Token::MINUS, Token::PLUS, Token::PLUS_PLUS, Token::MINUS_MINUS })) {
        Token op = previous();
        Expr* right = unary();
        return new ExprUnary(op, right);
    }

    return postscripts();
}

//StmtStruct* Parser::getStructType(Expr* expr)
//{
//    switch (expr->type)
//    {
//    case Expr::VARIABLE: {
//        auto symbol = symbols->get(((ExprVariable*)expr)->name.text);
//        return structDefs[((DataTypeStruct*)symbol->type)->name];
//    }
//    }
//    return nullptr;
//}

Expr* Parser::postscripts()
{
    Expr* variable = primary();
    Token op = peek();

    if (variable->type == Expr::VARIABLE) {
        std::vector<Expr*> arguments;

        if (op.type == Token::LPAREN)
        {
            advance();
            if (!check({ Token::RPAREN })) {
                do {
                    arguments.push_back(assignment());
                } while (match({ Token::COMMA }));
            }
            consume(Token::RPAREN, "Expect ')' after arguments.");
            variable = new ExprCall(((ExprVariable*)variable)->name, arguments);
        }
    }

    while(match({ Token::DOT, Token::PLUS_PLUS, Token::MINUS_MINUS, Token::LBRACKET }))
    {
        op = previous();
        
        if (op.type == Token::DOT)
        {
            //TODO: check if variable is a struct type

            bool isVecType = true;
            if (variable->type == Expr::VARIABLE)
                isVecType = isVector(((ExprVariable*)variable)->symbolTableId->type);

            if (isVecType)
            {
                Token swizzle = consume(Token::IDENT, "Expected swizzle after '.'");
                bool xyzw = !(swizzle.text.find_first_not_of("wxyz") != std::string::npos);
                bool rgba = !(swizzle.text.find_first_not_of("rgba") != std::string::npos);
                bool stpq = !(swizzle.text.find_first_not_of("stpq") != std::string::npos);
                if (!(xyzw || rgba || stpq))
                {
                    CompilerError("Swizzles can only contain characters from one of the following groups (xyzw, stuv, rgba) and not a mixture.");
                }
                variable = new ExprSwizzle(variable, swizzle);
            }
            else
            {
                Token member = consume(Token::IDENT, "Expected member name after '.'");
                variable = new ExprMember(variable, member);
            }
        }
        else if (op.type == Token::LBRACKET)
        {
            Expr* indexer = expression();
            consume(Token::RBRACKET, "Expected ']' after indexer.");
            variable = new ExprArray(variable, indexer);
        }
        else if (op.type == Token::PLUS_PLUS || op.type == Token::MINUS_MINUS)
        {
            variable = new ExprPostscript(variable, op);
        }
        else
        {
            CompilerError("Unimplemented operator!");
        }
    }
    return variable;
}

Expr* Parser::primary() {
    if (matchKeyword("false")) return new ExprLiteral(previous());
    if (matchKeyword("true")) return new ExprLiteral(previous());
    if (match({ Token::STRING })) return new ExprLiteral(previous());
    //if (matchKeyword(Token::NIL)) return new ExprLiteral(null);

    if (match({ Token::FLOAT, Token::INTEGER })) {
        return new ExprLiteral(previous());
    }

    if (match({ Token::LPAREN })) {
        Expr* expr = expression();
        consume(Token::RPAREN, "Expect ')' after expression.");
        return new ExprGrouping(expr);
    }

    if (matchAnyKeyword(types))
    {
        return new ExprVariable(previous(), nullptr);
    }

    if (match({ Token::IDENT })) {
        auto token = previous();
        auto symbol = symbols->get(token.text);
        return new ExprVariable(previous(), symbol);
    }

    error(3, "Failed to consume token, expected [primary] but found %s\n", to_string(peek().type).c_str());
    return nullptr;
}

DataType* Parser::typeFromString(std::string str)
{
    auto found = customTypes.find(str);
    if(found == customTypes.end())
        return ::typeFromString(str);
    return found->second;
}

bool Parser::match(std::vector<Token::Type> types)
{
    for (Token::Type type : types) {
        if (check(type)) {
            advance();
            return true;
        }
    }
    return false;
}

bool Parser::matchAnyKeyword(std::unordered_set<std::string> names)
{
    if (names.find(peek().text) != names.end())
    {
        advance();
        return true;
    }
    return false;
}

bool Parser::matchKeyword(std::string name)
{
    if (peek().type != Token::KEYWORD) return false;
    if (peek().text == name)
    {
        advance();
        return true;
    }
    return false;
}

bool Parser::check(Token::Type type)
{
    if (isAtEnd()) return false;
    return peek().type == type;
}


void Parser::error(int exitCode, const char* format, ...)
{
    va_list argptr;
    va_start(argptr, format);
    vfprintf(stderr, format, argptr);
    va_end(argptr);
    std::exit(exitCode);
}

Token Parser::consume(Token::Type type, std::string message) {
    if (check(type)) return advance();
    //setError(message);
    Token tok = peek();
    error(4, "Failed to consume token, %s but found \"%s\" (line#%d)\n", message.c_str(), tok.text.c_str(), tok.line);
    return {};
}

bool Parser::isAtEnd() {
    return peek().type == Token::EOT;
}

Token Parser::peek() {
    return *iter;
}

Token Parser::previous() {
    return *std::prev(iter);
}

Token Parser::advance()  {
    if (!isAtEnd()) iter++, counter++;
    return previous();
}

Symbol* SymbolMap::get(std::string symbolName)
{
    auto found = symbols.find(symbolName);
    if (found != symbols.end())
        return found->second;
    if(parent != nullptr)
        return parent->get(symbolName);
    return nullptr;
}

Symbol* SymbolMap::def(std::string symbolName)
{
    auto found = symbols.find(symbolName);
    if (found != symbols.end())
        return nullptr;
    auto symbol = new Symbol();
    symbols.insert_or_assign(symbolName, symbol);
    return symbol;
}

SymbolMap* SymbolMap::push()
{
    auto symbolMap = new SymbolMap();
    symbolMap->parent = this;
    return symbolMap;
}

SymbolMap* SymbolMap::pop()
{
    return this->parent;
}
