// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#ifndef SPIRV_INTERPRETER_H
#define SPIRV_INTERPRETER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

struct VM {};

extern VM* c_init(uint32_t* words, uint32_t word_count);


#ifdef __cplusplus
}
#endif
#endif