// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

const std = @import("std");
const testing = std.testing;

const spvm_word = u32;

const MemoryCellType = enum {
	UNINITED,
	TYPE,
	FUNCTION,
	VARIABLE,
	POINTER,
};

const TypeType = enum {
	VOID,
	FLOAT,
	INT,
	UINT,
	POINTER,
	VECTOR,
	MATRIX,
	FUNCTION,
	STRUCT,
};

const TypeMemoryCell = struct {
	type: TypeType,
	byteWidth: spvm_word,
	subtypeCount: spvm_word,
	subtype: ?*TypeMemoryCell,
	subtypes: ?[]*TypeMemoryCell,
};

const FunctionMemoryCell = struct {
	functionStart: spvm_word,
	functionLen: spvm_word,
};

const DataMemoryCell = struct {
	type: *TypeMemoryCell,
	memory: []u8,

	pub fn set(self: @This(), value: anytype) void {
		//const value_size = @sizeOf(@TypeOf(value));
		std.mem.copyForwards(u8, self.memory[0..], std.mem.asBytes(&value));
	}

	pub fn setRaw(self: @This(), value: []u8) void {
		//const value_size = @sizeOf(@TypeOf(value));
		std.mem.copyForwards(u8, self.memory[0..], value);
	}

	pub fn isPointer(self: @This()) bool {
		return self.type.type == .POINTER;
	}

	pub fn getPointer(self: @This()) []u8 {
		std.debug.assert(self.isPointer());
		return std.mem.bytesToValue([]u8, self.memory[0..16]);
	}

	pub fn setPointer(self: @This(), slice: []u8 ) void {
		std.debug.assert(self.isPointer());
		var bytes = std.mem.asBytes(&slice);
		std.mem.copyForwards(u8, self.memory[0..], bytes);
		//std.mem.bytesToValue([]u8, self.memory[0..16]);
	}
};

const MemoryCell = union(enum) {
	null: void, 
	type: TypeMemoryCell,
	function: FunctionMemoryCell,
	data: DataMemoryCell,
};

const VM = struct {
	allocator: std.mem.Allocator,
	code: []spvm_word,
	cIP: u32,
	nIP: u32,
	names: std.StringHashMap(spvm_word),
	cells: []MemoryCell,
	varsIdx: spvm_word,
	returnStack: [256]spvm_word,
	stackCount: spvm_word,
	returnValuePtr: spvm_word,
};

var global_allocator: std.mem.Allocator = undefined;

const OpCodeId = enum(u16) {
	OpSource = 3,

	OpName = 5,
	
	OpExtInstImport = 11,
	
	OpMemoryModel = 14,
	OpEntryPoint = 15,
	OpExecutionMode = 16,
	OpCapability = 17,
	
	OpTypeVoid = 19,
	
	OpTypeInt = 21,
	OpTypeFloat = 22,
	OpTypeVector = 23,
	
	OpTypeStruct = 30,

	OpTypePointer = 32,
	OpTypeFunction = 33,

	OpConstant = 43,
	
	OpFunction = 54,
	OpFunctionParameter = 55,
	OpFunctionEnd = 56,
	OpFunctionCall = 57,

	OpVariable = 59,

	OpLoad = 61,
	OpStore = 62,

	OpAccessChain = 65,

	OpCompositeConstruct = 80,

	OpReturn = 253,
	OpReturnValue = 254,
};

const VMError = error {
	InstructionsOverrun,
	OutOfMemory,
	TooManyArgs,
	ArgWrongSize,
	DereferencingNull,
};

const OpCodeFn = *const fn(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void;

const OpCodeFns = [_]OpCodeFn{
	&opSkip, //0
	&opSkip, //1
	&opSkip, //2
	&opSkip, //3
	&opSkip, //4
	&opSkip, //5
	&opSkip, //6
	&opSkip, //7
	&opSkip, //8
	&opSkip, //9
	&opSkip, //10
	&opSkip, //11
	&opSkip, //12
	&opSkip, //13
	&opSkip, //14
	&opSkip, //15
	&opSkip, //16
	&opSkip, //17
	&opSkip, //18
	&opSkip, //19
	&opSkip, //20
	&opSkip, //21
	&opSkip, //22
	&opSkip, //23
	&opSkip, //24
	&opSkip, //25
	&opSkip, //26
	&opSkip, //27
	&opSkip, //28
	&opSkip, //29
	&opSkip, //30
	&opSkip, //31
	&opSkip, //32
	&opSkip, //33
	&opSkip, //34
	&opSkip, //35
	&opSkip, //36
	&opSkip, //37
	&opSkip, //38
	&opSkip, //39
	&opSkip, //40
	&opSkip, //41
	&opSkip, //42
	&opSkip, //43
	&opSkip, //44
	&opSkip, //45
	&opSkip, //46
	&opSkip, //47
	&opSkip, //48
	&opSkip, //49
	&opSkip, //50
	&opSkip, //51
	&opSkip, //52
	&opSkip, //53
	&opSkip, //54
	&opSkip, //55
	&opSkip, //56
	&opFunctionCall, //57
	&opSkip, //58
	&opSkip, //59
	&opSkip, //60
	&opLoad, //61
	&opStore, //62
	&opSkip, //63
	&opSkip, //64
	&opAccessChain, //65
	&opSkip, //66
	&opSkip, //67
	&opSkip, //68
	&opSkip, //69
	&opSkip, //70
	&opSkip, //71
	&opSkip, //72
	&opSkip, //73
	&opSkip, //74
	&opSkip, //75
	&opSkip, //76
	&opSkip, //77
	&opSkip, //78
	&opSkip, //79
	&opCompositeConstruct, //80
	&opSkip, //81
	&opSkip, //82
	&opSkip, //83
	&opSkip, //84
	&opSkip, //85
	&opSkip, //86
	&opSkip, //87
	&opSkip, //88
	&opSkip, //89
	&opSkip, //90
	&opSkip, //91
	&opSkip, //92
	&opSkip, //93
	&opSkip, //94
	&opSkip, //95
	&opSkip, //96
	&opSkip, //97
	&opSkip, //98
	&opSkip, //99
	&opSkip, //100
	&opSkip, //101
	&opSkip, //102
	&opSkip, //103
	&opSkip, //104
	&opSkip, //105
	&opSkip, //106
	&opSkip, //107
	&opSkip, //108
	&opSkip, //109
	&opSkip, //110
	&opSkip, //111
	&opSkip, //112
	&opSkip, //113
	&opSkip, //114
	&opSkip, //115
	&opSkip, //116
	&opSkip, //117
	&opSkip, //118
	&opSkip, //119
	&opSkip, //120
	&opSkip, //121
	&opSkip, //122
	&opSkip, //123
	&opSkip, //124
	&opSkip, //125
	&opSkip, //126
	&opSkip, //127
	&opSkip, //128
	&opSkip, //129
	&opSkip, //130
	&opSkip, //131
	&opSkip, //132
	&opSkip, //133
	&opSkip, //134
	&opSkip, //135
	&opSkip, //136
	&opSkip, //137
	&opSkip, //138
	&opSkip, //139
	&opSkip, //140
	&opSkip, //141
	&opSkip, //142
	&opSkip, //143
	&opSkip, //144
	&opSkip, //145
	&opSkip, //146
	&opSkip, //147
	&opSkip, //148
	&opSkip, //149
	&opSkip, //150
	&opSkip, //151
	&opSkip, //152
	&opSkip, //153
	&opSkip, //154
	&opSkip, //155
	&opSkip, //156
	&opSkip, //157
	&opSkip, //158
	&opSkip, //159
	&opSkip, //160
	&opSkip, //161
	&opSkip, //162
	&opSkip, //163
	&opSkip, //164
	&opSkip, //165
	&opSkip, //166
	&opSkip, //167
	&opSkip, //168
	&opSkip, //169
	&opSkip, //170
	&opSkip, //171
	&opSkip, //172
	&opSkip, //173
	&opSkip, //174
	&opSkip, //175
	&opSkip, //176
	&opSkip, //177
	&opSkip, //178
	&opSkip, //179
	&opSkip, //180
	&opSkip, //181
	&opSkip, //182
	&opSkip, //183
	&opSkip, //184
	&opSkip, //185
	&opSkip, //186
	&opSkip, //187
	&opSkip, //188
	&opSkip, //189
	&opSkip, //190
	&opSkip, //191
	&opSkip, //192
	&opSkip, //193
	&opSkip, //194
	&opSkip, //195
	&opSkip, //196
	&opSkip, //197
	&opSkip, //198
	&opSkip, //199
	&opSkip, //200
	&opSkip, //201
	&opSkip, //202
	&opSkip, //203
	&opSkip, //204
	&opSkip, //205
	&opSkip, //206
	&opSkip, //207
	&opSkip, //208
	&opSkip, //209
	&opSkip, //210
	&opSkip, //211
	&opSkip, //212
	&opSkip, //213
	&opSkip, //214
	&opSkip, //215
	&opSkip, //216
	&opSkip, //217
	&opSkip, //218
	&opSkip, //219
	&opSkip, //220
	&opSkip, //221
	&opSkip, //222
	&opSkip, //223
	&opSkip, //224
	&opSkip, //225
	&opSkip, //226
	&opSkip, //227
	&opSkip, //228
	&opSkip, //229
	&opSkip, //230
	&opSkip, //231
	&opSkip, //232
	&opSkip, //233
	&opSkip, //234
	&opSkip, //235
	&opSkip, //236
	&opSkip, //237
	&opSkip, //238
	&opSkip, //239
	&opSkip, //240
	&opSkip, //241
	&opSkip, //242
	&opSkip, //243
	&opSkip, //244
	&opSkip, //245
	&opSkip, //246
	&opSkip, //247
	&opSkip, //248
	&opSkip, //249
	&opSkip, //250
	&opSkip, //251
	&opSkip, //252
	&opReturn, //253
	&opReturnValue, //254
	&opSkip, //255
};

pub fn dispatch(vm: *VM, _: OpCodeId, _: []spvm_word) VMError!void {
	if(vm.nIP >= vm.code.len) return VMError.InstructionsOverrun;
	const currentFn = vm.code[vm.nIP];
	const opcode: OpCodeId = @enumFromInt(@as(u16, @intCast(currentFn & 0x0000FFFF)));
	const len: u16 = @intCast((currentFn & 0xFFFF0000) >> 16);
	const args: []spvm_word = vm.code[vm.nIP+1..vm.nIP+len];
	var nextFn = OpCodeFns[@intFromEnum(opcode)];
	vm.cIP = vm.nIP;
	vm.nIP += len;
	return @call(.always_tail, nextFn, .{vm, opcode, args});
}

pub fn opSkip(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	std.debug.print("Skipping instruction: {} {any}\n", .{op, arg_bytes});
	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn opFunctionCall(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	_ = op;
	var ResultType = arg_bytes[0];
	_ = ResultType;
	var ResultId = arg_bytes[1];
	_ = ResultId;
	var FunctionId = arg_bytes[2];

	var paramIdx = vm.cells[FunctionId].function.functionStart;
	for(arg_bytes[3..]) |param| {
		var opFuncParam = vm.code[paramIdx + 2];
		var dest: []u8 = vm.cells[opFuncParam].data.memory;
		std.mem.copyForwards(u8, dest, vm.cells[param].data.memory);
		
		paramIdx += getOpLen(vm.code[paramIdx]);
	}

	vm.returnStack[vm.stackCount] = vm.cIP;
	vm.stackCount += 1;
	vm.nIP = vm.cells[FunctionId].function.functionStart;

	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn opLoad(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	_ = op;

	var ResultId = arg_bytes[1];
	var Pointer = arg_bytes[2];
	var srcMemory = vm.cells[Pointer].data.getPointer();

	std.mem.copyForwards(u8, vm.cells[ResultId].data.memory, srcMemory);

	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn opStore(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	_ = op;
	var Pointer = arg_bytes[0];
	var Object = arg_bytes[1];
	var destMemory = vm.cells[Pointer].data.getPointer();

	std.mem.copyForwards(u8, destMemory, vm.cells[Object].data.memory);
	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn opAccessChain(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	_ = op;

	var ResultType = vm.cells[arg_bytes[0]].type;
	var ResultId = arg_bytes[1];
	
	var Base = vm.cells[arg_bytes[2]].data;
	var Indexes = arg_bytes[3..];
	
	var Result = Base.getPointer();

	var BaseType = Base.type.subtype.?;
	var ResultSize: spvm_word = ResultType.subtype.?.byteWidth;

	//HACK: Assume one index
	var nextOffset: usize = 0;
	var offset: usize = 0;
	for(Indexes) |i| {
		var containerIdx = std.mem.bytesAsValue(u32, vm.cells[i].data.memory[0..4]).*;
		for(0..(containerIdx+1)) |j| {
			offset = nextOffset;
			if(BaseType.type == .STRUCT) {
				nextOffset += BaseType.subtypes.?[j].byteWidth;
			} else {
				nextOffset += BaseType.subtype.?.byteWidth;
			}
		}
	}

	var ResultSlice = Result[offset..][0..ResultSize];
	
	vm.cells[ResultId].data.setPointer(ResultSlice);

	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn opCompositeConstruct(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	_ = op;
	var ResultType = arg_bytes[0];
	var ResultId = arg_bytes[1];
	var Constituents = arg_bytes[2..];
	if(Constituents.len != vm.cells[ResultType].type.subtypeCount) return VMError.TooManyArgs;
	var offset: usize = 0;
	var memory = vm.cells[ResultId].data.memory;
	std.debug.print("memory size: {}\n", .{memory.len});
	for(Constituents) |Constituent| {
		var argMemory = vm.cells[Constituent].data.memory;
		var argMemorySize = vm.cells[Constituent].data.type.byteWidth;
		std.mem.copyForwards(u8, memory[offset..], argMemory);
		offset += argMemorySize;
	}
	
	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn opReturn(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	_ = op;
	_ = arg_bytes;

	if(vm.stackCount == 0) { return; }
	vm.stackCount -= 1;
	var returnPtr = vm.returnStack[vm.stackCount];
	var returnOp = vm.code[returnPtr];
	vm.cIP = vm.nIP;
	const len: u16 = @intCast((returnOp & 0xFFFF0000) >> 16);
	vm.nIP = returnPtr + len;

	
	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn opReturnValue(vm: *VM, op: OpCodeId, arg_bytes: []spvm_word) VMError!void {
	_ = op;
	
	if(vm.stackCount == 0) {
		//Store return value in designated spot
		vm.returnValuePtr = arg_bytes[0];
		return;
	}
	vm.stackCount -= 1;
	var returnPtr = vm.returnStack[vm.stackCount];
	var returnOp = vm.code[returnPtr];
	vm.cIP = vm.nIP;
	const len: u16 = @intCast((returnOp & 0xFFFF0000) >> 16);
	vm.nIP = returnPtr + len;
	//TODO: Fill return value (opFunctionCall is storage for return)
	var functionReturnCellPtr = vm.code[returnPtr + 2];
	var functionReturnCell = vm.cells[functionReturnCellPtr];
	var returnValueCell = vm.cells[arg_bytes[0]].data;

	functionReturnCell.data.setRaw(returnValueCell.memory);


	return @call(.always_tail, dispatch, .{vm, undefined, undefined});
}

pub fn getOpCode(currentFn: spvm_word) OpCodeId
{
	return @enumFromInt(@as(u16, @intCast(currentFn & 0x0000FFFF)));
}

pub fn getOpLen(currentFn: spvm_word) u16
{
	return @intCast((currentFn & 0xFFFF0000) >> 16);
}

pub fn setFunctionParameter(vm: *VM, parameterIdx: spvm_word, arg: anytype) !void {
	const len: u16 = getOpLen(parameterIdx);
	const args: []spvm_word = vm.code[parameterIdx+1..parameterIdx+len];
	//const resultType = args[0];
	const result = args[1];
	vm.cells[result].VARIABLE.set(arg);
}

pub fn runFn(vm: *VM, fnIdx: spvm_word, args: anytype) !void {
	
	vm.stackCount = 0;
	//Set both next and current instruction pointers to the same value.
	vm.nIP = vm.cells[fnIdx].function.functionStart;
	vm.cIP = vm.nIP;

    const ArgsType = @TypeOf(args);
    const args_type_info = @typeInfo(ArgsType);
	if (args_type_info != .Struct) {
        @compileError("expected tuple or struct argument, found " ++ @typeName(ArgsType));
    }
    
	const fields_info = args_type_info.Struct.fields;

	//inline for(std.meta.fieldNames(@TypeOf(args))) |fName| {
	//	@compileLog(fName);
	//}

	//@compileLog(args);
	//copy args to fields
	comptime var i=0;
	inline while(i < fields_info.len) {
		//@compileLog(fields_info[i].name);
		var arg = @field(args, fields_info[i].name);
		var argSize: usize = @sizeOf(@TypeOf(arg));
		
		const currentFn = vm.code[vm.nIP];
		const opcode: OpCodeId = getOpCode(currentFn);
		if(opcode != OpCodeId.OpFunctionParameter) return VMError.TooManyArgs;
		const len: u16 = getOpLen(currentFn);
		const fnArgs: []spvm_word = vm.code[vm.nIP+1..vm.nIP+len];
		var result = fnArgs[1];
		var resultType = vm.cells[fnArgs[0]].type;

		if(argSize != resultType.byteWidth) {
			std.debug.print("Error: size is {}, needed {}\n", .{argSize, resultType.byteWidth});
			return VMError.ArgWrongSize;
		}
		if(resultType.type == .POINTER)
		{
			vm.cells[result].data.setPointer(std.mem.sliceAsBytes(arg));
		}
		else 
		{
			vm.cells[result].data.set(arg);
		}
	
		vm.cIP = vm.nIP;
		vm.nIP += len;
		i += 1;
	}

	try dispatch(vm, undefined, undefined);
}

pub fn init(allocator: std.mem.Allocator, words: [*c]spvm_word, word_count: spvm_word) !*VM {
	var vm = try allocator.create(VM);
	//First 5 bytes are header bytes we don't care about
	vm.code = try allocator.alloc(spvm_word, word_count - 5);
	std.mem.copyForwards(spvm_word, vm.code, words[5..word_count]);
	vm.nIP = 0;
	vm.cIP = 0;
	//vm.functions = std.AutoHashMap(spvm_word, []spvm_word).init(allocator);
	vm.cells = try allocator.alloc(MemoryCell, 64);
	for(0..vm.cells.len) |i| {
		vm.cells[i] = .{ .null = {}, };
	}
	vm.allocator = allocator;
	vm.returnValuePtr = 0;
	vm.names = std.StringHashMap(spvm_word).init(allocator);

	//init variables
	var functionStart: spvm_word = undefined;
	var functionID: spvm_word = undefined;

	var codePtr: usize = 0;
	while(codePtr < vm.code.len) {
		var currentFn = vm.code[codePtr];
		const opcode: OpCodeId = getOpCode(currentFn);
		const len: u16 = getOpLen(currentFn);
		const args: []spvm_word = vm.code[codePtr+1..codePtr+len];
		codePtr += len;
		std.debug.print("opcode: {}", .{opcode});
		for(args) |arg| {
			std.debug.print(" {}", .{arg});
		}
		std.debug.print("\n", .{});
		switch(opcode) {
			OpCodeId.OpName => {
				var ResultID = args[0];
				const name_0: [*:0]const u8 = @ptrCast(@alignCast(std.mem.sliceAsBytes(args[1..])));
				const name = std.mem.span(name_0);
				std.debug.print("[{}] Name: \"{s}\"\n", .{ResultID, name});
				try vm.names.put(name, ResultID);
			},
			OpCodeId.OpTypeVoid => {
				var ResultID = args[0];
				std.debug.print("[{}] OpTypeVoid = VOID()\n", .{ResultID});
				vm.cells[ResultID] = .{.type=.{
					.type = .VOID,
					.byteWidth = 0,
					.subtype = null,
					.subtypes = null,
					.subtypeCount = 0,
				}};
			},
			OpCodeId.OpTypeInt => {
				var ResultID = args[0];
				var LiteralWidth = args[1];
				std.debug.print("[{}] OpTypeInt = Int({})\n", .{ResultID, LiteralWidth});
				vm.cells[ResultID] = .{.type=.{
					.type = .INT,
					.byteWidth = LiteralWidth / 8,
					.subtype = null,
					.subtypes = null,
					.subtypeCount = 1,
				}};
			},
			OpCodeId.OpTypeFloat => {
				var ResultID = args[0];
				var LiteralWidth = args[1];
				std.debug.print("[{}] OpTypeFloat = FLOAT({})\n", .{ResultID, LiteralWidth});
				vm.cells[ResultID] = .{.type=.{
					.type = .FLOAT,
					.byteWidth = LiteralWidth / 8,
					.subtype = null,
					.subtypes = null,
					.subtypeCount = 1,
				}};
			},
			OpCodeId.OpTypeVector => {
				var ResultID = args[0];
				var ComponentType = &(vm.cells[args[1]].type);
				var ComponentCount = args[2];
				std.debug.print("[{}] OpTypeVector = Vector({}, {})\n", .{ResultID, ComponentType.type, ComponentCount});				
				vm.cells[ResultID] = .{.type=.{
					.type = .VECTOR,
					.byteWidth = ComponentType.byteWidth * ComponentCount,
					.subtype = ComponentType,
					.subtypes = null,
					.subtypeCount = ComponentCount,
				}};
			},
			OpCodeId.OpTypeStruct => {
				var ResultID = args[0];
				//var Type = vm.cells[args[2]].type;
				var members = args[1..];
				//var memberCount = members.len;
				var byteWidth: spvm_word = 0;
				var subtypes = try vm.allocator.alloc(*TypeMemoryCell, members.len);
				for(members, 0..) |member, idx| {
					byteWidth += vm.cells[member].type.byteWidth;
					subtypes[idx] = &(vm.cells[member].type);
				}
				
				std.debug.print("[{}] OpTypeStruct = struct{{...}};\n", .{ResultID});
				vm.cells[ResultID] = .{.type=.{
					.type = .STRUCT,
					.byteWidth = byteWidth,
					.subtype = null,
					.subtypes = subtypes,
					.subtypeCount = @intCast(members.len),
				}};
			},
			OpCodeId.OpTypePointer => {
				var ResultID = args[0];
				var Type = &(vm.cells[args[2]].type);
				std.debug.print("[{}] OpTypePointer = Pointer()\n", .{ResultID});
				vm.cells[ResultID] = .{.type=.{
					.type = .POINTER,
					.byteWidth = @sizeOf([]u8),
					.subtype = Type,
					.subtypes = null,
					.subtypeCount = 1,
				}};
			},
			OpCodeId.OpConstant => {
				var resultType = &(vm.cells[args[0]].type);
				var result = args[1];
				std.debug.print("[{}] OpConstant = Literal()\n", .{result});
				vm.cells[result] = .{.data=.{
					.type = resultType,
					.memory = try vm.allocator.alloc(u8, resultType.byteWidth),
				}};
				vm.cells[result].data.setRaw(std.mem.sliceAsBytes(args[2..]));
			},
			OpCodeId.OpFunction => {
				functionStart = @intCast(codePtr);
				functionID = args[1];
				std.debug.print("[{X}] function = {}\n", .{functionID, functionStart});
			},
			OpCodeId.OpFunctionParameter => {
				var resultType = args[0];
				var result = args[1];
				std.debug.print("[{}] allocing Function Parameter memory {}\n", .{result, resultType});
				var resultSize = vm.cells[resultType].type.byteWidth;
				//TODO: support pointer type
				vm.cells[result] = .{.data=.{
					.type = &(vm.cells[resultType].type),
					.memory = try vm.allocator.alloc(u8, resultSize),
				}};
			},
			OpCodeId.OpFunctionEnd => {
				std.debug.print("function end = {}\n", .{codePtr - len});
				vm.cells[functionID] = .{.function=.{
					.functionStart = functionStart,
					.functionLen = @intCast((codePtr - args.len - 1) - functionStart),
				}};
			},
			OpCodeId.OpFunctionCall => {
				var resultType = args[0];
				var result = args[1];
				std.debug.print("[{}] allocing Function call memory {}\n", .{result, resultType});
				var resultSize = vm.cells[resultType].type.byteWidth;
				//TODO: support pointer type
				vm.cells[result] = .{.data=.{
					.type = &(vm.cells[resultType].type),
					.memory = try vm.allocator.alloc(u8, resultSize),
				}};
			},
			OpCodeId.OpVariable => {
				var resultType = &(vm.cells[args[0]].type);
				var result = args[1];
				//std.debug.print("[{}] allocing Variable memory {any}\n", .{result, resultType});
				var resultTypeSubtype = resultType.subtype;
				
				var cell = try vm.allocator.create(DataMemoryCell);
				cell.type = resultTypeSubtype.?;
				cell.memory = try vm.allocator.alloc(u8, resultTypeSubtype.?.byteWidth);
				
				var resultSize = resultType.byteWidth;
				vm.cells[result] = .{.data=.{
					.type = resultType,
					.memory = try vm.allocator.alloc(u8, resultSize),
				}};
				vm.cells[result].data.set(cell);
			},
			OpCodeId.OpLoad => {
				var resultType = args[0];
				var result = args[1];
				std.debug.print("[{}] allocing Variable memory {}\n", .{result, resultType});
				var resultSize = vm.cells[resultType].type.byteWidth;
				vm.cells[result] = .{.data=.{
					.type = &(vm.cells[resultType].type),
					.memory = try vm.allocator.alloc(u8, resultSize),
				}};
			},
			OpCodeId.OpAccessChain => {
				var resultType = args[0];
				var result = args[1];
				//var base = args[2];
				//var Indexes = arg_bytes[3..];
				std.debug.print("[{}] allocing Access Chain memory {}\n", .{result, resultType});
				var resultSize: spvm_word = vm.cells[resultType].type.byteWidth;
				vm.cells[result] = .{.data=.{
					.type = &(vm.cells[resultType].type),
					.memory = try vm.allocator.alloc(u8, resultSize),
				}};
			},
			OpCodeId.OpCompositeConstruct => {
				var resultType = args[0];
				var result = args[1];
				std.debug.print("[{}] allocing Composite Construct memory {}\n", .{result, resultType});
				var resultSize = vm.cells[resultType].type.byteWidth;
				vm.cells[result] = .{.data=.{
					.type = &(vm.cells[resultType].type),
					.memory = try vm.allocator.alloc(u8, resultSize),
				}};
			},
			else => {},
		}
	}

	var fnId: ?spvm_word = vm.names.get("main(pv3f32;");

	if(fnId) |fnIdx| {
		//Test run function (vec3)
		var output = [1]vec3{vec3{
			.x = 3.141592653589,
			.y = 3.141592653589 * 2.0,
			.z = 3.141592653589 / 2.0,
		}};
		var outputs: []vec3 = output[0..1];
		//@compileLog(@sizeOf(@TypeOf(outputs)));
		try runFn(vm, fnIdx, .{
			.output = outputs,
		});

		for(vm.cells, 0..) |cell, idx| {
			switch(cell) {
				.type => |typeCell| std.debug.print("[{}] typeCell: {}\n", .{idx, typeCell}),
				.function => |funcCell| std.debug.print("[{}] funcCell: {}\n", .{idx, funcCell}),
				.data => |dataCell| std.debug.print("[{}] dataCell: {}\n", .{idx, dataCell}),
				.null => std.debug.print("[{}] null: {{}}\n", .{idx}),
			}
		}

		std.debug.print("output: {}\n", .{output[0]});
	}
	else
	{
		std.debug.print("function not found!\n", .{});
	}

	//var retMemory = vm.cells[vm.returnValuePtr].data.memory;
	//var retValue = std.mem.bytesAsValue(vec3, retMemory[0..12]);

	//std.debug.print("Result: {}\n", .{retValue});

	return vm;
}

//Temp struct for visualizing result
const vec3 = extern struct {
	x: f32,
	y: f32,
	z: f32,
};

export fn c_init(words: [*c]spvm_word, word_count: spvm_word) [*c]c_short {
	global_allocator = std.heap.c_allocator;
	var vm = init(global_allocator, words, word_count) catch null;
	return @ptrCast(vm);
}
