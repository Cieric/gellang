// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

__inlineasm vec3 vec3(float@ x, float@ y, float@ z)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <var> Variable <vec3> Function;
    <composite> CompositeConstruct <vec3> <x> <y> <z>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec3 vec3(vec2 xy, float@ z)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <var> Variable <vec3> Function;
    <x> CompositeExtract <float> <xy> 0;
    <y> CompositeExtract <float> <xy> 1;
    <composite> CompositeConstruct <vec3> <x> <y> <z>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec3 vec3(float@ x, vec2 yz)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <var> Variable <vec3> Function;
    <y> CompositeExtract <float> <yz> 0;
    <z> CompositeExtract <float> <yz> 1;
    <composite> CompositeConstruct <vec3> <x> <y> <z>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec3 vec3(float@ x, vec3 yzw)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <var> Variable <vec3> Function;
    <y> CompositeExtract <float> <yzw> 0;
    <z> CompositeExtract <float> <yzw> 1;
    <composite> CompositeConstruct <vec3> <x> <y> <z>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec3 vec3(float@ x)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <var> Variable <vec3> Function;
    <composite> CompositeConstruct <vec3> <x> <x> <x>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec3 vec3(vec3@ xyz)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <var> Variable <vec3> Function;
    <x> CompositeExtract <float> <xyz> 0;
    <y> CompositeExtract <float> <xyz> 1;
    <z> CompositeExtract <float> <xyz> 2;
    <composite> CompositeConstruct <vec3> <x> <y> <z>;
    Store <var> <composite>;
    ReturnValue <var>;
}