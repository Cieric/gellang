// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

__inlineasm vec2 vec2(float@ x)
{
	<float> TypeFloat 32;
	<vec2> TypeVector <float> 2;
	<var> Variable <vec2> Function;
	<composite> CompositeConstruct <vec2> <x> <x>;
	Store <var> <composite>;
	ReturnValue <var>;
}

__inlineasm vec2 vec2(float@ x, float@ y)
{
	<float> TypeFloat 32;
	<vec2> TypeVector <float> 2;
	<var> Variable <vec2> Function;
	<composite> CompositeConstruct <vec2> <x> <y>;
	Store <var> <composite>;
	ReturnValue <var>;
}