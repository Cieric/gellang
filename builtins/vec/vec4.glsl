// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

__inlineasm vec4 vec4(float x, float y, float z, float w)
{
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <var> Variable <vec4> Function;
    <composite> CompositeConstruct <vec4> <x> <y> <z> <w>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec4 vec4(vec2 xy, float z, float w)
{
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <var> Variable <vec4> Function;
    <x> CompositeExtract <float> <xy> 0;
    <y> CompositeExtract <float> <xy> 1;
    <composite> CompositeConstruct <vec4> <x> <y> <z> <w>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec4 vec4(float x, vec3 yzw)
{
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <var> Variable <vec4> Function;
    <y> CompositeExtract <float> <yzw> 0;
    <z> CompositeExtract <float> <yzw> 1;
    <w> CompositeExtract <float> <yzw> 2;
    <composite> CompositeConstruct <vec4> <x> <y> <z> <w>;
    Store <var> <composite>;
    ReturnValue <var>;
}