// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include <GLSL420.glsl>

vec3 iResolution;
float iTime;
int iFrame;
vec4 iMouse;