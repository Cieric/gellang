// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include <vec/vec2.glsl>
#include <vec/vec3.glsl>
#include <vec/vec4.glsl>

__inlineasm float float(int x)
{
	<float> TypeFloat 32;
    <var> ConvertSToF <float> <x>;
	ReturnValue <var>;
}

__inlineasm mat2 mat2(float x, float y, float z, float w)
{
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <mat2> TypeMatrix <vec2> 2;
    <var> Variable <mat2> Function;
    <xy> CompositeConstruct <vec2> <x> <y>;
    <zw> CompositeConstruct <vec2> <z> <w>;
    <composite> CompositeConstruct <mat2> <xy> <zw>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm mat3 mat3(float e)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <mat3> TypeMatrix <vec3> 3;
    <var> Variable <mat3> Function;
    <0> Constant <float> 0.0;
    <r1> CompositeConstruct <vec3> <e> <0> <0>;
    <r2> CompositeConstruct <vec3> <0> <e> <0>;
    <r3> CompositeConstruct <vec3> <0> <0> <e>;
    <composite> CompositeConstruct <mat3> <r1> <r2> <r3>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm mat3 mat3(float e1, float e2, float e3, float e4, float e5, float e6, float e7, float e8, float e9)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <mat3> TypeMatrix <vec3> 3;
    <var> Variable <mat3> Function;
    <r1> CompositeConstruct <vec3> <e1> <e2> <e3>;
    <r2> CompositeConstruct <vec3> <e4> <e5> <e6>;
    <r3> CompositeConstruct <vec3> <e7> <e8> <e9>;
    <composite> CompositeConstruct <mat3> <r1> <r2> <r3>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm mat3 mat3(vec3 x, vec3 y, vec3 z)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <mat3> TypeMatrix <vec3> 3;
    <var> Variable <mat3> Function;
    <composite> CompositeConstruct <mat3> <x> <y> <z>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec4 vec4(vec3 xyz, float w)
{
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <var> Variable <vec4> Function;
    <x> CompositeExtract <float> <xyz> 0;
    <y> CompositeExtract <float> <xyz> 1;
    <z> CompositeExtract <float> <xyz> 2;
    <composite> CompositeConstruct <vec4> <x> <y> <z> <w>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec4 vec4(float x)
{
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <var> Variable <vec4> Function;
    <composite> CompositeConstruct <vec4> <x> <x> <x> <x>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm mat4 mat4(vec4 x, vec4 y, vec4 z, vec4 w)
{
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <mat4> TypeMatrix <vec4> 4;
    <var> Variable <mat4> Function;
    <composite> CompositeConstruct <mat4> <x> <y> <z> <w>;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm mat4 mat4(float x)
{
    <float> TypeFloat 32;
    <0> Constant <float> 0.0;

    <vec4> TypeVector <float> 4;
    <r1> CompositeConstruct <vec4> <x> <0> <0> <0>;
    <r2> CompositeConstruct <vec4> <0> <x> <0> <0>;
    <r3> CompositeConstruct <vec4> <0> <0> <x> <0>;
    <r4> CompositeConstruct <vec4> <0> <0> <0> <x>;
    
    <mat4> TypeMatrix <vec4> 4;
    <composite> CompositeConstruct <mat4> <r1> <r2> <r3> <r4>;

    <var> Variable <mat4> Function;
    Store <var> <composite>;
    ReturnValue <var>;
}

__inlineasm vec3 round(vec3 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 1 <v>;
    ReturnValue <2>;
}

__inlineasm float abs(float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 4 <x>;
    ReturnValue <2>;
}

__inlineasm vec2 abs(vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 4 <x>;
    ReturnValue <2>;
}

__inlineasm vec3 abs(vec3 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 4 <v>;
    ReturnValue <2>;
}

__inlineasm float sign(float v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 6 <v>;
    ReturnValue <2>;
}

__inlineasm vec2 sign(vec2 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 6 <v>;
    ReturnValue <2>;
}

__inlineasm vec3 sign(vec3 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 6 <v>;
    ReturnValue <2>;
}

__inlineasm vec4 sign(vec4 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <2> ExtInst <vec4> <1> 6 <v>;
    ReturnValue <2>;
}

__inlineasm float floor(float v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 8 <v>;
    ReturnValue <2>;
}

__inlineasm vec2 floor(vec2 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 8 <v>;
    ReturnValue <2>;
}

__inlineasm vec3 floor(vec3 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 8 <v>;
    ReturnValue <2>;
}

__inlineasm vec4 floor(vec4 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <2> ExtInst <vec4> <1> 8 <v>;
    ReturnValue <2>;
}

__inlineasm float acos(float v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 17 <v>;
    ReturnValue <2>;
}

__inlineasm float atan(float y, float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 18 <y> <x>;
    ReturnValue <2>;
}

__inlineasm vec3 reflect(vec3 I, vec3 N)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 71 <I> <N>;
    ReturnValue <2>;
}

__inlineasm vec3 normalize(vec3 v)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 69 <v>;
    ReturnValue <2>;
}

__inlineasm vec3 cross(vec3 x, vec3 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 68 <x> <y>;
    ReturnValue <2>;
}

__inlineasm int min(int x, int y)
{
    <1> ExtInstImport "GLSL.std.450";
    <int> TypeInt 32 1;
    <2> ExtInst <int> <1> 37 <x> <y>;
    ReturnValue <2>;
}

__inlineasm float min(float x, float y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 37 <x> <y>;
    ReturnValue <2>;
}

__inlineasm vec2 min(vec2 x, vec2 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 37 <x> <y>;
    ReturnValue <2>;
}

__inlineasm vec3 min(vec3 x, vec3 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 37 <x> <y>;
    ReturnValue <2>;
}

__inlineasm float max(float x, float y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 40 <x> <y>;
    ReturnValue <2>;
}

__inlineasm vec2 max(vec2 x, vec2 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 40 <x> <y>;
    ReturnValue <2>;
}

__inlineasm vec3 max(vec3 x, float y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <yyy> CompositeConstruct <vec3> <y> <y> <y>;
    <2> ExtInst <vec3> <1> 40 <x> <yyy>;
    ReturnValue <2>;
}

__inlineasm vec3 max(vec3 x, vec3 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 40 <x> <y>;
    ReturnValue <2>;
}

__inlineasm float clamp(float x, float minVal, float maxVal)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 43 <x> <minVal> <maxVal>;
    ReturnValue <2>;
}

__inlineasm vec3 clamp(vec3 x, float f_minVal, float f_maxVal)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <minVal> CompositeConstruct <vec3> <f_minVal> <f_minVal> <f_minVal>;
    <maxVal> CompositeConstruct <vec3> <f_maxVal> <f_maxVal> <f_maxVal>;
    <2> ExtInst <vec3> <1> 43 <x> <minVal> <maxVal>;
    ReturnValue <2>;
}

__inlineasm vec4 clamp(vec4 x, float f_minVal, float f_maxVal)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <minVal> CompositeConstruct <vec4> <f_minVal> <f_minVal> <f_minVal> <f_minVal>;
    <maxVal> CompositeConstruct <vec4> <f_maxVal> <f_maxVal> <f_maxVal> <f_maxVal>;
    <2> ExtInst <vec4> <1> 43 <x> <minVal> <maxVal>;
    ReturnValue <2>;
}

__inlineasm vec3 clamp(vec3 x, vec3 minVal, vec3 maxVal)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 43 <x> <minVal> <maxVal>;
    ReturnValue <2>;
}

__inlineasm float sin(float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 13 <x>;
    ReturnValue <2>;
}

__inlineasm vec2 sin(vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 13 <x>;
    ReturnValue <2>;
}

__inlineasm vec3 sin(vec3 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 13 <x>;
    ReturnValue <2>;
}

__inlineasm vec4 sin(vec4 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <2> ExtInst <vec4> <1> 13 <x>;
    ReturnValue <2>;
}

__inlineasm float cos(float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 14 <x>;
    ReturnValue <2>;
}

__inlineasm vec2 cos(vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 14 <x>;
    ReturnValue <2>;
}

__inlineasm vec3 cos(vec3 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 14 <x>;
    ReturnValue <2>;
}

__inlineasm vec4 cos(vec4 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <2> ExtInst <vec4> <1> 14 <x>;
    ReturnValue <2>;
}

__inlineasm float dot(vec2 x, vec2 y)
{
    <float> TypeFloat 32;
    <r> Dot <float> <x> <y>;
    ReturnValue <r>;
}

__inlineasm float dot(vec3 x, vec3 y)
{
    <float> TypeFloat 32;
    <r> Dot <float> <x> <y>;
    ReturnValue <r>;
}

__inlineasm float dot(vec4 x, vec4 y)
{
    <float> TypeFloat 32;
    <r> Dot <float> <x> <y>;
    ReturnValue <r>;
}

__inlineasm float mod(int x, int y)
{
    <int> TypeInt 1 32;
    <r> Mod <int> <x> <y>;
    ReturnValue <r>;
}

__inlineasm float mod(vec3 x, vec3 y)
{
    <float> TypeFloat 32;
    <r> Mod <float> <x> <y>;
    ReturnValue <r>;
}

__inlineasm float sqrt(float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 31 <x>;
    ReturnValue <2>;
}

__inlineasm float pow(float x, float y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 26 <x> <y>;
    ReturnValue <2>;
}

__inlineasm vec2 pow(vec2 x, vec2 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 26 <x> <y>;
    ReturnValue <2>;
}

__inlineasm vec3 pow(vec3 x, vec3 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 26 <x> <y>;
    ReturnValue <2>;
}

__inlineasm vec4 pow(vec4 x, vec4 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <2> ExtInst <vec4> <1> 26 <x> <y>;
    ReturnValue <2>;
}

__inlineasm float exp(float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 27 <x>;
    ReturnValue <2>;
}

__inlineasm vec2 exp(vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 27 <x>;
    ReturnValue <2>;
}

__inlineasm vec3 exp(vec3 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 27 <x>;
    ReturnValue <2>;
}

__inlineasm vec4 exp(vec4 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <2> ExtInst <vec4> <1> 27 <x>;
    ReturnValue <2>;
}

__inlineasm float exp2(float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 29 <x>;
    ReturnValue <2>;
}

__inlineasm vec3 exp2(vec3 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 29 <x>;
    ReturnValue <2>;
}

__inlineasm float step(float edge, float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 48 <edge> <x>;
    ReturnValue <2>;
}

__inlineasm vec2 step(vec2 edge, vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 48 <edge> <x>;
    ReturnValue <2>;
}

__inlineasm vec3 step(vec3 edge, vec3 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 48 <edge> <x>;
    ReturnValue <2>;
}

__inlineasm float fract(float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 10 <x>;
    ReturnValue <2>;
}

__inlineasm vec2 fract(vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 10 <x>;
    ReturnValue <2>;
}

__inlineasm vec3 fract(vec3 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 10 <x>;
    ReturnValue <2>;
}

__inlineasm vec4 fract(vec4 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <2> ExtInst <vec4> <1> 10 <x>;
    ReturnValue <2>;
}

__inlineasm float smoothstep(float edge0, float edge1, float x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 49 <edge0> <edge1> <x>;
    ReturnValue <2>;
}

__inlineasm vec2 smoothstep(vec2 edge0, vec2 edge1, vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <2> ExtInst <vec2> <1> 49 <edge0> <edge1> <x>;
    ReturnValue <2>;
}

__inlineasm float length(vec2 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 66 <x>;
    ReturnValue <2>;
}

__inlineasm float length(vec3 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 66 <x>;
    ReturnValue <2>;
}

__inlineasm float length(vec4 x)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 66 <x>;
    ReturnValue <2>;
}

__inlineasm float mix(float x, float y, float a)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <2> ExtInst <float> <1> 46 <x> <y> <a>;
    ReturnValue <2>;
}

__inlineasm vec2 mix(vec2 x, vec2 y, vec2 a)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 3;
    <2> ExtInst <vec2> <1> 46 <x> <y> <a>;
    ReturnValue <2>;
}

__inlineasm vec3 mix(vec3 x, vec3 y, float a)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <new_a> Variable <vec3> Function;
    <composite> CompositeConstruct <vec3> <a> <a> <a>;
    Store <new_a> <composite>;
    <2> ExtInst <vec3> <1> 46 <x> <y> <new_a>;
    ReturnValue <2>;
}

__inlineasm vec3 mix(vec3 x, vec3 y, vec3 a)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 46 <x> <y> <a>;
    ReturnValue <2>;
}

__inlineasm vec4 mix(vec4 x, vec4 y, float a)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <new_a> Variable <vec4> Function;
    <composite> CompositeConstruct <vec4> <a> <a> <a> <a>;
    Store <new_a> <composite>;
    <2> ExtInst <vec4> <1> 46 <x> <y> <new_a>;
    ReturnValue <2>;
}

__inlineasm vec3 distance(vec3 x, vec3 y)
{
    <1> ExtInstImport "GLSL.std.450";
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <2> ExtInst <vec3> <1> 67 <x> <y>;
    ReturnValue <2>;
}