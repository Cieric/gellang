// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#define GLFW_KEY_A 65
#define GLFW_KEY_B 66
#define GLFW_KEY_C 67
#define GLFW_KEY_D 68
#define GLFW_KEY_E 69
#define GLFW_KEY_F 70
#define GLFW_KEY_G 71
#define GLFW_KEY_H 72
#define GLFW_KEY_I 73
#define GLFW_KEY_J 74
#define GLFW_KEY_K 75
#define GLFW_KEY_L 76
#define GLFW_KEY_M 77
#define GLFW_KEY_N 78
#define GLFW_KEY_O 79
#define GLFW_KEY_P 80
#define GLFW_KEY_Q 81
#define GLFW_KEY_R 82
#define GLFW_KEY_S 83
#define GLFW_KEY_T 84
#define GLFW_KEY_U 85
#define GLFW_KEY_V 86
#define GLFW_KEY_W 87
#define GLFW_KEY_X 88
#define GLFW_KEY_Y 89
#define GLFW_KEY_Z 90
#define GLFW_KEY_SPACE 32
#define GLFW_KEY_LEFT_SHIFT 340
#define GLFW_KEY_LEFT_CONTROL 341

mat4 lookAt(vec3 eye, vec3 center, vec3 up)
{
	vec3 f = normalize(center - eye);
	vec3 u = normalize(up);
	vec3 s = normalize(cross(f, u));
	u = cross(s, f);
	f = -f;

	mat4 Result = mat4(
		vec4(s.x, u.x, f.x, 0.0),
		vec4(s.y, u.y, f.y, 0.0),
		vec4(s.z, u.z, f.z, 0.0),
		vec4(-dot(s, eye), -dot(u, eye), dot(-f, eye), 1.0)
    );
	return Result;
}

__inlineasm void setTransform(mat4 mat)
{
    <1> ExtInstImport "CGEngine.ObjectFunctions";
    <void> TypeVoid;
    <float> TypeFloat 32;
    <vec4> TypeVector <float> 4;
    <mat4> TypeVector <vec4> 4;
    ExtInst <void> <1> 2 <mat>;
    Return;
}

__inlineasm uint8 getMouseButton(uint mouseButton)
{
    <1> ExtInstImport "CGEngine.ObjectFunctions";
    <byte> TypeInt 8 0;
    <buttonState> ExtInst <byte> <1> 4 <mouseButton>;
    ReturnValue <buttonState>;
}

__inlineasm uint8 getKeyDown(int keycode)
{
    <1> ExtInstImport "CGEngine.ObjectFunctions";
    <byte> TypeInt 8 0;
    <keyState> ExtInst <byte> <1> 3 <keycode>;
    ReturnValue <keyState>;
}

__inlineasm vec2 getMousePos()
{
    <1> ExtInstImport "CGEngine.ObjectFunctions";
    <float> TypeFloat 32;
    <vec2> TypeVector <float> 2;
    <mousePos> ExtInst <vec2> <1> 5;
    ReturnValue <mousePos>;
}

__inlineasm void setCursorVisible(bool visible)
{
    <1> ExtInstImport "CGEngine.ObjectFunctions";
    <void> TypeVoid;
    ExtInst <void> <1> 6 <visible>;
    Return;
}

__inlineasm float random()
{
    <1> ExtInstImport "CGEngine.ObjectFunctions";
    <float> TypeFloat 32;
    <rng> ExtInst <float> <1> 7;
    ReturnValue <rng>;
}

__inlineasm float breakpoint()
{
    <1> ExtInstImport "CGEngine.ObjectFunctions";
    <void> TypeVoid;
    ExtInst <void> <1> 8;
    Return;
}