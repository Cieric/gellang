// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

//__inlineasm void printf(...)
//{
//    <1> ExtInstImport "NonSemantic.DebugPrintf";
//    <void> TypeVoid;
//    <2> ExtInst <void> <1> 1 <...>;
//}