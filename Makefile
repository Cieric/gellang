# SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
# SPDX-License-Identifier: MIT

.PHONY: build

build:
	cd GELLang && $(MAKE)
	cd SpirVInterpreter-zig && zig build
	cd SpirVTester && $(MAKE)

clean:
	cd GELLang && $(MAKE) clean
#	cd SpirVInterpreter && $(MAKE) clean
	cd SpirVTester && $(MAKE) clean