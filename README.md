# GEL Lang
General Idea behind the language is for it to be used with my game engine project. GEL is being designed as a superset to GLSL with more typical CPU side necessities added like string support. GEL will hopefully in the future have the ability to compile functions to SPIR-V, OpenCL, OpenGL, etc. The idea being that if a function could be run faster on the GPU the language runtime will deal with the seamlessly. (With compiler suggestions so you know if your code is using a feature not supported on the GPU.)  

GEL will basically always be a work in progress, don't expect to just download and it work right off the bat.
## Project Notes
### GEL Additions to GLSL
- Strings
- Built-in debugger
- Shaders inline with rest of code
### Things I don't plan to add
- Function pointers
- Pointers in general
- pass by reference
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Issue Submission
Please include a minimal test program if applicable, If you're submitting an issue about conformance to GLSL provide a link to the corresponding documentation.

## License
[MIT](LICENSE)
