// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

//#version 420

__inlineasm vec3 vec3(float@ x, float@ y, float@ z)
{
    <float> TypeFloat 32;
    <vec3> TypeVector <float> 3;
    <composite> CompositeConstruct <vec3> <x> <y> <z>;
    ReturnValue <composite>;
}

struct Object
{
	vec3 position;
	vec3 velocity;
};

void main(out vec3 output)
{
	Object obj;
	obj.position = vec3(5.0, 1.0, 2.0);
	obj.position.z = 10.0;
	output = obj.position;
}

//__inlineasm void main(out vec3 output)
//{
//	<float> TypeFloat 32;
//	<v3float> TypeVector <float> 3;
//	<Object> TypeStruct <v3float> <v3float>;
//	<_ptr_Private_Object> TypePointer Private <Object>;
//	<obj> Variable <_ptr_Private_Object> Private;
//	<int> TypeInt 32 1;
//	<int_0> Constant <int> 0;
//	<float_5> Constant <float> 5;
//	<float_1> Constant <float> 1;
//	<float_2> Constant <float> 2;
//	<16> CompositeConstruct <v3float> <float_5> <float_1> <float_2>;
//	<_ptr_Private_v3float> TypePointer Private <v3float>;
//	<_ptr_Output_v3float> TypePointer Output <v3float>;
//	<fragColor> Variable <_ptr_Output_v3float> Output;
//	<18> AccessChain <_ptr_Private_v3float> <obj> <int_0>;
//	Store <18> <16>;
//	<21> AccessChain <_ptr_Private_v3float> <obj> <int_0>;
//	<22> Load <v3float> <21>;
//	Store <output> <22>;
//}