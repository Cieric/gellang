// SPDX-FileCopyrightText: 2017-2023 Advanced Micro Devices, Inc. All rights reserved.
// SPDX-License-Identifier: MIT

#include <stdlib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <algorithm>
#include <chrono>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_write_png.h>
#undef STB_IMAGE_WRITE_IMPLEMENTATION

#include <SpirVInterpreter.h>

typedef uint32_t spvm_word;
typedef spvm_word* spvm_source;

spvm_source load_source(const char* fname, size_t* src_size) {
	FILE* f  = fopen(fname, "rb");
	if (f == 0) {
		printf("Failed to load file %s\n", fname);
		return 0;
	}

	fseek(f, 0, SEEK_END);
	long file_size = ftell(f);
	fseek(f, 0, SEEK_SET);

	size_t el_count = (file_size / sizeof(spvm_word));
	spvm_source ret = (spvm_source)malloc(el_count * sizeof(spvm_word));
	fread((void*)ret, el_count, sizeof(spvm_word), f);
	fclose(f);

	*src_size = el_count;

	return ret;
}

struct Img
{
	uint32_t width;
	uint32_t height;
	char* data;
};

void write_image(const char* filename, Img image)
{
	const int dimx = image.width, dimy = image.height;
	FILE* fp = fopen(filename, "wb"); /* b - binary mode */
	fprintf(fp, "P6\n%d %d\n255\n", dimx, dimy);
	fwrite(image.data, 3, dimx*dimy, fp);
	fclose(fp);
}

int main()
{
	//SpirVInterpreter interpreter;
	size_t size;
	spvm_source source = load_source("shader.spv", &size);
	std::vector<spvm_word> code(source, source + size); 
	//interpreter.Init(code);
	auto vm = c_init(code.data(), code.size());

	 

	printf("program len: %lld\n", code.size());
	
	//auto fnMap = interpreter.getFunctionMap();
	//printf("\nFunctions:\n");
	//for (auto fn : fnMap)
	//{
	//	printf("%4d : %s\n", (int)fn.second, fn.first.c_str());
	//}
	
	//int result = interpreter.Run(spvm_string("main(v3f32;"));
	//printf("result: %d\n", result);

	return 0;
	//return text_main();
}
